<?php
/**
 * Created by PhpStorm.
 * User: oluwatobi.okusanya
 * Date: 25/11/15
 * Time: 1:50 PM
 */

return [
    'FOLLOWING_CONTACT' => 'C',
    'FOLLOWING' => 'F',
    'DEFAULT_ALBUM_COVER_IMAGE' => 'default_image.jpg',
    'DEFAULT_WALL_COVER_IMAGE' => 'default_image.jpg',
    'GALLERY_PUBLIC' => '0',
    'GALLERY_CONTACTS' => '1',
    'EMAIL_NOTIFICATION_OFF' => '0',
    'EMAIL_NOTIFICATION_ON' => '1',
    'PRIVACY_CONTACT_REQUEST_EVERYONE' => 0,
    'PRIVACY_CONTACT_REQUEST_CONTACTS_OF_CONTACTS' => 1,
    'PRIVACY_CONTACT_LOOK_UP_EVERYONE' => 0,
    'PRIVACY_CONTACT_LOOK_UP_CONTACTS_OF_CONTACTS' => 1,
    'PRIVACY_CONTACT_LOOK_UP_CONTACTS' => 2,
    'MESSAGE_DELETED_ACTION_ID' => 1,
];