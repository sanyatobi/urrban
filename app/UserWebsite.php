<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserWebsite extends Model
{
    protected $table = 'users_websites';

    protected $fillable = ['website'];


}
