<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class UserAlbums extends Model
{
    use SoftDeletes;
    protected $table = 'users_albums';

    protected $fillable = ['title','slug','privilege'];
    protected $dates = ['deleted_at'];

    public function media(){
        return $this->hasMany('App\AlbumMedia','album_id');
    }

    //
}
