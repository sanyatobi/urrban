<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Post extends Model
{
    use SoftDeletes;
    //
    protected $dates = ['deleted_at'];

    public function owner(){
        return $this->belongsTo('App\User','user_id','id');
    }/*
    public function likes(){
        return $this->belongsToMany('App\User','posts_likes','post_id','user_id');
    }*/
    public function likes(){
        return $this->hasMany('App\Likes');
    }
    public function comments(){
        return $this->hasMany('App\Comments');
    }
}
