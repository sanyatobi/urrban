<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = ['status'];

    public function category(){
       return $this->belongsTo('App\NotificationCategory','category_id');
    }

    public function sender(){
        return $this->belongsTo('App\User','sender_id');
    }
}
