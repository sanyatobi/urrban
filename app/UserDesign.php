<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDesign extends Model
{
    protected $table = 'users_designs';

    protected $fillable = ['design'];


}
