<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Validator;

class User extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['name', 'email', 'password','interests','username','headline','design','gender','phone1',
        'phone2','dob','country_code','city_id','side_gig','years_in_business','summary_of_qualification','receive_email_notification','privacy_contact_request','privacy_look_up','use_email','use_phone','address','engaged_in','height','weight','skin','hair','eye','foot','waist','bust','hip'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    private $create_rules = array(
        'name' => 'required|max:255',
        'email' => 'required|unique:users,email|email',
        'username' => 'unique:users',
        'password' => 'required',
        'interests' => 'required',
    );

    private $update_rules = array(
        'name' => 'required|max:255',
        'interests' => 'required',
        'username' => 'unique:users',
    );

    private $settings_rules = array(
        'email' => 'required|unique:users,email|email',
        'username' => 'unique:users,username',
    );

    private $errors;

    public function validate($data,$scenario,$rules=null)
    {
        if(!$rules) {

            switch ($scenario) {
                case 'create':
                    $rules = $this->create_rules;
                    break;
                case 'update':
                    $rules = $this->update_rules;
                    break;
                case 'settings':
                    $rules = $this->settings_rules;
                    break;
            }
        }

        // make a new validator object
        $v = Validator::make($data, $rules);

        // check for failure
        if ($v->fails())
        {
            // set errors and return false
            $this->errors = $v->errors();
            return false;
        }


        // validation pass
        return true;
    }

    public function errors()
    {
        return $this->errors;
    }

    public function country(){
        return $this->belongsTo('App\Countries','country_code','code');
    }
    public function city(){
        return $this->belongsTo('App\Cities');
    }
    public function design(){
        return $this->hasMany('App\UserDesign');
    }
    public function website(){
        return $this->hasMany('App\UserWebsite');
    }
    public function language(){
        return $this->hasMany('App\UserLanguage');
    }
    public function education(){
        return $this->hasMany('App\UserEducation');
    }
    public function albums(){
        return $this->hasMany('App\UserAlbums');
    }
    public function email_notifications(){
        return $this->belongsToMany('App\EmailNotifications','user_email_notifications','user_id','email_notification_id');
    }
    public function album_media(){
        return $this->hasManyThrough('App\AlbumMedia', 'App\UserAlbums','user_id','album_id');
    }
    public function interests(){
        return $this->belongsTo('App\Categories','interests');
    }

    public function followers()
    {
        return $this->belongsToMany('App\User','users_followers','user_id','follower_id');
    }
    public function following()
    {
        return $this->belongsToMany('App\User','users_followers','follower_id','user_id');
    }
    public function contacts()
    {
        return $this->belongsToMany('App\User','users_followers','user_id','follower_id')
            ->where('type',config('constants.FOLLOWING_CONTACT'));
    }
    public function contact_requests(){
        return $this->belongsToMany('App\User','users_requests','user_id','sender_id')
            ->where('request_type',config('constants.FOLLOWING_CONTACT'));
    }


}
