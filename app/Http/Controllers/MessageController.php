<?php

namespace App\Http\Controllers;

use App\Message;
use App\MessagesAction;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id = $request->id;
        $body = $request->msg;

        $message = new Message();
        $message->body = $body;
        $message->sender_id = Auth::user()->id;
        $message->receiver_id = $id;
        if($message->save()){
            return response()->json(['status' => 'success', 'message' => "Message Sent"]);
        }
        return response()->json(['status' => 'error', 'message' =>'Error Occurred']);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //fetch all message ids
        $msgs = Message::where(function ($query) use ($request){
            $query->where('sender_id', '=', Auth::user()->id)
                ->where('receiver_id', '=', $request->id);
        })->orWhere(function ($query) use ($request){
            $query->where('receiver_id', '=', Auth::user()->id)
                ->where('sender_id', '=', $request->id);
        })->lists('id');

        foreach($msgs as $msg_id){
            $msg_action = new MessagesAction();
            $msg_action->message_id = $msg_id;
            $msg_action->action_id = config('constants.MESSAGE_DELETED_ACTION_ID');
            $msg_action->user_id = Auth::user()->id;

            $msg_action->save();
        }

        return response()->json(['status' => 'success',  'message' => "Conversation cleared"]);

    }

    public function conversation($id){

        //mark all messages in this conversation as read
        $update_messages = Message::where('sender_id',$id)
                            ->where('receiver_id',Auth::user()->id)
                            ->update(['status' => 1]);

        $result = DB::select(" select us.avatar, msg.body, msg.created_at, CASE mact.ACTION_ID WHEN  :deleted_action AND mact.user_id = :user_id_3 THEN 'cleared' END as 'message_status' from messages msg left join messages_action mact on msg.id=mact.message_id and mact.user_id = :user_id_4 join users us on msg.sender_id = us.id where (sender_id = :user_id and receiver_id = :contact_id) or (sender_id = :contact_id_2 and receiver_id = :user_id_2) order by msg.created_at DESC",[":user_id"=>Auth::user()->id,":contact_id"=>$id,":contact_id_2"=>$id,":user_id_2"=>Auth::user()->id,":deleted_action"=>config('constants.MESSAGE_DELETED_ACTION_ID'),":user_id_3"=>Auth::user()->id,":user_id_4"=>Auth::user()->id]);

        return response()->json(['status' => 'success', 'message' => $result]);

    }
    public function unread(){

        $unread = Message::where("receiver_id" ,'=', Auth::user()->id)
            ->with('sender')
            ->where('status', '=', '0')
            ->orderBy('updated_at','desc')
            ->get()->toArray();

        $util = new UtilityController();

        $messages = [];

        foreach($unread as $key=>$msg){
         $time = $util->time_elapsed_string($msg['created_at']);
            $preview = (strlen($msg['body']) > 45) ? substr($msg["body"],0,45)."..." : $msg['body'];
            $messages[] = ["avatar" => $msg['sender']['avatar'], "name" => $msg['sender']['name'], "time" => $time, "preview" => $preview];
        }

        return response()->json(["data"=>['status' => 'success', 'count' => count($unread), 'message' => $messages]]);


    }

    public function all(Request $request){





        $result = DB::select("select us.updated_at as last_online, us.avatar, us.name, us.id from users_followers uf join users us on us.id=uf.follower_id  left join messages msgs on uf.user_id = msgs.receiver_id and uf.follower_id=msgs.sender_id where uf.type=:contact_type and uf.user_id = :user_id and us.name like '%$request->q%'  group by follower_id order by msgs.created_at DESC;",["user_id"=>Auth::user()->id,"contact_type"=>config('constants.FOLLOWING_CONTACT')]);


        return response()->json(['status' => 'success', 'message' => $result]);
    }

}
