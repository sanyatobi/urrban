<?php
/**
 * Created by PhpStorm.
 * User: oluwatobi.okusanya
 * Date: 29/11/15
 * Time: 2:28 PM
 */
namespace App\Http\Controllers;
use App\AlbumMedia;
use App\Albums;
use App\Http\Controllers\Controller;
use App\MediaLikes;
use App\Post;
use App\User;
use App\UserAlbums;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class GalleryController extends Controller
{

    public function editAlbum(Request $request){

        $album = UserAlbums::find($request->album_id);

        if($request->user()->cannot('edit-album',$album)){
            return response()->json(['status' => 'error', 'message' => 'Access Denied']);
        }
        if($request->album_title == "Wall Posts"){
                return response()->json(['status' => 'error', 'message' => 'Invalid Album Title']);
            }
        $album->title = $request->album_title;
        $album->visibility = $request->privilege;
        $album->slug = str_slug($request->album_title, "-");

        if($album->save()){
            return response()->json(['status' => 'success', 'message' =>"Changes Saved"]);
        }
        else{
            return response()->json(['status' => 'error', 'message' => 'Access Denied']);
        }
    }

    public function viewAlbum($username, $slug)
    {
        $user_id = User::where('username', $username)->first()->id;

        if ($slug == 'wall-posts') {

            $wall_media = Post::where('user_id', $user_id)
                ->with('likes','comments','owner')
                ->where(function ($query) {
                    $query->where('image_url', '!=', '')
                        ->orWhere('video_url', '!=', '');
                })
                ->get()
                ->toArray();

            $album = ['visibility'=>'public','id'=>'0',"is_wall_post"=> true,"title" => "Wall Posts", "media" => $wall_media];

        } else {

            $album_data = UserAlbums::where('slug',$slug)->where('user_id',$user_id)->first();

            $album_id = $album_data['id'];

            $album_media = AlbumMedia::where('album_id',$album_id)
                ->with('likes','comments')
                ->get()
                ->toArray();

            /*$album = UserAlbums::where('user_id', $user_id)
                ->where('slug', $slug)
                ->with('media')
                ->first()
                ->toArray();*/

            $visibility = ($album_data['visibility'] == 0) ? 'public' : 'contact';


            $album = ['visibility'=>$visibility,'id' => $album_data['id'],'is_wall_post' => false,'title' => $album_data['title'],'media'=>$album_media];

        }
        return View::make('user.album')->with('album', $album);

    }

    public function sendMedia(Request $request){
        $allowed_mime = ["image","video"];




        if ($request->has('post_content')){
            $media_content = $request->post_content;

            $media_data = new AlbumMedia();
            $media_data->album_id = $request->album_id;
            $media_data->content = $media_content;


            if( ($request->hasFile('file'))&& ($request->file('file')->isValid()) ){
                $file_mime = substr($request->file('file')->getMimeType(),0,5);
                if(in_array($file_mime,$allowed_mime)){

                    $file_name = $request->user()->id."-".time().".".$request->file('file')->getClientOriginalExtension();
                    if(Storage::put('gallery_media/'.$file_name, file_get_contents($request->file('file')))){
                        //store post data
                        $media_data->image_url = ($file_mime == "image") ? $file_name : "";
                        $media_data->video_url = ($file_mime == "video") ? $file_name : "";

                    }
                    else{
                        return response()->json(['status' => 'error', 'message' =>"Error Occurred, Could not upload file"]);
                    }

                }
                else{
                    return response()->json(['status' => 'error', 'message' =>"Error Occurred, Invalid File Type"]);
                }

            }
            if($media_data->save()){
                return response()->json(['status' => 'success', 'message' =>"Media Saved"]);
            }
            else{

            }
        }
        else{
            return response()->json(['status' => 'error', 'message' =>"Error Occurred, Action could not be completed"]);

        }
    }
    public function deleteAlbum(Request $request){

        $album  = UserAlbums::find($request->album_id);

        if($request->user()->cannot('delete-album',$album)){
            return response()->json(['status' => 'error', 'message' =>'Access Denied']);
        }
        else{
            if($album->delete()){
                return response()->json(['status' => 'success', 'message' =>"Album Deleted"]);

            }
            else{
                return response()->json(['status' => 'error', 'message' =>'Error Occured']);
            }

        }

    }
    public function deleteMedia(Request $request){
        if($request->is_wall_post === "true") {
            $post = Post::find($request->post_id);

            if ($request->user()->cannot('delete-post', $post)) {
                return response()->json(['status' => 'error', 'message' =>'Access Denied']);
            }else{
                if($post->delete()){
                    return response()->json(['status' => 'success', 'message' =>"Post Deleted"]);
                }
            }
        }
        else {
        }
    }
    public function likePost(Request $request){

        $post_id = $request->post_id;
        $album_id = AlbumMedia::find($post_id)->album_id;

        $album = UserAlbums::find($album_id);

        $post_owner = $album->user_id;

        $like_data = new MediaLikes();
        $like_data->album_media_id = $post_id;
        $like_data->user_id = $request->user()->id;
        if($like_data->save()) {

            $notification = new NotificationController();

            if($notification->send(NotificationController::NOTIFICATION__LIKE_MEDIA, '', "/".$album->slug, $request->user()->id, $post_owner)) {
                return "success";
            }
            else
                return "failed";
        }



    }
    public function unlikePost(Request $request){
        $post_id = $request->post_id;

        $like_data = MediaLikes::where('album_media_id', $post_id)->where('user_id', $request->user()->id)->delete();
        if($like_data)
            return "success";
        else
            return "failed";


    }
}
