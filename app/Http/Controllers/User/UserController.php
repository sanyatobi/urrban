<?php
/**
 * Created by PhpStorm.
 * User: oluwatobi.okusanya
 * Date: 10/24/15
 * Time: 8:58 PM
 */
namespace App\Http\Controllers\User;
use App\AlbumMedia;
use App\EmailNotifications;
use App\Http\Controllers\Controller;
use App\Http\Controllers\NotificationController;
use App\Post;
use App\User;
use App\UserAlbums;
use App\UserEducation;
use App\UserLanguage;
use App\UserRequest;
use App\UserWebsite;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Tymon\JWTAuth\Facades\JWTAuth;



class UserController extends Controller
{

    public function getFollowing(){
        $users = User::with('following')
            ->find(Auth::user()->id)
            ->following
            ->pluck('id');

        return $users->toArray();
    }
    public function follow(Request $request){

        $user = User::find(Auth::user()->id);

        $user->following()->attach($request->user_id);

        $notification = new NotificationController();

        if ($notification->send(NotificationController::NOTIFICATION__FOLLOW_USER, '', '', Auth::user()->id, $request->user_id)) {
            return "success";
        } else
            return "failed";

    }
    public function unfollow(Request $request){

        $user = User::find(Auth::user()->id);

        if($user->following()->detach($request->user_id)){
            return "success";
        }
        else{
            return "failed";
        }

    }
    public function store(Request $request){

        $user_data = Input::all();

        $auth_pass = $user_data["password"];

        $user = new User();

        if ($user->validate($user_data,'create'))
        {
            $user_data["password"] = Hash::make($user_data["password"]);
            $names = explode(" ",$user_data["name"]);
            $last_name = $names[0];
            $first_name = (isset($names[1])) ? $names[1] : "" ;

            $user_data["username"] = $this->createUsername($last_name,$first_name);

            if(!$user->create($user_data)){
                return response()->json(['status' => 'error', 'message' =>$user->errors()]);
            }

            try {
                // verify the credentials and create a token for the user
                if (! $token = JWTAuth::attempt(["email"=>$user_data["email"],"password"=>$auth_pass])) {
                    return response()->json(['status'=>'error','message' => 'invalid_credentials'], 401);
                }
            } catch (JWTException $e) {
                // something went wrong
                return response()->json(['status'=>'error', 'message' => 'could_not_create_token'], 500);
            }

            // if no errors are encountered we can return a JWT
            return response()->json(["data"=>["token"=>$token,"user_id"=>Auth::user()->id]],200);

        }
        else
        {
            return response()->json(['status' => 'error', 'message' =>$user->errors()],400);

        }
    }

    public function login(Request $request) {

        $email = $request->input('email');
        $password = $request->input('password');

        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt(["email"=>$email,"password"=>$password])) {
                return response()->json(['status'=>'error','message' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['status'=>'error', 'message' => 'could_not_create_token'], 500);
        }

        // if no errors are encountered we can return a JWT
        return response()->json(["data"=>["token"=>$token,"user_id"=>Auth::user()->id]],200);

    }

    public function createUsername($last_name,$first_name){

        $user_name = $last_name.( ($first_name=="") ? "" : ".".$first_name);
        $count = 0;


        while(User::where("username",$user_name)->first()){
            $user_name = $last_name.( ($first_name=="") ? "" : ".".$first_name);
            $count++;
            $user_name = $user_name.".".$count;
        }
        return $user_name;

    }

    public function feedApi(){

        $following = User::with('following')
            ->find(Auth::user()->id)
            ->following
            ->pluck('id');

        $following = $following->push(Auth::user()->id);

        $feeds = Post::whereIn('user_id', $following)
            ->with('owner.interests','likes','comments')
            ->orderBy('updated_at', 'desc')
            ->get();

        return response()->json(["data"=>$feeds],200);

    }

    public function stayOnline(){
        $stay = User::where('id',Auth::user()->id)->update(['updated_at' => date('Y-m-d G:i:s')]);
    }

    public function suggestedContacts(){

        //fetch users that share same interest with user and are not users contacts
        $user_category = Auth::user()->interests;
        $users_contacts = User::with('contacts')
            ->find(Auth::user()->id)
            ->contacts
            ->pluck('id')
            ->toArray();

        $discover_users = User::
        where('interests','=',$user_category)
        ->where('id','<>',Auth::user()->id)
            ->whereNotIn('id',$users_contacts)
            ->get()
            ->toArray();

        return response()->json(["data"=>$discover_users],200);

    }

    public function contacts(){
        //get contacts
        $contacts = User::with('contacts')
            ->find(Auth::user()->id)
            ->contacts
            ->toArray();


        //get contact requests
        $contact_requests = User::with('contact_requests')
            ->find(Auth::user()->id)
            ->contact_requests
            ->toArray();

        $data = ['contacts'=>$contacts,'contact_requests'=>$contact_requests];

        return View::make('user.contacts')->with('data',$data);

    }

    public function gallery($username){

        $albums = User::where('username', $username)
            ->with('albums')
            ->first()
            ->toArray()['albums'];

        foreach($albums as $key=>$album){

            $cover = AlbumMedia::where('album_id',$album['id'])->where('image_url','!=','')->first();
            $image_count = AlbumMedia::where('album_id',$album['id'])->count();
            $albums[$key]['count'] = $image_count;
            if($cover){
                $albums[$key]['cover'] = $cover->toArray();
            }
        }


        $feeds = Post::where('user_id', Auth::user()->id)
            ->where(function ($query) {
                $query->where('image_url', '!=', '')
                    ->orWhere('video_url', '!=', '');
            })
            ->get()
            ->toArray();



        $wall["cover"] = ($feeds[0]['image_url'] != '') ? $feeds[0]['image_url'] : config('constants.DEFAULT_WALL_COVER_IMAGE');

        $wall["count"] = count($feeds);
        $wall["date"] = $feeds[0]['created_at'];

        $data["albums"] = $albums;
        $data["wall"] = $wall;

        return View::make('user.gallery')->with('data', $data);
    }

    public function profile($username){

        $user_data = User::with('interests','country','city','design','website','education','contacts', 'language')
            ->where('username',$username)
            ->first();

        if(!$user_data){
            return redirect()->route('home')->with('message', 'User Not Found');;
        }

        if($username == Auth::user()->username){
            return View::make('user.profile')->with('data',$user_data->toArray());
        }
        else{
            return View::make('user.other_profile')->with('data',$user_data->toArray());
        }


    }
    public function editProfile($username){

        $user_data = User::with('interests','country','city','design','website','education', 'language')
            ->where('username',$username)
            ->first();

        if(!$user_data){
            //handle error

            exit;
        }

        return View::make('user.edit_profile')->with('data',$user_data->toArray());
    }

    public function updateProfile(Request $request){

        $user = User::with('design','website','education','language')
            ->find(Auth::user()->id);

        //save user's designs
        $user->design()->delete();

        $designs = explode(",",$request->design);

        if(strlen($designs[0])) {

            foreach ($designs as $design) {

                $design = new UserDesign(["design" => $design]);


                $user->design()->save($design);
            }
        }
        //end save user's designs

        //save user's websites
        $user->website()->delete();

        $websites = explode(",",$request->website);

        if(strlen($websites[0])) {

            foreach ($websites as $website) {
                $website = new UserWebsite(["website" => $website]);


                $user->website()->save($website);
            }
        }
        //end save user's websites

        //save user's languages
        $user->language()->delete();

        $language = explode(",",$request->language);

        if(strlen($language[0])) {

            foreach ($language as $language) {
                $language = new UserLanguage(["language" => $language]);


                $user->language()->save($language);
            }
        }
        //end save user's languages

        //save user's education
        $user->education()->delete();

        $education = explode(",",$request->education);

        if(strlen($education[0])) {

            foreach ($education as $school) {

                $school = new UserEducation(["education" => $school]);


                $user->education()->save($school);
            }
        }
        //end save user's websites

        //save basic user data
        $user_data = ["headline"=>$request->headline,"interests"=>$request->interest,
            "name"=>$request->name,"gender"=>$request->gender,"phone1"=>$request->phone1,"phone2"=>$request->phone2,
            "dob"=>$request->dob,"country_code"=>$request->country,"city_id"=>$request->city_id,
            "side_gig"=>$request->side_gig,"years_in_business"=>$request->yib,"summary_of_qualification"=>$request->soq,'address'=>$request->address,'engaged_in'=>$request->engaged_in,'height'=>$request->height,'weight'=>$request->weight,'skin'=>$request->skin,'hair'=>$request->hair,'eye'=>$request->eye,'foot'=>$request->foot,'waist'=>$request->waist,'bust'=>$request->bust,'hip'=>$request->hip];

        if($user->validate($user_data,'update')){

            if($user->fill($user_data)->save()){
                return response()->json(['status' => 'success', 'data'=>['message' =>"Your account has been updated"]]);
            }
            else{
                return response()->json(['status' => 'error', 'data'=>['message' =>$user->errors()]]);
            }
        }
        else{
            return response()->json(['status' => 'error', 'message' =>$user->errors()]);

        }
    }

    public function requestcontact(Request $request){

        //add to requests
        $user_request = new UserRequest();
        $user_request->sender_id = Auth::user()->id;
        $user_request->user_id = $request->user_id;
        $user_request->request_type = config('constants.FOLLOWING_CONTACT');
        $user_request->save();
        //send notification
        $notification = new NotificationController();

        if ($notification->send(NotificationController::NOTIFICATION__REQUEST_CONTACT, '', '', Auth::user()->id, $request->user_id)) {
            return response()->json(['status' => 'success', 'data'=>['message' =>"Contact Requested"]]);
        } else
            return response()->json(['status' => 'error', 'data'=>['message' =>'Error Occured']]);
    }

    public function discover(){
        /**
         * Discover page controller
         * Feed search algortithm is based on followers of people I follow
         */
        $following = User::with('following')
            ->find(Auth::user()->id)
            ->following
            ->pluck('id')
            ->toArray();

        $discover_users = "";
        $fof_ids = [];

        if($following) {

            $followers_of_following = User::with('followers')
                ->find($following)
                ->pluck('followers')->toArray();

            $discover_users = $followers_of_following;

            foreach($discover_users as $fof){
                $fof_ids[] = array_pluck($fof, 'id');
            }
        }
        else{
            $discover_users = User::where('id','<>',Auth::user()->id)->get()->toArray();

            foreach($discover_users as $fof){
                $fof_ids[] = $fof['id'];
            }
        }


        $fof_ids = array_flatten($fof_ids);

        // $fof_ids = array_except($fof_ids,Auth::user()->id);

        $fof_ids = array_where($fof_ids, function ($key, $value) {
            return ($value != Auth::user()->id);
        });


        $feeds = Post::whereIn('user_id', $fof_ids)
            ->with('owner.interests','likes','comments')
            ->get();


        return View::make('user.discover')->with('feeds', $feeds->toArray());

    }
    public function settings(){

        $user = User::with('email_notifications')
            ->find(Auth::user()->id);

        $data['user'] = $user;

        $data['email_notifications'] = EmailNotifications::all();

        return View::make('user.settings')->with('data',$data);
    }

    public function changeSettings(Request $request){
        $user = User::with('email_notifications')->find($request->user()->id);

        $user_data = ['username' => $request->username,
            'email' => $request->email,
            'receive_email_notification' => $request->email_notifications,
            'privacy_contact_request' => $request->privacy_contact,
            'privacy_look_up' => $request->privacy_look_up,
            'use_email' => ($request->use_email == "true" ? 1 : 0),
            'use_phone'=>  ($request->use_number == "true" ? 1 : 0)];

        if( ($request->password !='')&&($request->password = $request->password_conf)){
            $user_data['password'] = Hash::make($request->password);
        }
        $user->email_notifications()->detach();

        if(is_array($request->notification_categories)) {
            $notification_categories = array_flatten($request->notification_categories);
            $user->email_notifications()->attach($notification_categories);

        }

        $settings_rules = array(
            'email' => 'required|email',
            'username' => 'unique:users,username,'.$request->user()->id,
            'email' => 'unique:users,email,'.$request->user()->id,
        );

        if($user->validate($user_data,'settings',$settings_rules)) {
            if($user->fill($user_data)->save()){
                return response()->json(['status' => 'success', 'message' => "Settings Changed"]);
            } else {
                return response()->json(['status' => 'error', 'message' => 'Error Occured']);
            }
        }
        else{
            return response()->json(['status' => 'error', 'message' =>$user->errors()]);

        }

    }
    public function createAlbum(Request $request){

        $user = User::find(Auth::user()->id);

        $album = UserAlbums::where('slug',str_slug($request->album_title, "-"))->where('user_id',Auth::user()->id)->first();


        if($album) {
            return response()->json(['status' => 'error', 'message' =>'Album title is not valid']);
        }


        $album = new UserAlbums(["title"=>$request->album_title,"slug"=>str_slug($request->album_title, "-")]);

        if($user->albums()->save($album)){
            return response()->json(['status' => 'success', 'message' => "Album Created"]);
        }
        else{
            return response()->json(['status' => 'error', 'message' =>'Error Occurred']);
        }

    }


    public function messages(){

        return View::make('user.messages');
    }
}
