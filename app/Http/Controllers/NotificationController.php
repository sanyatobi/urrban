<?php
/**
 * Created by PhpStorm.
 * User: oluwatobi.okusanya
 * Date: 10/24/15
 * Time: 8:58 PM
 */
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Notification;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class NotificationController extends Controller
{
    const NOTIFICATION__LIKE_POST = 1;
    const NOTIFICATION__FOLLOW_USER = 2;
    const NOTIFICATION__ADD_CONTACT = 3;
    const NOTIFICATION__REQUEST_CONTACT = 4;
    const NOTIFICATION__LIKE_MEDIA = 5;

    public function showAll(){

        $Allnotifications = Notification::where(['receiver_id'=>Auth::user()->id,"status"=>0])
            ->where('sender_id', '!=' , Auth::user()->id)
            ->where('status', '=', 0)
            ->with('category','sender')
            ->orderBy('created_at', 'desc')
            ->get()
            ->toArray();
        $notifications = [];
        $util = new UtilityController();


        foreach($Allnotifications as $notification){

            $message = $notification["sender"]["name"]." ".$notification["category"]["message"];
            $time = $util->time_elapsed_string($notification['created_at']);

            $notifications[] = [
                "id" => $notification["id"],
                "avatar" => $notification['sender']['avatar'],
                "message" => $message, "time" => $time
            ];
        }

        return response()->json(['status' => 'success', 'message' => $notifications]);
    }

    public function send($category_id,$message="",$action_link="",$sender_id,$receiver_id){

        $notification = new Notification();


        $notification->category_id = $category_id;
        $notification->message = $message;
        $notification->action_link = $action_link;
        $notification->sender_id = $sender_id;
        $notification->receiver_id = $receiver_id;

        $notification_exists = Notification::where('category_id',$category_id)
            ->where('action_link',$action_link)
            ->where('status',0)
            ->where('receiver_id',$receiver_id)
            ->where('sender_id',$sender_id)->first();

        if(!$notification_exists) {

            if ($notification->save()) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        else
            return TRUE;


    }
    function get(){
        $notifications = Notification::where(['receiver_id'=>Auth::user()->id,"status"=>0])
            ->where('sender_id', '!=' , Auth::user()->id)
            ->where('status', '=', 0)
            ->with('category','sender')
            ->get();


        // dd($notifications->toArray());
        $categories = [];
        $action_ids = [];
        $notifications = $notifications->toArray();
        $parsed_notifications = [];
        $count = [];


        foreach($notifications as $notification){
            $parsed_notifications[$notification["category_id"].$notification["action_link"]]["other"]["name"] = "";
            $parsed_notifications[$notification["category_id"].$notification["action_link"]]["id"] = $notification["id"];
            if( (in_array($notification["category_id"],$categories))&&(in_array($notification["action_link"],$action_ids)) ){
                $count[$notification["category_id"].$notification["action_link"]] = (isset($count[$notification["category_id"].$notification["action_link"]])) ? $count[$notification["category_id"].$notification["action_link"]]+ 1 : 1;
                $parsed_notifications[$notification["category_id"].$notification["action_link"]]["other"]["name"] = " and ".$count[$notification["category_id"].$notification["action_link"]]." other ".(($count[$notification["category_id"].$notification["action_link"]] > 1 )?"friends":"friend");
            }
            else{
                array_push($categories,$notification["category_id"]);
                array_push($action_ids,$notification["action_link"]);

                $parsed_notifications[$notification["category_id"].$notification["action_link"]]["sender"]["avatar"] = $notification["sender"]["avatar"];
                $parsed_notifications[$notification["category_id"].$notification["action_link"]]["message"] = $notification["message"];
                $parsed_notifications[$notification["category_id"].$notification["action_link"]]["sender"]["name"] = $notification["sender"]["name"];
                $parsed_notifications[$notification["category_id"].$notification["action_link"]]["category"]["message"] = $notification["category"]["message"];
                $parsed_notifications[$notification["category_id"].$notification["action_link"]]["category"]["action_url"] = $notification["category"]["action_url"];
                $parsed_notifications[$notification["category_id"].$notification["action_link"]]["action_link"] = $notification["action_link"];
            }

        }


        return $parsed_notifications;
    }

    function getCount(){

        $notifications = Notification::where(['receiver_id'=>Auth::user()->id,"status"=>0])
            ->where('sender_id', '!=' , Auth::user()->id)
            ->where('status', '=', 0)
            ->count();

        if (isset($notifications)) {
            return response()->json(["data"=>['status' => 'success', 'count' => $notifications]]);
        }
        return response()->json(["data"=>['status' => 'error', 'message' => 'Error Occurred']]);
    }

    function flag(Request $request)
    {
        $id = $request->id;
        $all = $request->all;

        if ($all) {
            $notifications = Notification::where("receiver_id", Auth::user()->id)->update(['status' => 1]);

            if ($notifications) {
                    return response()->json(['status' => 'success']);
                }
                return response()->json(['status' => 'error', 'message' => 'Error Occurred']);
            }
        else {

            $notification = Notification::where("id", $id)->with('category')->first();


            if ($notification) {
                $flag = $notification->update(['status' => 1]);

                if ($flag) {

                    $link = "";

                    switch($notification->category_id){

                        case self::NOTIFICATION__LIKE_MEDIA:
                        $link = Auth::user()->username."/".$notification->category->action_url . $notification->action_link;
                            break;

                        case self::NOTIFICATION__LIKE_POST:
                            $link = $notification->category->action_url . $notification->action_link;
                            break;

                        case self::NOTIFICATION__REQUEST_CONTACT:
                        case self::NOTIFICATION__ADD_CONTACT:
                        case self::NOTIFICATION__FOLLOW_USER:
                            $link = Auth::user()->username."/".$notification->category->action_url;
                            break;

                    }

                    return response()->json(['status' => 'success', 'link' => $link]);
                }
                return response()->json(['status' => 'error', 'message' => 'Error Occurred']);
            } else {
                return response()->json(['status' => 'error', 'message' => 'Notification not found']);
            }
        }
    }

}
