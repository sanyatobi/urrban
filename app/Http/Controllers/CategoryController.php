<?php
/**
 * Created by PhpStorm.
 * User: oluwatobi.okusanya
 * Date: 23/03/16
 * Time: 3:31 PM
 */

namespace App\Http\Controllers;

use App\Categories;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    function index(){
        $categories = Categories::all();
        return response()->json(["data"=>$categories],200);
    }
    function get(){
        $categories = Categories::all();
        return $categories;
    }
}