<?php
/**
 * Created by PhpStorm.
 * User: oluwatobi.okusanya
 * Date: 10/24/15
 * Time: 8:58 PM
 */
namespace App\Http\Controllers\Feed;
use App\Categories;
use App\Http\Controllers\Controller;
use App\Http\Controllers\NotificationController;
use App\Likes;
use App\Notification;
use App\Post;
use App\Posts;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class FeedController extends Controller
{
    public function index() {

        return View::make('user.feed');
    }

    public function jsonFeeds() {

        $following = User::with('following')
            ->find(Auth::user()->id)
            ->following
            ->pluck('id');

        $following = $following->push(Auth::user()->id);

        $feeds = Post::whereIn('user_id', $following)
            ->with('owner.interests','likes','comments')
            ->orderBy('updated_at', 'desc')
            ->get();

        return response()->json(["data"=>['status' => 'success', 'message' => $feeds->toArray()]]);

    }

    public function viewPost($id,$slug){

        $post = Post::with('owner.interests','comments','likes')
            ->find($id);
        $post->slug = $slug;

        $post = $post->toArray();

/*        dd($post);*/



        return View::make('view_post')->with('post',$post);
    }

    public function related($category,$post_id){

        $category_data = Categories::where('id',$category)->first();

        $category_id = $category_data->id;
        $category_title = $category_data->title;

        $category_users = User::where('interests',$category_id)->get()->pluck('id');



        $feeds = Post::whereIn('user_id', $category_users)
            ->with('owner.interests','likes','comments')
            ->where(function ($query) {
                $query->where('image_url', '!=', '')
                    ->orWhere('video_url', '!=', '');
            })
            ->orderByRaw('RAND()')
            ->where('id','<>',$post_id)
            ->limit(3)
            ->get();

        //return View::make('category')->with('feeds', $feeds->toArray())->with('category',$category_title);

        return response()->json(["data"=>['status' => 'success', 'message' => $feeds->toArray()]]);


    }
    public function categories($slug){

        $category_data = Categories::where('slug',$slug)->first();

        $category_id = $category_data->id;
        $category_title = $category_data->title;

        $category_users = User::where('interests',$category_id)->get()->pluck('id');



        $feeds = Post::whereIn('user_id', $category_users)
            ->with('owner.interests','likes','comments')
            ->get();

        return View::make('category')->with('feeds', $feeds->toArray())->with('category',$category_title);

    }

    public function getPosts(){

        $following = User::with('following')
            ->find(Auth::user()->id)
            ->following
            ->pluck('id');

        $feeds = Post::whereIn('user_id', $following)
            ->with('owner','likes','comments')
            ->get();;


        return response()->json(['status' => 'success', 'message' =>$feeds->toArray()]);
    }
    public function likePost(Request $request){


    $post_id = $request->post_id;
        $post = Post::find($post_id);
        $post_owner = $post->user_id;

        $pieces = explode(" ", $post->content);
        $first_part = implode(" ", array_splice($pieces, 0, 10));
        $post_slug = str_slug($first_part,'-');


        $like_data = new Likes();
        $like_data->post_id = $post_id;
        $like_data->user_id = Auth::user()->id;
if($like_data->save()) {

    $notification = new NotificationController();

    if($notification->send(NotificationController::NOTIFICATION__LIKE_POST, '', "/".$post_id."/".$post_slug, Auth::user()->id, $post_owner)) {
        return "success";
    }
else
    return "failed";
}



    }
    public function unlikePost(Request $request){
        $post_id = $request->post_id;

        $like_data = Likes::where('post_id', $post_id)->where('user_id', Auth::user()->id)->delete();
        if($like_data)
            return "success";
        else
            return "failed";
    }
    public function sendPost(Request $request){
        $allowed_mime = ["image","video"];

        if ($request->has('post_content')){
            $post_content = $request->post_content;

            $post_data = new Post();
            $post_data->user_id = Auth::user()->id;
            $post_data->category = Auth::user()->interests;
            $post_data->content = $post_content;

            if( ($request->hasFile('file'))&& ($request->file('file')->isValid()) ){
                $file_mime = substr($request->file('file')->getMimeType(),0,5);
                if(in_array($file_mime,$allowed_mime)){

                    $file_name = Auth::user()->id."-".time().".".$request->file('file')->getClientOriginalExtension();
                    if(Storage::put('feed_media/'.$file_name, file_get_contents($request->file('file')))){
                        //store post data
                        $post_data->image_url = ($file_mime == "image") ? $file_name : "";
                        $post_data->video_url = ($file_mime == "video") ? $file_name : "";
                    }
                    else{
                        return response()->json(['status' => 'error', 'message' =>"Error Occurred, Could not upload file"]);
                    }
                }
                else{
                    return response()->json(['status' => 'error', 'message' =>"Error Occurred, Invalid File Type"]);
                }
            }
                if($post_data->save()){
                    return response()->json(['status' => 'success', 'message' =>"Post Saved"]);
                }
            else{
                return response()->json(['status' => 'error', 'message' =>"Could not save post"]);
            }
        }
        else{
            return response()->json(['status' => 'error', 'message' =>"Error Occurred, Action could not be completed"]);
        }
    }
}
