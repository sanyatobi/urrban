<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Middleware\BaseMiddleware;

class JwtWeb extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        if (! $token = $request->cookie('urrbanq-token')) {
            return redirect()->guest('login');
        }

        try {
            $user = $this->auth->authenticate($token);
        } catch (TokenExpiredException $e) {
            return redirect()->guest('login');
        } catch (JWTException $e) {
            return redirect()->guest('login');
        }

        if (! $user) {
            return redirect()->guest('login');
        }

        $this->events->fire('tymon.jwt.valid', $user);

        return $next($request);
    }
}

