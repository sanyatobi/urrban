<?php

Route::get('/login', function () {
    return view('login');
});

Route::group(['middleware' => ['jwt.web']], function()
{
    Route::get('/feed', array(
        'as'=>'home',
        'uses' => 'Feed\FeedController@index'
    ));
    Route::post('/feed', array(
        'as' => 'send-post',
        'uses' => 'Feed\FeedController@sendPost'
    ));
    Route::get('/discover', array(
        'as' => 'discover',
        'uses' => 'User\UserController@discover'
    ));
    Route::post('/gallery', array(
        'as' => 'send-media',
        'uses' => 'GalleryController@sendMedia'
    ));
    Route::delete('/gallery/media', array(
        'as' => 'delete-media',
        'uses' => 'GalleryController@deleteMedia'
    ));
    Route::delete('/gallery', array(
        'as' => 'delete-album',
        'uses' => 'GalleryController@deleteAlbum'
    ));
    Route::post('/user/follow', array(
        'as' => 'follow-user',
        'uses' => 'User\UserController@follow'
    ));
    Route::post('/user/unfollow', array(
        'as' => 'unfollow-user',
        'uses' => 'User\UserController@unfollow'
    ));
    Route::post('/user/addcontact', array(
        'as' => 'add-user',
        'uses' => 'User\UserController@addcontact'
    ));
    Route::post('/user/removecontact', array(
        'as' => 'remove-user',
        'uses' => 'User\UserController@removecontact'
    ));
    Route::post('/user/requestcontact', array(
        'as' => 'request-user',
        'uses' => 'User\UserController@requestcontact'
    ));
    Route::post('/{username}/settings', array(
        'as' => 'settings',
        'uses' => 'User\UserController@changeSettings'
    ));
    Route::get('/', array(
        'uses' => 'Feed\FeedController@index'
    ));
    Route::get('/post/{id}/{slug}', array(
        'as' => 'view-post',
        'uses' => 'Feed\FeedController@viewPost'
    ));
    Route::put('/notifications/flag', array(
        'as' => 'notifications-flag',
        'uses' => 'NotificationController@flag'
    ));
    Route::post('/feed/unlike', array(
        'as' => 'unlike-post',
        'uses' => 'Feed\FeedController@unlikePost'
    ));
    Route::post('/feed/like', array(
        'as' => 'like-post',
        'uses' => 'Feed\FeedController@likePost'
    ));

    Route::get('/categories/{slug}', array(
        'as' => 'categories',
        'uses' => 'Feed\FeedController@categories'
    ));
    Route::get('gallery_media/{filename}', function ($filename)
    {
        $path ='gallery_media/' . $filename;

        $file = \Illuminate\Support\Facades\Storage::get($path);
        $type = \Illuminate\Support\Facades\Storage::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    });
    Route::get('media_feed/{filename}', function ($filename)
    {
        $path = 'feed_media/' . $filename;

        $file = \Illuminate\Support\Facades\Storage::get($path);
        $type = \Illuminate\Support\Facades\Storage::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    });
    Route::get('media_avatar/{filename}', function ($filename)
    {

        $path = 'avatars/' . $filename;

        $file = \Illuminate\Support\Facades\Storage::get($path);
        $type = \Illuminate\Support\Facades\Storage::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    });
    Route::get('/messages', array(
        'as' => 'messages',
        'uses' => 'User\UserController@messages'
    ));
    Route::post('/messages', array(
        'as' => 'send-message',
        'uses' => 'MessageController@create'
    ));
    Route::get('/messages/all', array(
        'as' => 'all-messages',
        'uses' => 'MessageController@all'
    ));
    Route::get('/messages/conversation/{id}', array(
        'as' => 'conversations',
        'uses' => 'MessageController@conversation'
    ));
    Route::get('/logout', array(
        'as' => 'logout',
        'uses' => 'Auth\AuthController@logout'
    ));
    Route::get('/{username}', array(
        'as' => 'profile',
        'uses' => 'User\UserController@profile'
    ));
    Route::get('/{username}/edit', array(
        'as' => 'profile',
        'uses' => 'User\UserController@editProfile'
    ));
    Route::get('/{username}/contacts', array(
        'as' => 'profile',
        'uses' => 'User\UserController@contacts'
    ));
    Route::post('/{username}/gallery', array(
        'as' => 'create-album',
        'uses' => 'User\UserController@createAlbum'
    ));
    Route::get('/{username}/gallery', array(
        'as' => 'galley',
        'uses' => 'User\UserController@gallery'
    ));
    Route::get('/{username}/gallery/{slug}', array(
        'as' => 'view-album',
        'uses' => 'GalleryController@viewAlbum'
    ));
    Route::get('/{username}/settings', array(
        'as' => 'settings',
        'uses' => 'User\UserController@settings'
    ));
});


Route::group(['prefix' => 'api/v1','middleware' => ['jwt.auth']], function()
{

    Route::get('/token/verify', [
        function () {
            $token = JWTAuth::getToken();
            $user = JWTAuth::toUser($token);

            return Response::json([
                'data' => [
                    'email' => $user->email,
                    'registered_at' => $user->created_at->toDateTimeString()
                ]
            ]);
        }
    ]);

    Route::get('users/{id}/contacts/suggested',['uses'=>'User\UserController@suggestedContacts']);
    Route::post('users/requestcontact',['uses'=>'User\UserController@requestcontact']);
    Route::put('/{username}/edit', ['as' => 'profile', 'uses' => 'User\UserController@updateProfile']);
    Route::get('/stayonline', ['as' => 'stay-online', 'uses' => 'User\UserController@stayOnline']);
    Route::get('/messages/unread', ['as' => 'unread-messages', 'uses' => 'MessageController@unread']);
    Route::get('/notifications/count', ['as' => 'notifications-count', 'uses' => 'NotificationController@getCount']);
    Route::get('/posts/related/{id}/{post_id}', ['uses' => 'Feed\FeedController@related']);
    Route::get('/feed', ['uses' => 'Feed\FeedController@jsonFeeds']);

});


Route::group(['prefix' => 'api/v1'], function()
{

    Route::get('/utility/categories', array(
        'uses' => 'CategoryController@index'
    ));

    Route::post('/user/login', array(
        'uses' => 'User\UserController@login'
    ));

    Route::resource('user', 'User\UserController');

});


