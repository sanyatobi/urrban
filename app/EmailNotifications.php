<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailNotifications extends Model
{
    public function owner(){
        return $this->belongsToMany('App\User','user_email_notifications','email_notification_id','user_id');
    }
    //
}
