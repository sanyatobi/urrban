<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AlbumMedia extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    //
    public function likes(){
        return $this->hasMany('App\MediaLikes');
    }

    public function comments(){
        return $this->hasMany('App\MediaComments');
    }
}
