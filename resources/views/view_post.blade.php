<?php
/**
 * Created by PhpStorm.
 * User: oluwatobi.okusanya
 * Date: 07/12/15
 * Time: 6:11 AM
 */
        ?>
        <!DOCTYPE html>
<html>

<head>

    @include('incs.imports_top')


    <title>UrbanQ -  <?php echo $post['slug']?></title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

</head>

<body>
<div id="panel_con">
    @include('incs.head_navigation')



    <div class="site_wrapper simple-post">
        <div class="main_wrapper">
            <div class="content_wrapper">
                <div class="container simple-post-container">
                    <div class="content_block right-sidebar row">
                        <div class="fl-container hasRS">
                            <div class="row">
                                <div class="posts-block simple-post">
                                    <div class="contentarea">
                                        <div class="row">
                                            <div class="span12 module_cont module_blog module_none_padding module_blog_page">
                                                <div class="blog_post_page blog_post_preview has_pf">

                                                    <button class="like_botton notliked but_toggler" ><span class="fa fa-heart"></span></button>

                                                    <button class="add_contact_button notadded  but_toggler" ><span class="fa fa-user-plus"></span></button>

                                                    <div class="preview_top">
                                                        <div class="preview_title">
                                                            <div class="listing_meta"> <span class="middot">&middot;</span> <span>{{date("F j, Y",strtotime($post["updated_at"]))}}</span> <span class="middot">&middot;</span> <span><a href="category.php" rel="category tag">{{$post["owner"]["interests"]["title"]}}</a></span> <span class="middot">&middot;</span> <!--span><a href="#comments">{{count($post["comments"])}} comment(s)</a></span--> </div>
                                                        </div>
                                                        <div class="preview_likes gallery_likes_add"> <i class="fa fa-heart"></i> <span>{{count($post["likes"])}}</span> </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                    @if($post['image_url'])
                                                    <div class="pf_output_container"> <img class="featured_image_standalone" src="{{url("media_feed/".$post["image_url"])}}" alt="" /> </div>
                                                    @endif
                                                    @if($post['video_url'])
                                                        <div class="pf_output_container vodeo_grid">
                                                            <video controls>
                                                                <source src="{{url("media_feed/".$post["video_url"])}}" type="video/mp4" id="video-id"  >
                                                                Your browser does not support the video tag.
                                                            </video>
                                                        </div>
                                                    @endif
                                                    <div class="blog_post_content">
                                                        <article class="contentarea">
                                                      {{$post["content"]}}
                                                        </article>
                                                    </div>



                                                </div>



                                                <div class="blog_post_page blog_post_preview has_pf">
                                                    <div class="sidepanel widget_posts">
                                                        <ul class="recent_posts">
                                                            <li>
                                                                <span class="fa fa-user-plus add_b"></span>
                                                                <a class="post_title read_more" href="{{ url($post["owner"]["username"]) }}">
                                                                    <div class="recent_posts_img"><img src="{{url("media_avatar/".$post["owner"]["avatar"])}}"></div>
                                                                    <div class="recent_posts_content"> {{$post["owner"]["name"]}}
                                                                        <br class="clear"> <span>{{$post["owner"]["headline"]}}</span> </div>
                                                                    <div class="clear"></div>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="blogpost_footer">
                                                        <div class="blogpost_share"> <a href="javascript:void(0)" class="top_socials share_facebook"><i class="stand_icon icon-facebook-square"></i>&nbsp;&nbsp;Facebook</a> <a href="javascript:void(0)" class="top_socials share_pinterest"><i class="stand_icon icon-pinterest"></i>&nbsp;&nbsp;Pinterest</a> <a href="javascript:void(0)" class="top_socials share_tweet"><i class="stand_icon icon-twitter"></i>&nbsp;&nbsp;Twitter</a> <a href="javascript:void(0)" class="top_socials share_gplus"><i class="icon-google-plus-square"></i>&nbsp;&nbsp;Google+</a>
                                                            <div class="clear"></div>
                                                        </div>
                                                        <!--div-- class="blogpost_tags"> <span><a href="javascript:void(0)">events</a><a href="javascript:void(0)">new york</a><a href="javascript:void(0)">places</a><a href="javascript:void(0)">streets</a><a href="javascript:void(0)">video</a></span>
                                                            <div class="clear"></div>
                                                        </div-->
                                                    </div>
                                                </div>



                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="span12 single_post_module module_cont module_small_padding featured_items single_feature featured_posts">
                                                <div class="featured_items">

                                                    <h5 id="relatedPostsHeader" style="display: none" class="headInModule postcomment">Also from this category:</h5>
                                                    <div class="items3 featured_posts">
                                                        <ul class="item_list" id="relatedPosts">



                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="disqus_thread"></div>
                                        <script>
                                            /**
                                             * RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                                             * LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
                                             */

                                             var disqus_config = function () {
                                             this.page.url = "<?php echo \Illuminate\Support\Facades\URL::current() ?>";
                                             this.page.identifier = "<?php echo $post['id']?>";
                                             };

                                            (function() { // DON'T EDIT BELOW THIS LINE
                                                var d = document, s = d.createElement('script');

                                                s.src = '//urrbanq.disqus.com/embed.js';

                                                s.setAttribute('data-timestamp', +new Date());
                                                (d.head || d.body).appendChild(s);
                                            })();
                                        </script>
                                        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
                                        <!--div class="row">
                                            <div class="span12">
                                                <div id="comments">
                                                    <h5 class="headInModule postcomment">4 Comments:</h5>
                                                    <div id="respond" class="comment-respond">
                                                        <h3 id="reply-title" class="comment-reply-title txt_capitalize">Leave a Comment!</h3>
                                                        <form action="javascript:void(0)" method="post" id="commentform" class="comment-form">

                                                            <label class="label-message"></label>
                                                            <textarea name="comment" cols="45" rows="5" placeholder="Message..." id="comment-message" class="form_field"></textarea>

                                                            <p class="form-submit">
                                                                <input name="submit" type="submit" id="submit" value="Post Comment" /> </p>
                                                        </form>
                                                    </div>

                                                    <ol class="commentlist">
                                                        <li class="comment">
                                                            <div class="stand_comment">
                                                                <div class="commentava wrapped_img"> <img alt="" src="img/imgs/avatar1.jpg" class="avatar" />
                                                                    <div class="img_inset"></div>
                                                                </div>
                                                                <div class="thiscommentbody">
                                                                    <div class="comment_content">
                                                                        <div class="comment_box">
                                                                            <p>Really nice looking photo post!</p><span class="comments"><a class="comment-reply-link" href="javascript:void(0)"></a></span> </div>
                                                                    </div>
                                                                    <div class="comment_info"> <span class="author_name">Tom White </span> <span class="middot">&middot;</span> <span class="comment_date">December 26, 2020</span> </div>
                                                                    <!--       <div id="respond" class="comment-respond reply_box">
                                                                               <h3 id="reply-title" class="comment-reply-title txt_capitalize">Reply</h3>
                                                                               <form action="javascript:void(0)" method="post" id="commentform" class="comment-form">

                                                                                   <label class="label-message"></label>
                                                                                   <textarea name="comment" cols="45" rows="5" placeholder="Message..." id="comment-message" class="form_field"></textarea>

                                                                                   <p class="form-submit">
                                                                                       <input name="submit" type="submit" id="submit" value="Reply" /> </p>
                                                                               </form>
                                                                           </div>-->
                                                                <!--/div>
                                                            </div>
                                                            <ul class="children">
                                                                <li class="comment">
                                                                    <div class="stand_comment">
                                                                        <div class="commentava wrapped_img"> <img alt="" src="img/imgs/avatar_gt3.jpg" class="avatar" />
                                                                            <div class="img_inset"></div>
                                                                        </div>
                                                                        <div class="thiscommentbody">
                                                                            <div class="comment_content">
                                                                                <div class="comment_box">
                                                                                    <p>Thanks a lot for your kind words!</p><span class="comments"><a class="comment-reply-link" href="javascript:void(0)"></a></span> </div>
                                                                            </div>
                                                                            <div class="comment_info"> <span class="author_name">Daniello </span> <span class="middot">&middot;</span> <span class="comment_date">December 26, 2020</span> </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="comment">
                                                            <div class="stand_comment">
                                                                <div class="commentava wrapped_img"> <img alt="" src="img/imgs/avatar2.jpg" class="avatar" />
                                                                    <div class="img_inset"></div>
                                                                </div>
                                                                <div class="thiscommentbody">
                                                                    <div class="comment_content">
                                                                        <div class="comment_box">
                                                                            <p>Amazing duuude. Cant believe you pulled this off <img src="img/imgs/icon_wink.gif" alt=";)" class="wp-smiley" /></p><span class="comments"><a class="comment-reply-link" href="javascript:void(0)"></a></span> </div>
                                                                    </div>
                                                                    <div class="comment_info"> <span class="author_name">John Doe </span> <span class="middot">&middot;</span> <span class="comment_date">December 26, 2020</span> </div>
                                                                </div>
                                                            </div>
                                                            <ul class="children">
                                                                <li class="comment">
                                                                    <div class="stand_comment">
                                                                        <div class="commentava wrapped_img"> <img alt="" src="img/imgs/avatar_gt3.jpg" class="avatar" />
                                                                            <div class="img_inset"></div>
                                                                        </div>
                                                                        <div class="thiscommentbody">
                                                                            <div class="comment_content">
                                                                                <div class="comment_box">
                                                                                    <p>Thank you!</p><span class="comments"><a class="comment-reply-link" href="javascript:void(0)"></a></span> </div>
                                                                            </div>
                                                                            <div class="comment_info"> <span class="author_name">Daniello </span> <span class="middot">&middot;</span> <span class="comment_date">December 26, 2020</span> </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ol>

                                                </div>
                                            </div>
                                        </div-->
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        @include('incs.posts_side_bar')

                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    @include('incs.footer_stuffs')



</div>



<!--    notification panel slides-->


@include('incs.notification_panels')


        <!--    end notification panel slides-->


@include('incs.imports_bottom')
<script>
    relatedPosts(<?php echo $post['category']?>,<?php echo $post['id']?>);
</script>
</body>

</html>
