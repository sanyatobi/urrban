<div class="left-sidebar-block">
                                	<div class="sidepanel widget_posts profile_m_sidep">
                                   	 	<ul class="recent_posts">
                                                <li>
                                                <a class="post_title read_more profile_m" href="{{url(\Illuminate\Support\Facades\Auth::user()->username)}}">
                                                    <div class="recent_posts_img">                    <img src="{{url("media_avatar/".Auth::user()->avatar)}}" />
                                                    </div>
                                                    <div class="recent_posts_content">                        {{\Illuminate\Support\Facades\Auth::user()->name}}

                                                        <br class="clear"> <span>{{\Illuminate\Support\Facades\Auth::user()->headline}}
</span> </div>
                                                    <div class="clear"></div>
                                                 </a>
                                                </li>
                                             </ul>
                                       </div>
                                    <div class="sidepanel widget_categories prof_sidebar_links">
                                    
                                        <ul>
                                            <li><a href="{{url(\Illuminate\Support\Facades\Auth::user()->username)}}">Profile</a></li>
                                            <li><a href="{{url(\Illuminate\Support\Facades\Auth::user()->username)}}/contacts">Contacts</a></li>
                                            <!--<li class="widj_res_ap">  <a href="gallery_albums.php">Gallery</a></li>-->
                                            <li class="">  <a href="{{url(\Illuminate\Support\Facades\Auth::user()->username."/gallery")}}">Gallery</a></li>
                                           <!-- <li><a href="favorites.php">Favourites</a></li>-->
                                            <li><a href="{{url(\Illuminate\Support\Facades\Auth::user()->username."/settings")}}">Settings</a></li>
                                        </ul>
                                    </div>
                                    
                                    
                                    <!--div-- class="sidepanel widget_flickr widj_res_disp">
                                        <h6 class="sidebar_header">My Photostream</h6>
                                        <div class="flickr_widget_wrapper">
                                        <div class="flickr_badge_image">
                                            <a><img alt="" src="img/flickr/1.jpg">
                                                <div class="flickr_fadder"></div>
                                            </a>
                                        </div>
                                        <div class="flickr_badge_image">
                                            <a ><img alt="" src="img/flickr/2.jpg">
                                                <div class="flickr_fadder"></div>
                                            </a>
                                        </div>
                                        <div class="flickr_badge_image">
                                            <a ><img alt="" src="img/flickr/3.jpg">
                                                <div class="flickr_fadder"></div>
                                            </a>
                                        </div>
                                        <div class="flickr_badge_image">
                                            <a ><img alt="" src="img/flickr/4.jpg">
                                                <div class="flickr_fadder"></div>
                                            </a>
                                        </div>
                                        <div class="flickr_badge_image">
                                            <a ><img alt="" src="img/flickr/5.jpg">
                                                <div class="flickr_fadder"></div>
                                            </a>
                                        </div>
                                        <div class="flickr_badge_image">
                                            <a ><img alt="" src="img/flickr/6.jpg">
                                                <div class="flickr_fadder"></div>
                                            </a>
                                        </div>
                                        <div class="flickr_badge_image">
                                            <a ><img alt="" src="img/flickr/7.jpg">
                                                <div class="flickr_fadder"></div>
                                            </a>
                                        </div>
                                        <div class="flickr_badge_image">
                                            <a ><img alt="" src="img/flickr/8.jpg">
                                                <div class="flickr_fadder"></div>
                                            </a>
                                        </div>
                                        <div class="flickr_badge_image">
                                            <a ><img alt="" src="img/flickr/9.jpg">
                                                <div class="flickr_fadder"></div>
                                            </a>
                                        </div>
                                        <div class="flickr_badge_image">
                                            <a ><img alt="" src="img/flickr/10.jpg">
                                                <div class="flickr_fadder"></div>
                                            </a>
                                        </div>
                                      
                                        </div>
                                         <div class="more_marg">
                                        <a href="{{ url(\Illuminate\Support\Facades\Auth::user()->username."/gallery") }}" class="hasIcon shortcode_button btn_small btn_type5"> <i class=" fa fa-image ic_t2"></i> View More </a>
                                       </div>
                                    </div-->
                                    
                                   <!--         
                                   <div class="sidepanel widget_text widj_res_disp">
                                        <h6 class="sidebar_header">Text Widget</h6>
                                        <div class="textwidget">Nam nunc massa, molestie vel sollicitudin sit amet consectetur a risus phasellus tristique diam nisl, a <a href="javascript:void(0)">bibendum urna,</a> venenatis varius nullam congue, pellentesque ipsum, vel ornare ligula.</div>
                                    </div>
                                    -->
                                </div>