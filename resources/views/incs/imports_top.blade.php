 
    <link href="{{asset('js/flag/bootstrap.min.css')}}" rel='stylesheet' type='text/css' />
    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
    <link rel="apple-touch-icon" href="{{asset('img/apple_icons_57x57_2.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('img/apple_icons_72x72_2.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('img/apple_icons_114x114_2.png')}}">
  <link rel="stylesheet" href="{{asset('css/theme.css')}}" type="text/css" media="all" />
    <link rel="stylesheet" href="{{asset('css/responsive_2.css')}}" type="text/css" media="all" />
    <link rel="stylesheet" href="{{asset('css/custom_2.css')}}" type="text/css" media="all" />
    <link rel="stylesheet" href="{{asset('css/base.css')}}" type="text/css" media="all" />
    <link href="{{asset('css/font.css')}}" rel='stylesheet' type='text/css' />
    <link rel="stylesheet" type="text/css" href="{{asset('css/msdropdown/dd.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/msdropdown/flags.css')}}" />


<!--    accordion-->
<link rel="stylesheet" href="{{asset('js/acc/smk-accordion.css')}}" />
<!--    accordion-->

<!--    qtip-->
    <link href="{{asset('js/qtip/jquery.qtip.css')}}" rel='stylesheet' type='text/css' />
    
<!--    qtip-->
    
<!--notifier-->
  <link type="text/css" rel="stylesheet" href="{{asset('js/notif/jquery.toast.css')}}"/>
<!--notifier-->

	<link rel="stylesheet" type="text/css" href="{{asset('js/pop_gall/source/jquery.fancybox.css')}}" media="screen" />
    
    <link href="{{asset('js/flag/flags.css')}}" rel="stylesheet">
    
    <link href="{{asset('js/tagger/bootstrap-tokenizer.css')}}" rel="stylesheet">
    <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/utilities.js')}}"></script>
