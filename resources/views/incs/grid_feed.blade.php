<div class="fullscreen_block">
    <div class="full_loader"></div>

    <!--    post area-->
    <div class="row post_bg">
    	<div class="post_post_but_row">
        	<div class="postarea_post_but bg_o">
             	<i class="fa  fa-pencil-square"></i>
        		Post to feed
            </div>
        </div>
        <div class="post_bg_inner">
              <div class="post_area_margineer">
                    <div class="post_act_area">
                            <div class="post_box_con">
                                <textarea class="post_box_enter" id="post_content_feed"></textarea>
                               <!--  <span class="upload_med_button fa fa-camera-retro"></span>  -->
                            </div>
                        <span class="dropzone-previews" id="uploadPreview"></span>

                        <div id="post_file" class="post_box_upload_box">
                                <span   src="{{url("img/upload.jpg")}} class="noimg"></span>
                                <div  class="text">
                                    Upload Media <br />
                                    ---
                                </div>
                                <span class="close fa fa-times"></span>
                            </div>
                            
                            <div class="post_box_upload_box_button_row">
                                <button id="share" class="post_button shortcode_button btn_small btn_type5">Share</button>
                            <!--    <a href="javascript:void(0)" class="hasIcon shortcode_button btn_small btn_type5"><i class="icon-cog"></i>Colored</a>-->
                            </div>
                        </div>
                </div>
        </div>
    
    </div>
    
<!--    post area end-->

    <div class="fs_blog_module is_masonry this_is_blog" id="feed_area">


    </div>
    </div>
    
    