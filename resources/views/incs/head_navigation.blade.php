<header class="main_header">
        <div class="header_wrapper">
            <a href="/" class="logo"><img src="{{asset('img/logo_.png')}}" alt="" class="logo_def"></a>
            
            <div class="top_icy_set">
            
            
            <div class="header_who_con">
                <div class="header_who_pic_con">
                    <img src="" id="users_avatar" />
                </div>
                <div class="header_who_menu_list_popup">
                	<div class=" margineer">
                    	<div class="titley">
                        {{\Illuminate\Support\Facades\Auth::user()->name}}
                        </div>
                        <a href="{{url(\Illuminate\Support\Facades\Auth::user()->username)}}"> <span class="fa fa-user"></span> Profile</a>
                        <a href="{{url(\Illuminate\Support\Facades\Auth::user()->username."/contacts")}}"><span class="fa fa-file-text-o"></span> Contacts</a>
                        <a href="{{url(\Illuminate\Support\Facades\Auth::user()->username."/gallery")}}"><span class="fa  fa-image"></span> Gallery</a>
                        <a href="{{url(\Illuminate\Support\Facades\Auth::user()->username."/settings")}}"> <span class="fa fa-cog"></span>  Settings</a>
                        <a href="" id="logoutButton"> <span class="fa fa-sign-out"></span> Logout</a>
                    </div>
                </div>
            </div>
            
            <div class="header_ics_con_cell">
            	<a href="#search-panel" class="fa fa-search global_notifs side_slider resp_search_mob">
                </a>
            	<a href="#notif-panel" class="fa fa-globe global_notifs side_slider">
                	<div class="header_ics_con_cell_poper" id="notification_count"></div>
                </a>
            	<a href="#msg-notif-panel" class="fa fa-envelope-o global_messages side_slider">
                	<div class="header_ics_con_cell_poper" id="messages_count"></div>
                </a>
            </div>
            
            <div class="header_searchbar_con resp_search_full">
            	<div class="header_searchbar_inner_con">
                	<div class="header_searchbar_con_advanced_link_con">
                    	<a href="javascript:void(0)">Advanced Search</a>
                    </div>
                    <input type="text" placeholder="Search" />
                    <a href="search.php"><button class="fa fa-search" ></button></a>
                </div>
            </div>
            
            </div>
            
            
            <nav>
                <div class="menu-main-menu-container">
                    <ul id="menu-main-menu" class="menu">
                        <li class="menu-item-has-children"> <a href="javascript:void(0)">Categories</a>
                            <ul class="sub-menu">
                                <?php
                                    $categories = new \App\Http\Controllers\CategoryController();
                                    $categories = $categories->get();
                                ?>
                                @foreach ($categories as $category)
                                    <li><a href="/categories/{{$category->slug}}">{{$category->title}}</a> </li>
                                @endforeach
                            </ul>
                        </li>
                       
                        <li class="menu-item-has-children"> <a href="{{ URL::route('discover') }}">Discover</a>
                    </ul>
                </div>
            </nav>
            
            <div class="socials">
          <ul class="socials_list">
                    <li>
                        <a class="ico_social_facebook fa fa-facebook-square" target="_blank" href="javascript:void(0)" title="Facebook"></a>
                    </li>
                   
                    <li>
                        <a class="ico_social_twitter fa fa-twitter" target="_blank" href="javascript:void(0)" title="Twitter"></a>
                    </li>
                    <li>
                        <a class="ico_social_instagram fa fa-instagram" target="_blank" href="javascript:void(0)" title="Instagram"></a>
                    </li>
                  
                </ul>
            </div>
            
            
            
            <div class="clear"></div>
        </div>
    </header>