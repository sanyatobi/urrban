<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/jquery.timeago.js')}}" type="text/javascript"></script>
<script src="{{asset('js/msdropdown/jquery.dd.min.js')}}"></script>
<script src="{{asset('js/jquery.easing-1.3.pack.js')}}"></script>
<script type="text/javascript" src="{{asset('js/flag/bootstrap.min.js')}}"></script>
<script src="{{asset('js/flag/jquery.flagstrap.js')}}"></script>

<script>
    $('#f1_country').flagStrap();

</script>

<script type="text/javascript" src="{{asset('js/pop_gall/source/jquery.fancybox.js')}}"></script>

<script>
    $(document).ready(function() {
        $('.fancybox').fancybox();
    });
</script>

<script type="text/javascript" src="{{asset('js/modules.js')}}"></script>
<script type="text/javascript" src="{{asset('js/theme.js')}}"></script>



<script src="{{asset('js/oda_js.js')}}"></script>

<script>
    jQuery(window).load(function () {


        function recom_box_hyt(){
            /*	alert ('rwgr');*/
            var name_box_hyt= $('.all_head_sizer').height();
            $('.all_body_cont').css('min-height', name_box_hyt);
        }
        $(window).resize(function() {
            recom_box_hyt();
            //set_footer_down();
        });

        //set_footer_down();
        //	 set_tooltip_values();
        add_user();
        recom_box_hyt();
    });





</script>


<!--notifier-->
<script type="text/javascript" src="{{asset('js/notif/jquery.toast.js')}}"></script>
<!--notifier-->

<script>
/*
    //Start of masonry editing

    function iframe16x9fb(e) {
        "use strict";
        e.find("iframe").each(function() {
            jQuery(this).height(jQuery(this).width() / 16 * 9)
        })
    }
    jQuery(document).ready(function() {
        initialise_masonry();
    })



    window.setInterval(function(){
        console.log(jQuery(".is_masonry"));
        jQuery(".is_masonry").masonry(), iframe16x9(jQuery(".fs_blog_module"))
    }, 10000);

console.log("masonry done");
    //end  of masonry editing*/
</script>
<script type="text/javascript" src="{{asset('js/script.js')}}"></script>


<!--    accordion-->
<script type="text/javascript" src="{{asset('js/acc/smk-accordion.js')}}"></script>
<script type="text/javascript" src="{{asset('js/acc/smk-accordion.js')}}"></script>
<script type="text/javascript">
    jQuery(document).ready(function($){

        $(".notif_panel").smk_Accordion({
            showIcon: false, //boolean
            animation: true, //boolean
            closeAble: true, //boolean
            slideSpeed: 200 //integer, miliseconds
        });

    });
</script>
<!--    accordion-->





<!--  include slide-out menu/-->
<script src="{{asset('js/slide_menu/jquery.panelslider.js')}}"></script>
<script>
    $('.side_slider').panelslider({side: 'right', duration: 600, easingOpen: 'easeInBack', easingClose: 'easeOutBack'});
    //    $('.side_slider').panelslider();
    //  $('#right-panel-link').panelslider({side: 'right', clickClose: false, duration: 600, easingOpen: 'easeInBack', easingClose: 'easeOutBack'});

    $('.close-panel-bt').click(function() {
        $.panelslider.close();
    });
</script>
<!-- end of  include slide-out menu/-->


<!-- Include the plug-in -->
<script src="{{asset('js/qtip/jquery.qtip.js')}}"></script>



<!--scroller-->
<script src="{{asset('js/scroller/jquery.nicescroll.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('body').niceScroll({cursoropacitymax:0.8,cursorcolor:"#f2f2f2",cursorwidth:13,cursorborder:"1px solid #000", background:"#262A3F", cursorborderradius:"8px", zindex:"99999"});
        $('.uqscroll_hide').niceScroll({cursoropacitymax:0.8,cursorcolor:"#f2f2f2",autohidemode:true,cursorwidth:10,cursorborder:"1px solid #000", background:"#9D9FAA", cursorborderradius:"8px", zindex:"6666"});
        $('.uqscroll_show').niceScroll({cursoropacitymax:0.8,cursorcolor:"#f2f2f2",cursorwidth:10,cursorborder:"1px solid #000", background:"#9D9FAA", cursorborderradius:"8px", zindex:"6666"});
    });
</script>
<!--scroller-->


<!--tokenizer-->
<script src="{{asset('js/tagger/bootstrap-tokenizer.js')}}"></script>
<script src="{{asset('js/dropzone.js')}}"></script>
<script>
    $(document).ready(function () {
        Dropzone.autoDiscover = false;
        //var myDropzone = new Dropzone("#magnific_video_pop_inline_upload_box",{
        var myDropzone = new Dropzone("#post_content_feed",{
            url:'/gallery',
            autoProcessQueue:false,
            clickable: true,
            uploadMultiple:false,
            acceptedFiles:"image/*,video/*",
            sending: function(file, xhr, formData) {
                formData.append("post_content", $('#media_content').val());
                formData.append("album_id", $('#album_id').val());
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            success: function(file, response){
                if(response.status == "error"){
                    showError(response.message);
                    return;
                }
                else{
                    //close box and reload feeds
                    location.reload();

                }
            }
        });

        $('#post').on('click',function(e) {
            e.preventDefault();
            if (myDropzone.getQueuedFiles().length > 0) {
                myDropzone.processQueue();
            } else {
                showError("Upload an Image");
                return;
            }
        });

    /*    var myDropzone = new Dropzone("#post_file",{
            url:'/feed',
            autoProcessQueue:false,
            clickable: true,
            uploadMultiple:false,
            addRemoveLinks:true,
            acceptedFiles:"image/!*,video/!*",
            previewsContainer:"#uploadPreview",
            sending: function(file, xhr, formData) {
                formData.append("post_content", $('#post_content_feed').val());
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            success: function(file, response){
                if(response.status == "error"){
                    showError(response.message);
                    return;
                }
                else{
                    //close box and reload feeds
                    location.reload();

                }
            }
        });*/

        $('#share').on('click',function(e) {
            e.preventDefault();
            if (myDropzone.getQueuedFiles().length > 0) {
                myDropzone.processQueue();
            } else {
                showLoader();
                var post_content = $("#post_content_feed").val();
                $.ajax({
                    url: site_url+"feed" ,
                    type: 'post',
                    dataType: 'json',
                    data  : {post_content:post_content}
                }).done(function(response) {
                    hideLoader();
                    if(response.status == "error"){
                        showError(response.message);
                        return;
                    }
                    else{
                        //close box and reload feeds
                        location.reload();

                    }
                }).fail(function() {
                    hideLoader();
                });
            }
        });


    });
</script>
