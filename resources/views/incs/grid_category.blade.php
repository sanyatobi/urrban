<div class="fullscreen_block">

    <!--    post area-->
    <div class="row post_bg">
        <div class="post_post_but_row">
            <div class="postarea_post_but bg_o">
                <i class="fa  fa-pencil-square"></i>
                Post to feed
            </div>
        </div>
        <div class="post_bg_inner">
            <div class="post_area_margineer">
                <div class="post_act_area">
                    <div class="post_box_con">
                        <textarea class="post_box_enter" id="post_content"></textarea>
                        <!--  <span class="upload_med_button fa fa-camera-retro"></span>  -->
                    </div>
                    <span class="dropzone-previews" id="uploadPreview"></span>

                    <div id="post_file" class="post_box_upload_box">
                        <span   src="{{asset('img/upload.jpg')}}" class="noimg"></span>
                        <div  class="text">
                            Upload Media <br />
                            ---
                        </div>
                        <span class="close fa fa-times"></span>
                    </div>

                    <div class="post_box_upload_box_button_row">
                        <button id="share" class="post_button shortcode_button btn_small btn_type5">Share</button>
                        <!--    <a href="javascript:void(0)" class="hasIcon shortcode_button btn_small btn_type5"><i class="icon-cog"></i>Colored</a>-->
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!--    post area end-->

    <div class="fs_blog_module is_masonry this_is_blog" >
        @foreach ($feeds as $feed)
            {{$liked_flag = "notliked"}}
            {{$follow_flag = "notadded"}}
            @if(in_array(\Illuminate\Support\Facades\Auth::user()->id,(array_column($feed["likes"],"user_id"))))
                {{$liked_flag = "liked"}}
            @endif
            <?php
            $pieces = explode(" ", $feed['content']);
            $first_part = implode(" ", array_splice($pieces, 0, 10));
                $title = str_slug($first_part,'-');
                
            $users = new \App\Http\Controllers\User\UserController();
            $following = $users->getFollowing();
            ?>
            @if(($following) && (in_array($feed["owner"]["id"],$following)) )
                {{$follow_flag = "user_added_yes"}}
            @endif
            <div class='blogpost_preview_fw'>
                <button value="{{$feed["id"]}}" class='like_botton {{$liked_flag}} but_toggler wallpost'><span class='fa fa-heart'></span></button>
                @if(\Illuminate\Support\Facades\Auth::user()->id != $feed["owner"]["id"])
                <button value="{{$feed["owner"]["id"]}}" class="add_contact_button {{$follow_flag}}  but_toggler" ><span class="fa fa-user-plus"></span></button>
                @endif
                
                <div class='fw_preview_wrapper featured_items'>

                    @if ($feed["image_url"])
                        <div class='img_block wrapped_img'>
                            <div class='pf_output_container'>
                                <a href='{{url('post/'.$feed['id'].'/'.$title)}}'><img class='featured_image_standalone' src='{{url("media_feed/".$feed["image_url"])}}' alt='' /></a>
                            </div>
                        </div>
                    @endif

                    @if ($feed["video_url"])
                        <div class='img_block wrapped_img'>
                            <div class='pf_output_container vodeo_grid'>
                            <a href='{{url('post/'.$feed['id'].'/'.$title)}}'>
                                <video controls preload="none">
                                    <source src='{{url("media_feed/".$feed["video_url"])}}' type='video/mp4' id='video-id'>
                                </video>
                                </a>
                            </div>
                        </div>
                    @endif
                    <div class='bottom_box' class=''> 
                        <div class='bc_content'>
                            <h5 class='bc_title'><a href='{{url('post/'.$feed['id'].'/'.$title)}}'>{{$feed["content"]}}</a></h5>
                            <div class='featured_items_meta'> <span class='preview_meta_data'>{{date("F j, Y",strtotime($feed["updated_at"]))}}</span> <span class='middot'>&middot;</span> <span><a href="/categories/{{$feed['owner']['interests']['slug']}}">{{$feed["owner"]["interests"]["title"]}}</a></span> <span class='middot'>&middot;</span>
                            </div>
                        </div>
                        <div class='bc_likes gallery_likes_add '> <div class='cell_info_hrts_cmm'> <i class=' fa fa-heart'></i> <span id="fd_lks{{$feed["id"]}}">{{count($feed["likes"])}}</span> </div> <div class='cell_info_hrts_cmm'> <i class=' fa fa-comment'></i> <span id="fd_cmnts{{$feed["id"]}}">{{count($feed["comments"])}}</span> </div> </div>
                        <div class='row'>
                            <div class='grid_det_bottom'> <div class='grip_u_pic_con'>
                                    <a href='{{ url($feed["owner"]["username"]) }}'><img src='{{url("media_avatar/".$feed["owner"]["avatar"])}}' /></a>
                                </div>
                                <div class='grid_det_bottom_text'> <div class='who'> <a href='{{ url($feed["owner"]["username"]) }}'>{{$feed["owner"]["name"]}}</a> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

    </div>
</div>
    
    