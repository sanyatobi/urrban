
    
<!--    notifictaion panel-->
    
    	<div id="notif-panel" class="notif-panel">
      <div class="notif_logo_con">
      	<img src="{{asset('images/log_ic.png')}}">
      </div>
      <div class="notif_p_margineer">
      	<div-- class="notif_p_content">
        
            <div class="titley">Notifications</div>
            <div class="notif_panel"> 
                <!-- Section 2 -->
                <?php
                $notifications = new \App\Http\Controllers\NotificationController();
                    $notifications = $notifications->get();

                ?>
                @forelse($notifications as $notification)
                    <div class="accordion_in">
                        @if($notification["message"])
                        <div class="acc_head">
                         <div class="notif_pic_con">
                            <img src="{{url("media_avatar/".$notification["sender"]["avatar"])}}" />
                         </div>
                          <span class="notf_text"> {{$notification["sender"]["name"]}} {{$notification["category"]["message"]}} </span>
                         </div>
                        <div class="acc_content">
                        <a href="profile.html#view_my_endorsement_artcl"><p>Samuel L Jackson is a fantastic guy. Extraordinary, hardworking...</p></a>
                        </div>
                            @else
                                <a onclick="flagNotification({{$notification["id"]}})"">
                                    <div class="acc_head">
                                        <div class="notif_pic_con">
                                            <img src="{{url("media_avatar/".$notification["sender"]["avatar"])}}" />
                                        </div>
                                        <span class="notf_text"> {{$notification["sender"]["name"]." ".$notification["other"]["name"]}} {{$notification["category"]["message"]}} </span>
                                    </div>
                                </a>
                            @endif

                    </div>
                @endforeach
            @if(count($notifications))
			<div class="see_more-wide">
            	<a href="{{url("notifications")}}">See all</a>
            	<a style="cursor: pointer; cursor: hand;" onclick="flagNotification(0,1)">Clear all</a>
            </div>
                    @else
                        <div class="see_more-wide">
                            <a>No Notifications</a>
                        </div>
                    @endif

</div>

            
            
            
            <!--div class="titley">Contact Requests</div>
            <div class="notif_panel"> 
                    <!-- Section 1 >
                    <div class="accordion_in">
                    <div class="acc_head"> <div class="notif_pic_con"> 
                            <img src="{{url('images/dps/Adrian2.png')}}" />
                         </div>
                            <span class="notf_text"> Adrian Franchesco <i class=" fa fa-times"></i></span> 
                         </div>
                         
                         <div class="acc_content">
                        	<p><div class="my_row">
                            	<div class="notif_cntct_req_margineer">
                                	<div class="notif_cntct_req">
                                    	Just Met <i class="fa fa-check"></i>
                                    </div>
                                    
                                	<div class="notif_cntct_req">
                                    	Freinds <i class="fa fa-check"></i>
                                    </div>
                                    
                                	<div class="notif_cntct_req">
                                    	Paddy Mi <i class="fa fa-check"></i>
                                    </div>
                                </div>
                            </div></p>
                        </div>
                        
                    </div>
                    
                    
                    <!-- Section 2 >
                    <div class="accordion_in">
                    <div class="acc_head">
                         <div class="notif_pic_con"> 
                            <img src="{{url('images/dps/Adrian2.png')}}" />
                         </div>
                          <span class="notf_text"> Sarah palin  <i class=" fa fa-times"></i> </span>
                         </div>
                         <div class="acc_content">
                        	<p><div class="my_row">
                            	<div class="notif_cntct_req_margineer">
                                	<div class="notif_cntct_req">
                                    	Just Met <i class="fa fa-check"></i>
                                    </div>
                                    
                                	<div class="notif_cntct_req">
                                    	Freinds <i class="fa fa-check"></i>
                                    </div>
                                    
                                	<div class="notif_cntct_req">
                                    	Paddy Mi <i class="fa fa-check"></i>
                                    </div>
                                </div>
                            </div></p>
                        </div>
                        
                    </div>
                    
                    
                    <!-- Section 3 >
                    <div class="accordion_in">
                        <div class="acc_head"> 
                            <div class="notif_pic_con"> 
                                <img src="{{url('images/dps/Adrian2.png')}}" />
                             </div>
                            <span class="notf_text">Tomek Williams    <i class=" fa fa-times"></i> </span>
                        </div>
                            
                         <div class="acc_content">
                        	<p><div class="my_row">
                            	<div class="notif_cntct_req_margineer">
                                	<div class="notif_cntct_req">
                                    	Just Met <i class="fa fa-check"></i>
                                    </div>
                                    
                                	<div class="notif_cntct_req">
                                    	Freinds <i class="fa fa-check"></i>
                                    </div>
                                    
                                	<div class="notif_cntct_req">
                                    	Paddy Mi <i class="fa fa-check"></i>
                                    </div>
                                </div>
                            </div></p>
                        </div>
                        
                    </div>
                    
                    
                    <!-- Section 4 >
                    <div class="accordion_in">
                        <div class="acc_head"> 
                             <div class="notif_pic_con"> 
                                <img src="{{url('images/dps/Adrian2.png')}}" />
                             </div>
                            <span class="notf_text"> Keery Woodsmyster  <i class=" fa fa-times"></i> </span>
                         </div>
                              
                        <div class="acc_content">
                        	<p><div class="my_row">
                            	<div class="notif_cntct_req_margineer">
                                	<div class="notif_cntct_req">
                                    	Just Met <i class="fa fa-check"></i>
                                    </div>
                                    
                                	<div class="notif_cntct_req">
                                    	Freinds <i class="fa fa-check"></i>
                                    </div>
                                    
                                	<div class="notif_cntct_req">
                                    	Paddy Mi <i class="fa fa-check"></i>
                                    </div>
                                </div>
                            </div></p>
                        </div>
                    </div>

			<div class="see_more-wide">
            	<a href="javascript:void(0)">See all</a>
            	<a href="javascript:void(0)">Clear List</a>
            </div>

</div>
            
</div-->
        </div>
      </div>

<!--  end of  notifictaion panel-->     
      
      
<!--    messages panel-->
            
    	<div id="msg-notif-panel" class="msg-notif-panel" >
      <div class="notif_logo_con">
          <img src="{{asset('images/log_ic.png')}}">
      </div>
      <div class="notif_p_margineer">
            <div class="notif_p_content">
            
                <div class="titley">Messages</div>
                <div class="notif_panel">
                    <div id="messages_area"></div>

                <div class="see_more-wide">
                    <a href="{{url("messages")}}">See all</a>
                    <!--a href="javascript:void(0)">Clear all</a-->
                </div>
    </div>
    
                
            </div>
      </div>
      
    </div>

<!--    end  of essages panel-->


      
<!--    search panel-->

    	<!--div id="search-panel" class="msg-notif-panel" >
      <div class="notif_logo_con">
      	<img src="{{url('images/dps/Adrian2.png')}}">
      </div>
      <div class="notif_p_margineer">
            <div class="notif_p_content">

                <div class="notif_panel">

                    <div class="notif_p_margineer">
                        <div class="header_searchbar_inner_con">
                        <input type="text" placeholder="Search" class="panel_search_box" />
                        <button class="fa fa-search"></button>

                        <div class="side_p_adv_search_lnk">
                            <a href="javascript:void(0)">Advanced Search</a>
                        </div>
                    </div>
                    </div>
    			</div>

                <div class="titley sid_search">Contacts</div>
                <!-- notif panel->
                <div class="notif_panel panel_slide_srch">
                                     <div class="accordion_in">
                                    <div class="acc_head"> <div class="notif_pic_con">
                                            <img src="{{url('images/dps/Adrian2.png')}}" />
                                         </div>
                                            <span class="notf_text"> Delilah Johnson</span>
                                         </div>
                                    </div>

                                     <div class="accordion_in">
                                    <div class="acc_head"> <div class="notif_pic_con">
                                            <img src="{{url('images/dps/Adrian2.png')}}" />
                                         </div>
                                            <span class="notf_text"> Fracis J Underwood</span>
                                         </div>
                                    </div>

                                     <div class="accordion_in">
                                    <div class="acc_head"> <div class="notif_pic_con">
                                             <img src="{{url('images/dps/Adrian2.png')}}" />
                                         </div>
                                            <span class="notf_text"> Katy Perry</span>
                                         </div>
                                    </div>

                            <div class="see_more-wide">
                                <a href="javascript:void(0)">See all</a>
                            </div>
                </div>
                <!--end of notif panel->

                <!-- notif panel->
                <div class="titley sid_search">Posts</div>
                <div class="notif_panel panel_slide_post">
                                    <!-- Section 1 ->
                                    <div class="accordion_in">
                                    <div class="acc_head">
                                            <span class="notf_text">The Best Medium Length Hairstyles for Men 2015 ...</span>
                                         </div>
                                    </div>

                                     <div class="accordion_in">
                                    <div class="acc_head">
                                            <span class="notf_text"> I'm in heaven! Cheap Michael Kors Handbags Outlet ...</span>
                                         </div>
                                    </div>

                                     <div class="accordion_in">
                                    <div class="acc_head">
                                            <span class="notf_text"> OMG FINALLY! I NEED THIS! Fashion Forms Plunge Backless...</span>
                                         </div>
                                    </div>


                            <div class="see_more-wide">
                                <a href="javascript:void(0)">See all</a>
                            </div>
                </div>
                <!--end of notif panel->


            </div>
      </div>

    </div>

<!--    end  of search panel-->


<!--    share panel-->

<div id="share-panel" class="msg-notif-panel" >
    <div class="close-panel-bt fa  fa-times-circle"></div>

    <div class="notif_logo_con">
        <img src="{{asset('images/log_ic.png')}}">
    </div>
    <div class="notif_p_margineer">
        <div class="notif_p_content">

            <div class="titley sid_search">Share to Social Media</div>
            <!-- notif panel-->
            <div class="notif_panel panel_slide_srch">

                <div class="blogpost_share sidepanel_share">
                    <a href="javascript:void(0)" class="top_socials share_facebook"><i class="stand_icon icon-facebook-square"></i>&nbsp;&nbsp;Facebook</a>
                    <a href="javascript:void(0)" class="top_socials share_pinterest"><i class="stand_icon icon-pinterest"></i>&nbsp;&nbsp;Pinterest</a>
                    <a href="javascript:void(0)" class="top_socials share_tweet"><i class="stand_icon icon-twitter"></i>&nbsp;&nbsp;Twitter</a>
                    <a href="javascript:void(0)" class="top_socials share_gplus"><i class="icon-google-plus-square"></i>&nbsp;&nbsp;Google+</a>
                </div>

            </div>
            <!--end of notif panel-->

            <!-- notif panel-->
            <div class="titley sid_search">Share to Contact</div>
            <div class="notif_panel panel_slide_share_con">
                <div class="accordion_in">
                    <div class="acc_head">
                        <span class="notf_text"> <i class="fa fa-envelope-o"></i>Send to Email</span>
                    </div>

                    <div class="acc_content">
                        <input type="text" placeholder="Enter email" />
                        <a href="javascript:void(0)" class="shortcode_button btn_small btn_type1 slide_share_con_send">Send</a>
                    </div>
                </div>
                <div class="accordion_in">
                    <div class="acc_head">
                        <span class="notf_text"> <i class="fa fa-send-o"></i> Send to Contact</span>
                    </div>

                    <div class="acc_content">
                        <textarea type="text" placeholder="Enter contacts"> </textarea>
                        <a href="javascript:void(0)" class="shortcode_button btn_small btn_type1 slide_share_con_send">Send</a>
                    </div>
                </div>

            </div>
            <!--end of notif panel-->


        </div>
    </div>

</div>
