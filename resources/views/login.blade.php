<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>UrbanQ - Welcome</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="images/favicon.png" />

    <!-- Custom css files -->
    <link href="css/base.css" rel='stylesheet' type='text/css' />
    <link href="css/font.css" rel='stylesheet' type='text/css' />
    <!-- Custom css files -->

    <!--custom select box -->
    <link rel="stylesheet" href="js/select/selectric.css">
    <!--custom select box -->

    <!--   login theng imports-->
    <link rel="stylesheet" type="text/css" href="css/component.css" />
    <link rel="stylesheet" type="text/css" href="css/content.css" />
    <script src="js/login/modernizr.custom.js"></script>

    <!--   login theng imports-->

    <!--js's-->
    <script src="js/jquery.js"></script>
    <script src="js/script.js"></script>

    <!--  <script src="js/oda_js.js"></script>-->
    <script>

        function set_footer_down(){
            if ($(document).height() <= $(window).height() ) {
                $('.footer_con').css('position', 'absolute');
                $('.footer_con').css('bottom', 0);
                $('.footer_con').css('margin-top', '-1px');
            }
            else{
                $('.footer_con').css('position', 'static');
            }
        };
        //alert ('hello');
        function forgot_pass_toggler() {

            $( ".forgot_pass_link" ).click(function() {

                /*	$(this).animate({"display":"none"}, 2000, function() {
                 // Animation complete.
                 });*/

                /*		 $( this ).animate({
                 display: hide
                 }, 5000, function() {
                 // Animation complete.
                 });

                 $( '.fgort_pass_tt' ).animate({
                 display: show
                 }, 5000, function() {
                 // Animation complete.
                 });*/


                $(this).fadeOut( "fast" );
                $('.fgort_pass_tt').fadeIn( "slow" );


                /*
                 $(this).css('display','none');
                 $('.fgort_pass_tt').css('display','block');*/
            });

        };

        $(document).ready(function(){
            set_footer_down();
            forgot_pass_toggler();
        });

        window.onresize = set_footer_down_caller;
        function set_footer_down_caller() {
            set_footer_down();
        }
    </script>

    <!--js's-->


</head>
<div class="hello yea">

</div>
<body class="login">
<div class="login_marginer">
    <div class="login_con">
        <div class="login_logo_con">
            <a href="">
                <img src="img/logo_.png" />
            </a>
        </div>
        <div class="login_slogan">
            <div class="login_slogan_marginer">
                <div class="big">Your creative fashion hub</div>
                <div class="small">Connecting creative professionals from all fashion to photography</div>
            </div>
        </div>
        <div class="log_sign_function_row my_row">
            <div class="log_sign_function_row_top"></div>
            <div class="log_sign_function_button_row my_row">
                <div class="log_sign_function_button_marginer">
                    <i class="morph-button morph-button-modal morph-button-modal-2 morph-button-fixed">
                        <button type="button" class="log_sign_function_button">Login </button>
                        <div class="morph-content">
                            <div>
                                <div class="content-style-form content-style-form-1">
                                    <span class="fa fa-times icon-close"></span>
                                    <h2><img src="images/log_ic.png" /></h2>
                                    <form action="feed.php">
                                        <p><label>Email</label><input id="email" required type="text"/></p>
                                        <p><label>Password</label><input id="password" type="password" /></p>
                                        <p><button id="loginButton">Login</button></p>
                                        <a href="javascript:void(0)"  class="forgot_pass_link"><span class="fgtp">forgot Password ?</span></a>
                                        <p><input type="text" placeholder="Enter your email to reset password" class="fgort_pass_tt" /></p>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </i>
                    <span class="or"> Or </span>
                    <i class="morph-button morph-button-modal morph-button-modal-3 morph-button-fixed">
                        <button type="button" class="log_sign_function_button">Signup</button>
                        <div class="morph-content signu_pop">
                            <div>
                                <div class="content-style-form content-style-form-2">
                                    <span class="fa fa-times icon-close"></span>
                                    <form>
                                        <h2><img src="images/log_ic.png" /></h2>
                                        <p><label>Full name</label><input required id="name" type="text" /></p>
                                        <p><label>Email</label><input id="signup_email" type="text" /></p>
                                        <p><label>Password</label><input id="signup_password" required type="password" /></p>
                                        <div>
                                            <label>Interest</label>
                                            <select class="customOptions" required id="interests">
                                            </select>
                                        </div>
                                        <p><button id="signUpButton">Sign Up</button></p>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </i>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="footer_con">
    <div class="footer_con_top"></div>
    <a href="category.php">Fashion Design</a>
    <a href="category.php">Modeling</a>
    <a href="category.php">Photography</a>
    <a href="category.php">Makeup art</a>
    <a href="category.php">Hair styling</a>
    <a href="category.php">Fashion Directing</a>
    <a href="category.php">Bridals</a>
    <a href="category.php">Fashion Forecasting</a>
    <a href="category.php">Fashion Editing</a>
</div>

<script src="js/login/classie.js"></script>
<script src="js/login/uiMorphingButton_fixed.js"></script>
<script>
    (function() {
        var docElem = window.document.documentElement, didScroll, scrollPosition;

        // trick to prevent scrolling when opening/closing button
        function noScrollFn() {
            window.scrollTo( scrollPosition ? scrollPosition.x : 0, scrollPosition ? scrollPosition.y : 0 );
        }

        function noScroll() {
            window.removeEventListener( 'scroll', scrollHandler );
            window.addEventListener( 'scroll', noScrollFn );
        }

        function scrollFn() {
            window.addEventListener( 'scroll', scrollHandler );
        }

        function canScroll() {
            window.removeEventListener( 'scroll', noScrollFn );
            scrollFn();
        }

        function scrollHandler() {
            if( !didScroll ) {
                didScroll = true;
                setTimeout( function() { scrollPage(); }, 60 );
            }
        };

        function scrollPage() {
            scrollPosition = { x : window.pageXOffset || docElem.scrollLeft, y : window.pageYOffset || docElem.scrollTop };
            didScroll = false;
        };

        scrollFn();

        [].slice.call( document.querySelectorAll( '.morph-button' ) ).forEach( function( bttn ) {
            new UIMorphingButton( bttn, {
                closeEl : '.icon-close',
                onBeforeOpen : function() {
                    // don't allow to scroll
                    noScroll();
                },
                onAfterOpen : function() {
                    // can scroll again
                    canScroll();
                },
                onBeforeClose : function() {
                    // don't allow to scroll
                    noScroll();
                },
                onAfterClose : function() {
                    // can scroll again
                    canScroll();
                }
            } );
        } );

    })();
</script>



<!--custom select box -->
<link rel="stylesheet" href="js/select/selectric.css">
<script src="js/select/jquery.selectric.js"></script>
<script>
    $(function(){
        $('select').selectric({
            maxHeight: 200
        });
        $('.customOptions').selectric({
            optionsItemBuilder: function(itemData, element, index) {
                return element.val().length ? '<span class="ico ico-' + element.val() +  '"></span>' + itemData.text : itemData.text;
            }
        });
    });
    $(document).ready(function() {
        fetchCategories('interests');
    });
</script>
<!--custom select box -->
</body>
</html>
