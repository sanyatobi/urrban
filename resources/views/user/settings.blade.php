<?php
/**
 * Created by PhpStorm.
 * User: oluwatobi.okusanya
 * Date: 01/12/15
 * Time: 2:23 PM
 */
        ?>

        <!DOCTYPE html>
<html>

<head>

    @include("incs.imports_top")


    <title>UrbanQ -  My Settings</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />


</head>

<body>
<div id="panel_con">
    @include("incs.head_navigation")



    <div class="site_wrapper">
        <div class="main_wrapper">
            <div class="content_wrapper">
                <div class="container">
                    <div class="content_block row left-sidebar">
                        <div class="fl-container">

                            @include("incs.my_profile_sidebar")



                            <div class="row">
                                <div class="posts-block hasLS">
                                    <div class="span12 module_number_3 module_cont pb10 module_toggle">
                                        <h5 class="headInModule">Settings</h5>
                                        <div class="bg_title">

                                            <div class="row">
                                                <div class="span12 module_number_16 module_cont pb5 module_promo_text">
                                                    <a  class="saveSettingsButton hasIcon shortcode_button btn_normal btn_type17"><i class=" fa fa-save"></i>Save</a>
{{--
                                                    <a href="my_settings.php" class="  hasIcon shortcode_button btn_normal btn_type2"><i class=" fa fa-times"></i>Cancel</a>
--}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="shortcode_toggles_shortcode toggles">
                                            <!--<h5 data-count="" class="shortcode_toggles_item_title expanded_no">Basic Info<span class="ico"></span></h5>-->
                                            <h5 data-count="" class="shortcode_toggles_item_title expanded_yes">General<span class="ico"></span></h5>
                                            <div class="shortcode_toggles_item_body">
                                                <div class="ip">
<input type="hidden" value="{{\Illuminate\Support\Facades\Auth::user()->username}}" id="old_username">
                                                    <div class="module_content contact_form">
                                                        <div id="note"></div>
                                                        <div id="fields">
                                                            <div class="edit_prof">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0"  class="prof_tab">

                                                                    <td width="16%">Username:</td>
                                                                    <td width="84%"><input type="text" id="username" value="{{$data['user']['username']}}"> </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Email:</td>
                                                                        <td> <input type="text" id="email"  value="{{$data['user']['email']}}"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>New password:</td>
                                                                        <td> <input type="password" id="password"  value=""></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Re New password:</td>
                                                                        <td> <input type="password" id="password_conf"  value=""></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Email Notifications:</td>
                                                                        <td>
                                                                            <select id="email_notifications">
                                                                                <option <?php echo ($data['user']['receive_email_notification']) ? 'selected' : ''?>  value="{{Config::get('constants.EMAIL_NOTIFICATION_OFF')}}"> Off </option>
                                                                                <option <?php echo ($data['user']['receive_email_notification']) ? 'selected' : ''?> value="{{Config::get('constants.EMAIL_NOTIFICATION_ON')}}"> On </option>
                                                                            </select>
                                                                            <div class="notif_op">
                                                                                <?php
                                                                                $user_notifications = array_pluck($data['user']->toArray()['email_notifications'],'id')
?>
                                                                            @foreach($data['email_notifications'] as $notification)
                                                                                <div class="op_o"> <input class="notification_categories" <?php if (in_array($notification->id,$user_notifications)) echo "checked"?> value="{{$notification->id}}" type="checkbox"> {{$notification->title}}    </div>
                                                                                @endforeach
                                                                                  </div>
                                                                                {{--<div class="op_o">  <input type="checkbox"> Reminders  </div>
                                                                                <div class="op_o">  <input type="checkbox"> Endorsement Request     </div>
                                                                                <div class="op_o">  <input type="checkbox"> Approved Endorsement     </div>--}}
                                                                            </div>
                                                                        </td>
                                                                    </tr>

                                                                </table>

                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>





                                            </div>



                                        </div>




                                        <div class="shortcode_toggles_shortcode toggles">
                                            <h5 data-count="" class="shortcode_toggles_item_title expanded_yes">Privacy<span class="ico"></span></h5>
                                            <div class="shortcode_toggles_item_body">


                                                <div class="prof_info">
                                                    <strong class="my_row">Who can send me a contact request? </strong>
                                                    <select id="privacy_contact">

                                                        <option <?php echo ($data['user']['privacy_contact_request'] == Config::get('constants.PRIVACY_CONTACT_REQUEST_EVERYONE')) ? 'selected' : ''?>  value="{{Config::get('constants.PRIVACY_CONTACT_REQUEST_EVERYONE')}}"> Everyone </option>
                                                        <option <?php echo ($data['user']['privacy_contact_request'] == Config::get('constants.PRIVACY_CONTACT_REQUEST_CONTACTS_OF_CONTACTS')) ? 'selected' : ''?>  value="{{Config::get('constants.PRIVACY_CONTACT_REQUEST_CONTACTS_OF_CONTACTS')}}"> Contacts of contacts </option>
                                                    </select>
                                                </div>

                                                <div class="prof_info">
                                                    <strong class="my_row">Who can look me up ? </strong>
                                                    <select id="privacy_look_up">
                                                        <option  <?php echo ($data['user']['privacy_look_up'] == Config::get('constants.PRIVACY_CONTACT_LOOK_UP_EVERYONE')) ? 'selected' : ''?>  value="{{Config::get('constants.PRIVACY_CONTACT_LOOK_UP_EVERYONE')}}"> Everyone </option>

                                                        <option  <?php echo ($data['user']['privacy_look_up'] == Config::get('constants.PRIVACY_CONTACT_LOOK_UP_CONTACTS_OF_CONTACTS'))  ? 'selected' : '' ?>  value="{{Config::get('constants.PRIVACY_CONTACT_LOOK_UP_CONTACTS_OF_CONTACTS')}}"> Contacts of contacts </option>
                                                        <option <?php echo ($data['user']['privacy_look_up'] == Config::get('constants.PRIVACY_CONTACT_LOOK_UP_CONTACTS')) ? 'selected' : ''?>  value="{{Config::get('constants.PRIVACY_CONTACT_LOOK_UP_CONTACTS')}}"> Only contacts </option>
                                                    </select>

                                                    <div class="notif_op">
                                                        <div class="op_o"> <input id="use_email" <?php if ($data['user']['use_email']) echo "checked"?> type="checkbox"> Using the email provided    </div>
                                                        <div class="op_o">  <input id="use_number" <?php if ($data['user']['use_phone']) echo "checked"?> type="checkbox"> Using the contact number provided  </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--  <div class="prof_info"><span>Toggle 1 :</span>
                                                  <div class="toggle_switch_con">
                                                      <div class="fa  fa-toggle-off toggle_switch toggle_off_"> <span>Off</span> </div>
                                                   </div>
                                              </div>-->

                                        </div>

                                    </div>



                                    <div class="row">
                                        <div class="span12 module_number_16 module_cont pb5 pt10 module_promo_text">
                                            <a  class="saveSettingsButton hasIcon shortcode_button btn_normal btn_type17"><i class=" fa fa-save"></i>Save</a>
{{--
                                            <a href="" class="  hasIcon shortcode_button btn_normal btn_type2"><i class=" fa fa-times"></i>Cancel</a>
--}}
                                        </div>
                                    </div>



                                    <div class="row">
                                        <div class="span12 module_number_16 module_cont pb5 pt10 module_promo_text">
                                            <a href="javascript:void(0)" class=" shortcode_button btn_small btn_type11">Deactivate Your Account </a>
                                        </div>
                                    </div>




                                </div>

                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    @include("incs.footer_stuffs")




</div>



<!--    notification panel slides-->

@include("incs.notification_panels")


        <!--    end notification panel slides-->


@include("incs.imports_bottom")

</body>

</html>
