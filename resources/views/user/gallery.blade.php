<?php
/**
 * Created by PhpStorm.
 * User: oluwatobi.okusanya
 * Date: 27/11/15
 * Time: 10:16 AM
 */
        ?>
        <!DOCTYPE html>
<html>

<head>

    @include("incs.imports_top")


    <title>UrbanQ -  Gallery</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />


</head>

<body>
<div id="panel_con">
    @include("incs.head_navigation")


    <div class="gallery_top">
        <h5 class=""> Gallery </h5>
        <div class="gallery_top_cntrls">
            <div class="hasIcon shortcode_button btn_small btn_type4 g_t_cntrl _close"><span><i class="fa  fa-folder-open"></i>Create Album</span></div>
            <div class="con_accept_options_con g_t_cntrl_pop">
                <span class="fa fa-times"></span>
                <div class="con_accept_options ">
                    <div class="mc_form_inside">
                        <div class="mc_merge_var">
                            <input type="hidden" value="{{\Illuminate\Support\Facades\Auth::user()->username}}"/>
                            <label for="mc_s_fm_txt_b" class="mc_var_label mc_header mc_header_email">Email Address</label>
                            <input type="text" name="mc_s_fm_txt_b" id="album_title" class="mc_input" placeholder="Enter Album name" /> </div>
                        <div class="s_row_fm_submit">
                            <a id="create_album" href="javascript:void(0)"><input type="submit" name="s_row_fm_submit" id="s_row_fm_submit" value="Go!" class="button" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="fullscreen_block fullscreen_portfolio with_padding row gal_alb_fixr">

        <div class="fs_blog_module fw_port_module2 image-grid sorting_block gallery_albums" id="list">
            <a href="{{url(\Illuminate\Support\Facades\Auth::user()->username."/gallery/wall-posts")}}">
                <div class="blogpost_preview_fw element events">
                    <div class="fw_preview_wrapper featured_items">
                        <div class="img_block wrapped_img"> <img src="{{url("media_feed/".$data['wall']['cover'])}}" alt="" class="fw_featured_image" width="540"> </div>
                        <div class="bottom_box gall_ib_wall">
                            <div class="bc_content">
                                <h5 class="bc_title">Wall Posts</h5>
                                <div class="featured_items_meta"> <span class="preview_meta_data">{{date("F j, Y",strtotime($data['wall']['date']))}}</span> <span class="middot">&middot;</span> <span>{{$data['wall']['count']}} <?php echo ($data['wall']['count'] > 1 || $data['wall']['count'] == 0) ? 'items' : 'item' ?></span> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>

            @foreach($data['albums'] as $album)
                <a href="{{url(\Illuminate\Support\Facades\Auth::user()->username."/gallery/".$album['slug'])}}">
                    <div class="blogpost_preview_fw element events">
                        <div class="fw_preview_wrapper featured_items">
                            <div class="img_block wrapped_img">
                                <?php
                                    $cover_image = config('constants.DEFAULT_ALBUM_COVER_IMAGE');

                                    if( isset($album['cover']) && ($album['cover']['image_url'])){
                                        $cover_image = $album['cover']['image_url'];
                                    }
                                ?>
                                <img src="{{url("gallery_media/".$cover_image)}}" alt="" class="fw_featured_image" width="540">

                            </div>
                            <div class="bottom_box gall_ib_wall">
                                <div class="bc_content">
                                    <h5 class="bc_title">{{$album['title']}}</h5>
                                    <div class="featured_items_meta"> <span class="preview_meta_data">{{date("F j, Y",strtotime($album['created_at']))}}</span> <span class="middot">&middot;</span>
                                        <span>{{$album['count']}} <?php echo ($album['count'] > 1 || $album['count'] == 0) ? 'items' : 'item' ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                @endforeach

        </div>
    </div>



    @include("incs.footer_stuffs")




</div>


<!--    notification panel slides-->


@include("incs.notification_panels")


        <!--    end notification panel slides-->


@include("incs.imports_bottom")

</body>

</html>
