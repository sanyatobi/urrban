<?php
/**
 * Created by PhpStorm.
 * User: oluwatobi.okusanya
 * Date: 25/11/15
 * Time: 12:21 PM
 */
?>
        <!DOCTYPE html>
<html>

<head>

    @include("incs.imports_top")


<title>UrbanQ -  My contacts</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <meta name="csrf-token" content="{{ csrf_token() }}" />


</head>

<body>
<div id="panel_con">
    @include("incs.head_navigation")



    <div class="site_wrapper">
        <div class="main_wrapper">
            <div class="content_wrapper">
                <div class="container">
                    <div class="content_block row left-sidebar">
                        <div class="fl-container">

                            @include("incs.my_profile_sidebar")


                            <div class="row">
                                <div class="posts-block hasLS">
                                    <div class="span12 module_number_3 module_cont pb10 module_toggle">

                                        <h5 class="headInModule">Contacts</h5>

                                        <div class="shortcode_toggles_shortcode toggles">
                                            <!--<h5 data-count="" class="shortcode_toggles_item_title expanded_no">Basic Info<span class="ico"></span></h5>-->
                                            <h5 data-count="" class="shortcode_toggles_item_title expanded_yes">Pending Contact Requests<span class="ico"></span></h5>
                                            <div class="shortcode_toggles_item_body">
                                                <div class="sidepanel widget_posts contacts_acc">
                                                    <ul class="recent_posts">

                                                        @foreach($data["contact_requests"] as $request)
                                                        <li>
                                                            <span class="fa fa-times add_b"></span>
                                                                <span onclick="add_contact({{$request['id']}},'{{$request['name']}}')"  class="fa fa-user-plus add_b removeb_"></span>
                                                            <a class="post_title read_more" href="{{ url($request["username"]) }}">
                                                                <div class="recent_posts_img"><img src="{{url("media_avatar/".$request['avatar'])}}" alt="Underground Walls"></div>
                                                                <div class="recent_posts_content">{{$request['name']}}
                                                                    <br class="clear"> <span>{{$request['headline']}}</span> </div>
                                                                <div class="clear"></div>
                                                            </a>
                                                        </li>
                                                        @endforeach

                                                    </ul>
                                                    <div class="row">
                                                        <div class="load_more_con">
                                                            <a href="javascript:void(0)" class="shortcode_button btn_small btn_type3 fa fa-chevron-down ld_mr_ic"></a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <h5 data-count="1" class="shortcode_toggles_item_title expanded_yes"> Contacts<span class="ico"></span></h5>


                                            <div class="shortcode_toggles_item_body">
                                                <div class="sidepanel widget_posts contacts_acc">
                                                    <ul class="recent_posts">
                                              @foreach($data['contacts'] as $contact)
                                                        <li>
                                                            <span onclick="remove_contact({{$contact['id']}},'{{$contact['name']}}')" class="fa fa-user-times add_b"></span>
                                                            <a class="post_title read_more" href="{{ url($contact["username"]) }}">
                                                                <div class="recent_posts_img"><img src="{{url("media_avatar/".$contact['avatar'])}}" alt="Underground Walls"></div>
                                                                <div class="recent_posts_content">{{$contact['name']}}
                                                                    <br class="clear"> <span>{{$contact['headline']}}</span> </div>
                                                                <div class="clear"></div>
                                                            </a>


                                                        </li>
                                                  @endforeach
                                                    </ul>
                                                    <div class="row">
                                                        <div class="load_more_con">
                                                            <a href="javascript:void(0)" class="shortcode_button btn_small btn_type3 fa fa-chevron-down ld_mr_ic"></a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </div>
                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    @include("incs.footer_stuffs")




</div>



<!--    notification panel slides-->


@include("incs.notification_panels")


        <!--    end notification panel slides-->


@include("incs.imports_bottom")


</body>

</html>