<?php
/**
 * Created by PhpStorm.
 * User: oluwatobi.okusanya
 * Date: 25/11/15
 * Time: 7:14 PM
 */
        ?>
        <!DOCTYPE html>
<html>

<head>

    @include("incs.imports_top")

    <title>UrbanQ -  User Profile</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />


</head>

<body>
<div id="panel_con">
    @include("incs.head_navigation")



    <div class="site_wrapper">
        <div class="main_wrapper">
            <div class="content_wrapper">
                <div class="container">
                    <div class="content_block row left-sidebar">
                        <div class="fl-container">

                            <div class="left-sidebar-block">
                                <div class="sidepanel widget_posts profile_m_sidep">
                                    <ul class="recent_posts">
                                        <li>
                                            <?php
                                            $is_contact = in_array(\Illuminate\Support\Facades\Auth::user()->id,array_pluck($data['contacts'],'id'));
                                            ?>

                                           @if($is_contact)
                                                    <span onclick="remove_contact({{$data['id']}},'{{$data['name']}}')" class="fa fa-user-times add_b"></span>                                                @else
                                                    <span onclick="request_contact({{$data['id']}},'{{$data['name']}}')"  class="fa fa-user-plus add_b removeb_"></span>
                                                @endif
                                            <a class="post_title read_more profile_m" href="{{url($data['username'])}}">
                                                <div class="recent_posts_img"><img src="{{url("media_avatar/".$data['avatar'])}}"></div>
                                                <div class="recent_posts_content"> {{$data['name']}}
                                                    <br class="clear"> <span>{{$data['headline']}}</span> </div>
                                                <div class="clear"></div>
                                            </a>

                                        </li>
                                    </ul>
                                </div>
                                <div class="sidepanel widget_categories">

                                    <ul>
                                        <li><a href="{{url($data['username'])}}">Profile</a></li>
                                        <li><a href="">Contacts</a></li>
                                        <li class="widj_res_ap"><a href="javascript:void(0)">Gallery</a></li>
                                    </ul>
                                </div>


                                <div class="sidepanel widget_flickr widj_res_disp">
                                    <h6 class="sidebar_header">Photostream</h6>
                                    <div class="flickr_widget_wrapper">
                                        <div class="flickr_badge_image">
                                            <a><img alt="" src="img/flickr/1.jpg">
                                                <div class="flickr_fadder"></div>
                                            </a>
                                        </div>
                                        <div class="flickr_badge_image">
                                            <a ><img alt="" src="img/flickr/2.jpg">
                                                <div class="flickr_fadder"></div>
                                            </a>
                                        </div>
                                        <div class="flickr_badge_image">
                                            <a ><img alt="" src="img/flickr/3.jpg">
                                                <div class="flickr_fadder"></div>
                                            </a>
                                        </div>
                                        <div class="flickr_badge_image">
                                            <a ><img alt="" src="img/flickr/4.jpg">
                                                <div class="flickr_fadder"></div>
                                            </a>
                                        </div>
                                        <div class="flickr_badge_image">
                                            <a ><img alt="" src="img/flickr/5.jpg">
                                                <div class="flickr_fadder"></div>
                                            </a>
                                        </div>
                                        <div class="flickr_badge_image">
                                            <a ><img alt="" src="img/flickr/6.jpg">
                                                <div class="flickr_fadder"></div>
                                            </a>
                                        </div>
                                        <div class="flickr_badge_image">
                                            <a ><img alt="" src="img/flickr/7.jpg">
                                                <div class="flickr_fadder"></div>
                                            </a>
                                        </div>
                                        <div class="flickr_badge_image">
                                            <a ><img alt="" src="img/flickr/8.jpg">
                                                <div class="flickr_fadder"></div>
                                            </a>
                                        </div>
                                        <div class="flickr_badge_image">
                                            <a ><img alt="" src="img/flickr/9.jpg">
                                                <div class="flickr_fadder"></div>
                                            </a>
                                        </div>
                                        <div class="flickr_badge_image">
                                            <a ><img alt="" src="img/flickr/10.jpg">
                                                <div class="flickr_fadder"></div>
                                            </a>
                                        </div>

                                    </div>
                                    <div class="more_marg">
                                        <a href="javascript:void(0)" class="hasIcon shortcode_button btn_small btn_type5"> <i class=" fa fa-image ic_t2"></i> View More </a>
                                    </div>
                                </div>

                                <div class="sidepanel widget_text widj_res_disp">
                                    <h6 class="sidebar_header">Text Widget</h6>
                                    <div class="textwidget">Nam nunc massa, molestie vel sollicitudin sit amet consectetur a risus phasellus tristique diam nisl, a <a href="javascript:void(0)">bibendum urna,</a> venenatis varius nullam congue, pellentesque ipsum, vel ornare ligula.</div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="posts-block hasLS">
                                    <div class="span12 module_number_3 module_cont pb10 module_toggle">
                                        <h5 class="headInModule">Profile</h5>
                                        <div class="bg_title">

                                            <div class="row">
                                                <div class="span12 module_number_16 module_cont  module_promo_text">
                                                    <div class="shortcode_promoblock no_button_text no_button_link ">
                                                        <div class="promoblock_wrapper">
                                                            <div class="promo_text_block">
                                                                <div class="promo_text_block_wrapper">
                                                                    <h3 class="promo_title">{{$data['headline']}}</h3>  </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--<div class="w_catgory">Fashion Designer</div>-->
                                        <div class="">
                                            <ul class="contact_info_list">
                                                <li class="contact_info_item">
                                                    <div class="contact_info_wrapper"> <span class="contact_info_icon icon_type5"><i class="fa  fa-diamond"></i></span>
                                                        <div class="contact_info_text w_cat_txt">{{$data['interests']['title']}}</div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>


                                        <div class="shortcode_toggles_shortcode toggles">
                                            <!--<h5 data-count="" class="shortcode_toggles_item_title expanded_no">Basic Info<span class="ico"></span></h5>-->
                                            <h5 data-count="" class="shortcode_toggles_item_title expanded_yes">Basic Info<span class="ico"></span></h5>
                                            <div class="shortcode_toggles_item_body">
                                                <div class="ip">

                                                    <div class="module_content contact_form">
                                                        <div id="note"></div>
                                                        <div id="fields">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0"  class="prof_tab">
                                                                <tr>
                                                                    <td width="16%">Name:</td>
                                                                    <td width="84%">{{$data['name']}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Gender:</td>
                                                                    <td>  {{$data['gender']}} </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Phone No 1:</td>
                                                                    <td> {{$data['phone1']}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Phone No 2:</td>
                                                                    <td> {{$data['phone2']}}</td>
                                                                </tr>

                                                                <tr>
                                                                    <td>Country:</td>
                                                                    <td>
                                                                        <div id="f1_country_picked" class="cc_selector picked" data-input-name="country">
                                                                            <div class="wedge"></div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>City:</td>
                                                                    <td>  {{ucfirst($data['city']['name'])}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Side Gig:</td>
                                                                    <td>  {{$data['side_gig']}} </td>
                                                                </tr>
                                                            </table>

                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                            <h5 data-count="1" class="shortcode_toggles_item_title expanded_yes">Professional Info<span class="ico"></span></h5>
                                            <div class="shortcode_toggles_item_body">
                                                <div class="ip">


                                                    <div class="module_content contact_form">
                                                        <div id="note"></div>
                                                        <div id="fields">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0"  class="prof_tab">
                                                                <tr>
                                                                    <td width="16%">Designs:</td>
                                                                    <td width="84%">
                                                                        @foreach($data['design'] as $design)
                                                                            <u>{{$design['design']}}</u>
                                                                        @endforeach
                                                                        </td>
                                                                </tr>
                                                                <!--    <tr>
                                                                        <td>Email:</td>
                                                                        <td> J_Sean360@ymail.com  </td>
                                                                      </tr>-->
                                                                <tr>
                                                                    <td>Website/url:</td>
                                                                    <td>
                                                                        @foreach($data['website'] as $website)
                                                                            <u><a target="_blank" href="http://{{$website['website']}}">{{$website['website']}}</a></u>
                                                                        @endforeach
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Years in Business:</td>
                                                                    <td> {{$data['years_in_business']}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Education & Qualification:</td>
                                                                    <td>
                                                                        @foreach($data['education'] as $education)
                                                                            <u>{{$education['education']}}</u>
                                                                        @endforeach
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Summary of Qualification:</td>
                                                                    <td>{{$data['summary_of_qualification']}}</td>

                                                                </tr>

                                                            </table>

                                                        </div>


                                                    </div>
                                                </div>

                                                <!--div-- id="endorsments">
                                                    <h5 data-count="2" class="shortcode_toggles_item_title expanded_yes">Endorsements<span class="ico"></span></h5>
                                                    <div class="shortcode_toggles_item_body">
                                                        <div class="ip">
                                                            <div class="row">
                                                                <div class="span12 module_number_24 module_cont pb20 module_tabs">
                                                                    <div class="shortcode_tabs type2">
                                                                        <div class="all_head_sizer">
                                                                            <div class="all_heads_cont"></div>
                                                                        </div>
                                                                        <div class="all_body_sizer">
                                                                            <div class="all_body_cont"></div>
                                                                        </div>
                                                                        <h5 class="shortcode_tab_item_title expand_yes">Frank J Underwood</h5>
                                                                        <div class="shortcode_tab_item_body tab-content clearfix">
                                                                            <div class="ip">

                                                                                <div class="widget_posts">
                                                                                    <ul class="recent_posts">
                                                                                        <li>

                                                                                            <a class="post_title read_more" href="user_profile.php">
                                                                                                <div class="recent_posts_img"><img src="img/imgs/testimonial2.jpg"></div>
                                                                                                <div class="recent_posts_content"> Jean Sean
                                                                                                    <br class="clear"> <span>Experienced fashion designer from Dublin</span> </div>
                                                                                                <div class="clear"></div>
                                                                                            </a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                                <p>Sed scelerisque id, sodales et nulla aliquam eget dui ipsum vestibulum vel leo cursus velit. Proin sapien augue, finibus nec quam in, condimentum iaculis sem duis lobortis fermentum pulvinar. Proin sed posuere sapien ac tvestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae nunc tempor ac nisi. Vitae nibh tincidunt, eget posuere dolor lacinia ut ante mauris, lorem ullamcorper.&nbsp;Nulla faucibus eu elit quis efficitur. Mauris tincidunt ligula sed elit tristique euismod. Curabitur commodo eget tellus quis consectetur. Nam eget suscipit arcu, nec maximus orci.</p>
                                                                            </div>
                                                                        </div>
                                                                        <h5 class="shortcode_tab_item_title expand_no">George Orwell</h5>
                                                                        <div class="shortcode_tab_item_body tab-content clearfix">
                                                                            <div class="ip">


                                                                                <div class="widget_posts">
                                                                                    <ul class="recent_posts">
                                                                                        <li>

                                                                                            <a class="post_title read_more" href="user_profile.php">
                                                                                                <div class="recent_posts_img"><img src="img/imgs/team1.jpg" ></div>
                                                                                                <div class="recent_posts_content">Craig Bennert
                                                                                                    <br class="clear"> <span>Bridal Design Expert</span> </div>
                                                                                                <div class="clear"></div>
                                                                                            </a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>

                                                                                <p>Vitae nibh tincidunt, eget posuere dolor lacinia ut ante mauris, lorem ullamcorper.&nbsp;Nulla faucibus eu elit quis efficitur. Mauris tincidunt ligula sed elit tristique euismod. Curabitur commodo eget tellus .</p>
                                                                            </div>
                                                                        </div>
                                                                        <h5 class="shortcode_tab_item_title expand_no"> Mary J Blige </h5>
                                                                        <div class="shortcode_tab_item_body tab-content clearfix">
                                                                            <div class="ip">

                                                                                <div class="widget_posts">
                                                                                    <ul class="recent_posts">
                                                                                        <li>

                                                                                            <a class="post_title read_more" href="user_profile.php">
                                                                                                <div class="recent_posts_img"><img src="img/imgs/avatar3.jpg" alt="Underground Walls"></div>
                                                                                                <div class="recent_posts_content">Jennifer Biongo
                                                                                                    <br class="clear"> <span>Freestyle Hair Stylist</span> </div>
                                                                                                <div class="clear"></div>
                                                                                            </a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>

                                                                                <p>Nam eget suscipit arcu, nec maximus orci. Sed scelerisque id, sodales et nulla aliquam eget dui ipsum vestibulum vel leo cursus velit. Proin sapien augue, finibus nec quam in, condimentum iaculis sem duis lobortis fermentum pulvinar. Proin sed posuere sapien ac tvestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae nunc tempor ac nisi. Vitae nibh tincidunt, eget posuere dolor lacinia ut ante mauris, lorem ullamcorper.&nbsp;Nulla faucibus eu elit quis efficitur. Mauris tincidunt ligula sed elit tristique euismod. Curabitur commodo eget tellus quis consectetur.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div-->


                                            </div>
                                        </div>



                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    @include("incs.footer_stuffs")




</div>



<!--    notification panel slides-->


@include("incs.notification_panels")


        <!--    end notification panel slides-->


@include("incs.imports_bottom")


<script>

    $('#f1_country_picked').flagStrap({
        countries: {
            "{{$data['country']['code']}}": "{{$data['country']['name']}}",
        },
    });

</script>
</body>

</html>
