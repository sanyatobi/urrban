<!DOCTYPE html>
<html>

<head>

    @include("incs.imports_top")

    <title>UrbanQ -   I'm in heaven! Cheap Michael... </title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link rel="stylesheet" type="text/css" href="{{asset('js/pop_gall/source/jquery.fancybox.css')}}" media="screen" />

</head>

<body>
<div id="panel_con">
    @include("incs.head_navigation")


    <div class="gallery_top">
        <h5 class=""> <a href="{{url(\Illuminate\Support\Facades\Auth::user()->username.'/gallery')}}">Gallery</a> <i class="fa  fa-angle-right"></i> <span class="uncapt"> <i class="fa fa-folder"></i>  <a href="javascript:void(0)">{{$album['title']}}</a>  </span></h5>
        <div class="gall_two_half-width">



            <!--      <div class="gallery_top_cntrls">
                      <div class="shortcode_button btn_small btn_type4 g_t_cntrl _close jst_icon"><span class="fa fa-desktop"></span></div>
                  </div>-->
            <div class="gallery_top_cntrls">
                <div class="shortcode_button btn_small btn_type1 current_gal_view g_t_cntrl _close jst_icon"><span class="fa fa-th-large"></span></div>
            </div>



            <div class="gallery_top_cntrls last_bt_fixr">
                <a href="#share-panel" class="shortcode_button btn_small btn_type4 side_slider  g_t_cntrl _close jst_icon"><span class="fa fa-share-alt"></span></a>
            </div>
            @if(!$album['is_wall_post'])

            <div class="gallery_top_cntrls ">
                <div class="shortcode_button btn_small btn_type11 g_t_cntrl _close jst_icon"><span id="deleteAlbumButton" class="fa fa-trash"></span></div>
            </div>

            <div class="gallery_top_cntrls">
                <div class="hasIcon shortcode_button btn_small btn_type4 g_t_cntrl _close"><span><i class="fa fa-cog"></i>Edit</span></div>
                <div class="con_accept_options_con g_t_cntrl_pop">
                    <span class="fa fa-times"></span>
                    <div class="con_accept_options ">
                        <div class="mc_form_inside">
                            <div class="mc_merge_var">
                                <label for="mc_s_fm_txt_b" class="mc_var_label mc_header mc_header_email">Email Address</label>
                                <input type="text" name="mc_s_fm_txt_b" id="mc_s_fm_txt_b" value="{{$album['title']}}" class="mc_input" placeholder="Rename Album" /> </div>
                            <div class="s_row_fm_submit">
                                <input type="submit" name="s_row_fm_submit" class='editAlbumButton' id="s_row_fm_submit" value="Go!" class="button" />
                            </div>
                        </div>
                        <div class="priv_op_box_con">
                            <div class="titley"> Privileges</div>
<input type="hidden" value="{{$album['id']}}" id="album_id" />
                            <div class="cell_">
                                <div class="priv_rad_con"><input type="radio" <?php echo ($album['visibility'] == 'public') ? 'checked' : '' ?>  name="privilege" id="pb" value="{{config('constants.GALLERY_PUBLIC')}}" > <label for="pb">Public</label></div>
                                <div class="priv_rad_con"><input type="radio" <?php echo ($album['visibility'] == 'contact') ? 'checked' : ''?> name="privilege" id="pb" value="{{config('constants.GALLERY_CONTACTS')}}" > <label for="pb">Contacts Only</label></div>
                            </div>

                            <button  class="editAlbumButton width_full shortcode_button btn_normal btn_type17">Save</button>

                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>

        @if(!$album['is_wall_post'])
           <div class="gallery_top_cntrls">
            <a href="#magnific_video_pop_inline_upload_box" class="fancybox magnific_video_popper fancybox.inline "  >
                <div class="hasIcon shortcode_button btn_small btn_type4 g_t_cntrl _close"><span><i class="fa fa-upload"></i>Upload</span></div>
            </a>
        </div>
        @endif


    </div>

    <div class="fullscreen_block fullscreen_portfolio with_padding row gal_alb_fixr gallery_ini">
        <div class="fs_blog_module fw_port_module2 is_masonry photo_gallery">

            @foreach ($album['media'] as $media)
                <?php
                    $media_path  =  'gallery_media';
                    $wall_post_class = '';
                    if($album['is_wall_post']){
                        $wall_post_class = 'wallpost';
                       $media_path = 'media_feed';
                    }
                $id = "#magnific_video_pop_inline_html_video_".$media['id'];
                if($media['image_url'])
                    $id = "#magnific_video_pop_inline_image_".$media['id'];
                ?>
                {{$liked_flag = "notliked"}}
                @if(in_array(\Illuminate\Support\Facades\Auth::user()->id,(array_column($media["likes"],"user_id"))))
                    {{$liked_flag = "liked"}}
                @endif
                <div class="blogpost_preview_fw media_{{$media['id']}}">
                    <button value="{{$media["id"]}}" id="media_butt_{{$media["id"]}}" class='like_botton media {{$wall_post_class}} {{$liked_flag}} but_toggler' ><span class="fa fa-heart"></span></button>

                    <a href="#share-panel" class="side_slider"> <button class="share_gal_button gall_default but_toggler" ><span class="fa fa-share-alt"></span></button></a>

                    <button value="{{$media["id"]}}" class="del_button but_toggler {{$wall_post_class}}" ><span class="fa fa-trash"></span></button>
                    <div class="my_magnific_video">

                        <div class="img_block wrapped_img fs_port_item gallery_item_wrapper">

                            <a href="{{$id}}" class="fancybox magnific_video_popper fancybox.inline "data-fancybox-group="gallery" ></a>
                            @if ($media["image_url"])
                            <img src="{{url("$media_path/".$media["image_url"])}}" alt="" />
                            @endif

                            @if ($media["video_url"])

                            <div class="img_block wrapped_img">
                                <div class="pf_output_container vodeo_grid">
                                    <video controls  preload="none">
                                        <source src="{{url("$media_path/".$media["video_url"])}}" type="video/mp4" id="video-id"  >
                                        Your browser does not support the video tag.
                                    </video>
                                </div>
                            </div>
                                @endif
                            <div class="gallery_fadder"></div>
                        <span class="featured_items_ico">
                      {{$media["content"]}}
                        	<div class="gal_hvr_right">
                                <span  class="gal_ic_ma">
                                    <i class="fa fa-heart"></i>
                                    <i id="fd_lks{{$media["id"]}}"> {{count($media["likes"])}}</i>
                                 </span>
                            </div>

                        </span>
                        </div>
                    </div>
                </div>
            @endforeach






        </div>
        <div class="clear"></div>
    </div>



    <div id="magnific_video_pop_inline_upload_box" style="display: none;">
        Photo upload box
        <textarea class="" id="media_content"></textarea>
        <input type="hidden" value="{{$album["id"]}}" id="album_id"></input>

        <div class="post_box_upload_box_button_row">
            <button id="post" class="post_button shortcode_button btn_small btn_type5">Post</button>
        </div>

    </div>


<!--popovers-->
    @foreach ($album['media'] as $media)
        <?php
        $media_path  =  'gallery_media';
        $wall_post_class = '';
        if($album['is_wall_post']){
            $wall_post_class = 'wallpost';
            $media_path = 'media_feed';
        }
            $cls = "magnific_video_pop_inline_html_video";
            $id = "magnific_video_pop_inline_html_video_".$media['id'];
            if($media['image_url']){
                $cls = "magnific_video_pop_inline_image";
                $id = "magnific_video_pop_inline_image_".$media['id'];
                }
            $liked_flag = "notliked";
            if(in_array(\Illuminate\Support\Facades\Auth::user()->id,(array_column($media["likes"],"user_id")))){
                $liked_flag = "liked";
            }
        ?>
    <div id="{{$id}}" class="{{$cls}} media_{{$media['id']}}" style="display: none;">
        @if($album['is_wall_post'])
        <div class="top_row">
            <a href="post_view_pic.php" class=" shortcode_button btn_normal btn_type5 ">View post</a>
            <div class="magnific_closer fa fa-times"></div>
        </div>
        @endif

        <button value="{{$media["id"]}}" id="pop_butt_{{$media["id"]}}" class="like_botton pop {{$wall_post_class}}  {{$liked_flag}} but_toggler" ><span class="fa fa-heart"></span></button>

        <a href="#share-panel" class="side_slider"> <button class="share_gal_button gall_default but_toggler" ><span class="fa fa-share-alt"></span></button></a>

        <button value="{{$media["id"]}}" class="del_button but_toggler {{$wall_post_class}}" ><span class="fa fa-trash"></span></button>
        @if ($media["image_url"])
        <img src="{{url("$media_path/".$media["image_url"])}}">
        @endif

        @if ($media["video_url"])
        <video controls>
            <source src="{{url("$media_path/".$media["video_url"])}}" type="video/mp4" id="video-id"  >
            Your browser does not support the video tag.
        </video>
        @endif
        <div class="pop_det_but_row">
            <div class="but">
                <div class="cell"><i class="fa fa-heart"></i><i id="fd_lks_pop{{$media["id"]}}">{{count($media["likes"])}}</i></div>
            </div>
            <div class="but">{{$media["content"]}}
            </div>
        </div>

    </div>
    @endforeach
    <!--end popovers-->



    @include("incs.footer_stuffs")




</div>



<!--    notification panel slides-->


@include("incs.notification_panels")


        <!--    end notification panel slides-->


@include("incs.imports_bottom")


</body>

</html>