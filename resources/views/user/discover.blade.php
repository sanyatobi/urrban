<?php
/**
 * Created by PhpStorm.
 * User: oluwatobi.okusanya
 * Date: 10/24/15
 * Time: 8:08 PM
 */
?>
        <!DOCTYPE html>
<html>

<head>

    @include('incs.imports_top')


    <title>UrbanQ - Home</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />


</head>

<body>
<div id="panel_con">

    @include('incs.head_navigation')

    <div class="row">
        <div class="content_tabs_con">
            <a href="{{ URL::route('home') }}"  > Feed</a>
            <a href="{{ URL::route('discover') }}" class="current"> Discover</a>
        </div>
    </div>
    @include('incs.grid_discover')


    <div class="row">
        <div class="load_more_con">
            <a onclick="loadFeeds()" href="javascript:void(0)">
                <span  class="fa  fa-arrow-down"></span> Load more
            </a>
        </div>
    </div>


    @include('incs.footer_stuffs')




</div>



<!--    notification panel slides-->


@include('incs.notification_panels')


        <!--    end notification panel slides-->


@include('incs.imports_bottom')




<script>

    $(document).ready(function() {
        sticky_onload_notif();


    });
</script>
</body>

</html>
