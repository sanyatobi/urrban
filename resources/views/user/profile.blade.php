<?php
/**
 * Created by PhpStorm.
 * User: oluwatobi.okusanya
 * Date: 05/11/15
 * Time: 8:34 PM
 */
?>

<!DOCTYPE html>
<html>

<head>

@include("incs.imports_top")


<title>UrbanQ -  Profile</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


</head>

<body>
<div id="panel_con">

    @include("incs.head_navigation")


    <div class="site_wrapper">
        <div class="main_wrapper">
            <div class="content_wrapper">
                <div class="container">
                    <div class="content_block row left-sidebar">
                        <div class="fl-container">

                            @include("incs.my_profile_sidebar")


                            <div-- class="row">
                                <div class="posts-block hasLS">
                                    <div class="span12 module_number_3 module_cont pb10 module_toggle">
                                        <h5 class="headInModule">Profile</h5>
                                        <div class="bg_title">

                                            <div class="row">
                                                <div class="span12 module_number_16 module_cont pb5 module_promo_text">
                                                    <a href="{{url(\Illuminate\Support\Facades\Auth::user()->username)}}/edit" class="  hasIcon shortcode_button btn_normal btn_type2"><i class="fa fa-pencil"></i>Edit </a>
                                                    <a href="#share-panel" class="  hasIcon shortcode_button btn_normal btn_type5 side_slider "><i class=" fa fa-share-alt"></i>Share</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="span12 module_number_16 module_cont  module_promo_text">
                                                    <div class="shortcode_promoblock no_button_text no_button_link ">
                                                        <div class="promoblock_wrapper">
                                                            <div class="promo_text_block">
                                                                <div class="promo_text_block_wrapper">
                                                                    <h3 class="promo_title">{{$data["headline"]}}</h3>  </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--<div class="w_catgory">Fashion esigner</div>-->
                                        <div class="">
                                            <ul class="contact_info_list">
                                                <li class="contact_info_item">
                                                    <div class="contact_info_wrapper"> <span class="contact_info_icon icon_type5"><i class="fa  fa-diamond"></i></span>
                                                        <div class="contact_info_text w_cat_txt">{{$data["interests"]["name"]}}</div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>


                                        <div class="shortcode_toggles_shortcode toggles">
                                            <!--<h5 data-count="" class="shortcode_toggles_item_title expanded_no">Basic Info<span class="ico"></span></h5>-->
                                            <h5 data-count="" class="shortcode_toggles_item_title expanded_yes">Basic Info<span class="ico"></span></h5>
                                            <div class="shortcode_toggles_item_body">
                                                <div class="ip">

                                                    <div class="module_content contact_form">
                                                        <div id="note"></div>
                                                        <div id="fields">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0"  class="prof_tab">
                                                                <tr>
                                                                    <td width="16%">Name:</td>
                                                                    <td width="84%">{{$data["name"]}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Gender:</td>
                                                                    <td>  {{$data["gender"]}} </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Phone No 1:</td>
                                                                    <td>{{$data["phone1"]}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Phone No 2:</td>
                                                                    <td>{{$data["phone2"]}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>DOB:</td>
                                                                    <td>{{$data["dob"]}}<strong> (* Visible to you only)</strong></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Country:</td>
                                                                    <td>
                                                                        <div id="f1_country_picked" class="cc_selector picked" data-input-name="country">
                                                                            <div class="wedge"></div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>City:</td>
                                                                    <td>  {{ucfirst($data['city']['name'])}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Languages:</td>
                                                                    <td>
                                                                        @foreach($data['language'] as $language)
                                                                            <u>{{$language['language']}}</u>
                                                                        @endforeach
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Side Gig:</td>
                                                                    <td>{{$data['side_gig']}}</td>
                                                                </tr>
                                                            </table>

                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                            <h5 data-count="1" class="shortcode_toggles_item_title expanded_yes">Professional Info<span class="ico"></span></h5>
                                            <div class="shortcode_toggles_item_body">
                                                <div class="ip">


                                                    <div class="module_content contact_form">
                                                        <div id="note"></div>
                                                        <div id="fields">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0"  class="prof_tab">
                                                                <?php
                                                                    if($data["interests"]["id"] == 2) {
                                                                        ?>
                                                                <tr class="modelling_block">
                                                                    <td>Height (ft):</td>
                                                                    <td>{{@$data["height"]}}</td>
                                                                </tr>
                                                                <tr class="modelling_block">
                                                                    <td>Weight (kg):</td>
                                                                    <td>{{@$data["weight"]}}</td>
                                                                </tr>
                                                                <tr class="modelling_block">
                                                                    <td>Skin:</td>
                                                                    <td>{{@$data["skin"]}}</td>
                                                                </tr>
                                                                <tr class="modelling_block">
                                                                    <td>Hair:</td>
                                                                    <td>{{@$data["hair"]}}</td>
                                                                </tr>
                                                                <tr class="modelling_block">
                                                                    <td>Eye Colour:</td>
                                                                    <td>{{@$data["eye"]}}</td>

                                                                <tr class="modelling_block">
                                                                    <td>Foot size (in):</td>
                                                                    <td>{{@$data["foot"]}}</td>
                                                                </tr>
                                                                <tr class="modelling_block">
                                                                    <td>Waist Size (in):</td>
                                                                    <td>{{@$data["waist"]}}</td>
                                                                </tr>
                                                                    <?php
                                                                        if($data["gender"] == "female"){
                                                                            ?>
                                                                <tr class="modelling_female_block">
                                                                    <td>Bust Size (in):</td>
                                                                    <td>{{@$data["bust"]}}</td>
                                                                </tr>
                                                                <tr class="modelling_female_block">
                                                                    <td>Hip Size (in):</td>
                                                                    <td>{{@$data["hip"]}}</td>
                                                                </tr>
                                                                    <?php
                                                                        }
                                                                    }
                                                                            ?>
                                                                <tr>
                                                                    <td>Contact Address:</td>
                                                                    <td> {{$data['address']}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Engaged in:</td>
                                                                    <td> {{$data['engaged_in']}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="16%">Designs:</td>
                                                                    <td width="84%">
                                                                        @foreach($data['design'] as $design)
                                                                            <u>{{$design['design']}}</u>
                                                                        @endforeach
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Email:</td>
                                                                    <td> {{$data['email']}} <strong>(* Visible to you only)</strong></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Website/url:</td>
                                                                    <td>
                                                                        @foreach($data['website'] as $website)
                                                                        <u><a target="_blank" href="http://{{$website['website']}}">{{$website['website']}}</a></u>
                                                                        @endforeach
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Years in Business:</td>
                                                                    <td> {{$data['years_in_business']}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Education & Qualification:</td>
                                                                    <td>
                                                                        @foreach($data['education'] as $education)
                                                                            <u>{{$education['education']}}</u>
                                                                    @endforeach
                                                                </tr>
                                                                <tr>
                                                                    <td>Summary of Qualification:</td>
                                                                    <td>{{$data['summary_of_qualification']}}</td>
                                                                </tr>

                                                            </table>

                                                        </div>


                                                    </div>
                                                </div>

                                                <!--div id="endorsments">
                                                    <h5 data-count="2" class="shortcode_toggles_item_title expanded_yes">Endorsements<span class="ico"></span></h5>
                                                    <div class="shortcode_toggles_item_body">
                                                        <div class="ip">
                                                            <p> You have <a href="i_endorse.php"><strong>5 endorsements requests</strong></a>
                                                                <br><a href="req_endorsements.php" class="hasIcon shortcode_button btn_small btn_type5"> <i class=" fa  fa-certificate ic_t2"></i> Request endorsements </a> </p>
                                                            <div class="row">
                                                                <div class="span12 module_number_24 module_cont pb20 module_tabs">
                                                                    <div class="shortcode_tabs type2">
                                                                        <div class="all_head_sizer">
                                                                            <div class="all_heads_cont"></div>
                                                                        </div>
                                                                        <div class="all_body_sizer">
                                                                            <div class="all_body_cont"></div>
                                                                        </div>
                                                                        <h5 class="shortcode_tab_item_title expand_yes">Frank J Underwood</h5>
                                                                        <div class="shortcode_tab_item_body tab-content clearfix">
                                                                            <div class="ip">

                                                                                <div class="widget_posts">
                                                                                    <ul class="recent_posts">
                                                                                        <li>

                                                                                            <a class="post_title read_more" href="user_profile.php">
                                                                                                <div class="recent_posts_img"><img src="img/imgs/testimonial2.jpg"></div>
                                                                                                <div class="recent_posts_content"> Jean Sean
                                                                                                    <br class="clear"> <span>Experienced fashion designer from Dublin</span> </div>
                                                                                                <div class="clear"></div>
                                                                                            </a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                                <p>Sed scelerisque id, sodales et nulla aliquam eget dui ipsum vestibulum vel leo cursus velit. Proin sapien augue, finibus nec quam in, condimentum iaculis sem duis lobortis fermentum pulvinar. Proin sed posuere sapien ac tvestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae nunc tempor ac nisi. Vitae nibh tincidunt, eget posuere dolor lacinia ut ante mauris, lorem ullamcorper.&nbsp;Nulla faucibus eu elit quis efficitur. Mauris tincidunt ligula sed elit tristique euismod. Curabitur commodo eget tellus quis consectetur. Nam eget suscipit arcu, nec maximus orci.</p>
                                                                            </div>
                                                                        </div>
                                                                        <h5 class="shortcode_tab_item_title expand_no">George Orwell</h5>
                                                                        <div class="shortcode_tab_item_body tab-content clearfix">
                                                                            <div class="ip">


                                                                                <div class="widget_posts">
                                                                                    <ul class="recent_posts">
                                                                                        <li>

                                                                                            <a class="post_title read_more" href="user_profile.php">
                                                                                                <div class="recent_posts_img"><img src="img/imgs/team1.jpg" ></div>
                                                                                                <div class="recent_posts_content">Craig Bennert
                                                                                                    <br class="clear"> <span>Bridal Design Expert</span> </div>
                                                                                                <div class="clear"></div>
                                                                                            </a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>

                                                                                <p>Vitae nibh tincidunt, eget posuere dolor lacinia ut ante mauris, lorem ullamcorper.&nbsp;Nulla faucibus eu elit quis efficitur. Mauris tincidunt ligula sed elit tristique euismod. Curabitur commodo eget tellus .</p>
                                                                            </div>
                                                                        </div>
                                                                        <h5 class="shortcode_tab_item_title expand_no"> Mary J Blige </h5>
                                                                        <div class="shortcode_tab_item_body tab-content clearfix">
                                                                            <div class="ip">

                                                                                <div class="widget_posts">
                                                                                    <ul class="recent_posts">
                                                                                        <li>

                                                                                            <a class="post_title read_more" href="user_profile.php">
                                                                                                <div class="recent_posts_img"><img src="img/imgs/avatar3.jpg" alt="Underground Walls"></div>
                                                                                                <div class="recent_posts_content">Jennifer Biongo
                                                                                                    <br class="clear"> <span>Freestyle Hair Stylist</span> </div>
                                                                                                <div class="clear"></div>
                                                                                            </a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>

                                                                                <p>Nam eget suscipit arcu, nec maximus orci. Sed scelerisque id, sodales et nulla aliquam eget dui ipsum vestibulum vel leo cursus velit. Proin sapien augue, finibus nec quam in, condimentum iaculis sem duis lobortis fermentum pulvinar. Proin sed posuere sapien ac tvestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae nunc tempor ac nisi. Vitae nibh tincidunt, eget posuere dolor lacinia ut ante mauris, lorem ullamcorper.&nbsp;Nulla faucibus eu elit quis efficitur. Mauris tincidunt ligula sed elit tristique euismod. Curabitur commodo eget tellus quis consectetur.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>



                                    </div>

                                </div>
                            </div-->
                        </div>
                    </div>
                </div>
            </div>
        </div>



        @include("incs.footer_stuffs")



    </div>



    <!--    notification panel slides-->


    @include("incs.notification_panels")


            <!--    end notification panel slides-->


    @include("incs.imports_bottom")

    <script>

        $('#f1_country_picked').flagStrap({
            countries: {
                "{{$data['country']['code']}}": "{{$data['country']['name']}}",
            },
        });
    </script>

</body>

</html>
