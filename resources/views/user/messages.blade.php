<?php
/**
 * Created by PhpStorm.
 * User: oluwatobi.okusanya
 * Date: 10/01/16
 * Time: 4:29 PM
 */
?>

        <!DOCTYPE html>
<html>

<head>

    @include("incs.imports_top")


    <title>UrbanQ -  All notifications</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />


</head>

<body>
<div id="panel_con">
    @include("incs.head_navigation")


    <div class="site_wrapper">
        <div class="main_wrapper">
            <div class="content_wrapper">
                <div class="container">
                    <h5 class="headInModule">Messages</h5>
                    <div class="content_block row left-sidebar">
                        <div class="fl-container">

                            <div class="left-sidebar-block">
                                <div class=" my_row">
                                    <div class="header_searchbar_inner_con">
                                        <input type="text" placeholder="Search" id="user_search" onkeyup="allMessages(1)" class="msgs_search" />
                                        <button class="fa fa-search"></button>
                                    </div>
                                </div>
                                <div class="sidepanel widget_posts messges_hm uqscroll_hide">


                                    <ul class="recent_posts" id="all_messages_area">

                                    </ul>
                                    <!--div-- class="row">
                                        <div class="load_more_con">
                                            <a href="javascript:void(0)" class="shortcode_button btn_small btn_type3 fa fa-chevron-down ld_mr_ic"></a>
                                        </div>
                                    </div-->

                                </div>
                            </div>

                            <div class="row">
                                <div class="posts-block hasLS">
                                    <div style="display: none" id="conversation_area" class="span12 module_number_3 module_cont pb10 module_toggle">

                                        <h5 class="headInModule  mess_top my_row">
                                            Conversation with
                                            <a  id="conversation_with" class=""></a>
                                            <div id="clear" class="rebtn1 hasIcon shortcode_button btn_small btn_type4 right_but"><i class="fa fa-trash-o"></i>Clear conversation</div>
                                        </h5>


                                        <div class="module_content testimonials_list messages_list uqscroll_show">
                                            <ul id="conversation_list"></ul>
                                        </div>

                                        <div class="row my_row pt20">
                                            <div class="span12 module_number_19  pb0 module_divider">
                                                <hr>
                                            </div>
                                            <h5 class="headInModule  mess_top">Compose message</h5>
                                            <div class="ip mess_com_box_con">
                                                <div id="message_body" class="compose" contenteditable></div>
                                                <p class="form-submit">
                                                    <input name="submit" id="submit" value="Send" type="button">
                                                    <!--a href="#magnific_video_pop_inline_upload_box" class="fancybox magnific_video_popper fancybox.inline  hasIcon shortcode_button btn_small btn_type4"><i class="fa fa-paperclip"></i>Attach files</a-->
                                                </p>
                                            </div>
                                        </div>


                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="magnific_video_pop_inline_upload_box" style="display: none;">
        Upload files box
    </div>


    @include("incs.footer_stuffs")




</div>



<!--    notification panel slides-->


@include("incs.notification_panels")


        <!--    end notification panel slides-->


@include("incs.imports_bottom")
<script>
    allMessages();
</script>


</body>

</html>
