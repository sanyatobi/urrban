<?php
/**
 * Created by PhpStorm.
 * User: oluwatobi.okusanya
 * Date: 15/11/15
 * Time: 8:43 AM
 */
?>
        <!DOCTYPE html>
<html>

<head>

    @include("incs.imports_top")


    <title>UrbanQ -  Edit Profile</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />


    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <style>
        .ui-autocomplete-loading {
            background: white url("images/ui-anim_basic_16x16.gif") right center no-repeat;
        }
    </style>

</head>

<body>
<div id="panel_con">
    @include("incs.head_navigation")



    <div class="site_wrapper">
        <div class="main_wrapper">
            <div class="content_wrapper">
                <div class="container">
                    <div class="content_block row left-sidebar">
                        <div class="fl-container">

                            @include("incs.my_profile_sidebar")

                            <div class="row">
                                <div class="posts-block hasLS">
                                    <div class="span12 module_number_3 module_cont pb10 module_toggle">
                                        <h5 class="headInModule">Edit Profile</h5>
                                        <div class="bg_title">

                                            <div class="row">
                                                <div class="span12 module_number_16 module_cont pb5 module_promo_text">
                                                    <a style="cursor: hand; cursor: pointer" id="saveButton" class="hasIcon shortcode_button btn_normal btn_type17"><i class=" fa fa-save"></i>Save</a>
                                                    <a href="{{url(\Illuminate\Support\Facades\Auth::user()->username)}}" class="  hasIcon shortcode_button btn_normal btn_type2"><i class=" fa fa-times"></i>Cancel</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="span12 module_number_16 module_cont pb30 module_promo_text">
                                                    <div class="shortcode_promoblock no_button_text no_button_link ">
                                                        <div class="promoblock_wrapper">
                                                            <div class="promo_text_block">
                                                                <div class="promo_text_block_wrapper">
                                                                    <h3 class="promo_title p_edit" id="headline" contenteditable><?php echo ($data["headline"]) ? $data["headline"] : "Professionally I am a/an" ?></h3>  </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="">
                                            <ul class="contact_info_list">
                                                <li class="contact_info_item">
                                                    <div class="contact_info_wrapper"> <span class="contact_info_icon icon_type5"><i class="fa  fa-diamond"></i></span>
                                                        <div class="contact_info_text w_cat_txt ">
                                                            <?php
                                                            $categories = new \App\Http\Controllers\CategoryController();
                                                            $categories = $categories->get();
                                                            ?>
                                                            <select id="interest" onchange="watchInterest()">
                                                                @foreach ($categories as $category)
                                                                    {{$selected = ""}}
                                                                    @if($category->id == $data["interests"]["id"])
                                                                        {{$category->id}}
                                                                        {{$selected = "selected"}}
                                                                    @endif
                                                                    <option {{$selected}} value="{{$category->id}}">{{$category->title}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <input type="hidden" id="username" value="{{\Illuminate\Support\Facades\Auth::user()->username}}"/>
                                        <div class="shortcode_toggles_shortcode toggles">
                                            <!--<h5 data-count="" class="shortcode_toggles_item_title expanded_no">Basic Info<span class="ico"></span></h5>-->
                                            <h5 data-count="" class="shortcode_toggles_item_title expanded_yes">Basic Info<span class="ico"></span></h5>
                                            <div class="shortcode_toggles_item_body">
                                                <div class="ip">

                                                    <div class="module_content contact_form">
                                                        <div id="note"></div>
                                                        <div id="fields">
                                                            <div class="edit_prof">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0"  class="prof_tab">
                                                                    <tr>
                                                                        <td width="16%">Name:</td>
                                                                        <td width="84%"><input id='name' type="text"  value="{{$data["name"]}}"> </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>Gender:</td>
                                                                        <td>
                                                                            <select id="gender" onchange="watchInterest()">
                                                                                <?php
                                                                                $male_sel="";
                                                                                $female_sel="";
                                                                                ($data["gender"] == "female") ? $female_sel = "selected" : $male_sel = "selected";
                                                                                ?>
                                                                                <option {{$male_sel}} value="male"> Male </option>
                                                                                <option {{$female_sel}} value="female"> Female </option>
                                                                            </select>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Phone No 1:</td>
                                                                        <td> <input id='phone1' type="text"  value="{{$data['phone1']}}"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Phone No 2:</td>
                                                                        <td> <input id='phone2' type="text"  value="{{$data['phone2']}}"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>DOB:</td>
                                                                        <td><input id="dob" type="text" class="date" value="{{$data['dob']}}" placeholder="DD-MM-YYYY" ></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Country:</td>
                                                                        <td>

                                                                            <div id="f1_country" data-selected-country="{{$data['country']['code']}}" class="cc_selector"   data-input-name="country">
                                                                            </div>


                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>City:</td>
                                                                        <td>
                                                                            <input type="text" value="{{ucfirst($data['city']['name'])}}" id="city">
                                                                            <input type="hidden"  id="city_id">
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                    $languages = array_pluck($data['language'], 'language');
                                                                    $languages = implode(",",$languages);

                                                                    ?>
                                                                    <tr>
                                                                        <td>Languages:</td>
                                                                        <td> <input id="languages" class="" type="text" data-provide="tokenizer" value="{{$languages}}"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Side Gig:</td>
                                                                        <td> <input id="side_gig" type="text"  value="{{$data['side_gig']}}">  </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                            <h5 data-count="1" class="shortcode_toggles_item_title expanded_yes">Professional Info<span class="ico"></span></h5>
                                            <div class="shortcode_toggles_item_body">
                                                <div class="ip">


                                                    <div class="module_content contact_form">
                                                        <div id="note"></div>
                                                        <div id="fields">
                                                            <div class="edit_prof">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0"  class="prof_tab">
                                                                    <tr class="modelling_block">
                                                                        <td>Height (ft):</td>
                                                                        <td><input id="height" type="text" class="short"  value="{{@$data["height"]}}"></td>
                                                                    </tr>
                                                                    <tr class="modelling_block">
                                                                        <td>Weight (kg):</td>
                                                                        <td> <input id="weight" class="short" type="text"  value="{{@$data["weight"]}}"></td>
                                                                    </tr>
                                                                    <tr class="modelling_block">
                                                                        <td>Skin:</td>
                                                                        <td>
                                                                            <select id="skin">
                                                                                <option <?php echo ($data["skin"] == "normal") ?  "selected" : "" ?> value="normal"> Normal </option>
                                                                                <option <?php echo ($data["skin"] == "dry") ?  "selected" : "" ?> value="dry"> Dry </option>
                                                                                <option <?php echo ($data["skin"] == "oily") ?  "selected" : "" ?> value="oily"> Oily </option>
                                                                                <option <?php echo ($data["skin"] == "combination") ?  "selected" : "" ?> value="combination"> Combination</option>
                                                                                <option <?php echo ($data["skin"] == "sensitive") ?  "selected" : "" ?> value="sensitive"> Sensitive</option>
                                                                            </select>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="modelling_block">
                                                                        <td>Hair:</td>
                                                                        <td>
                                                                            <select id="hair">
                                                                                <option <?php echo ($data["hair"] == "straight") ?  "selected" : "" ?> value="straight"> Straight </option>
                                                                                <option <?php echo ($data["hair"] == "wavy") ?  "selected" : "" ?> value="wavy"> Wavy </option>
                                                                                <option <?php echo ($data["hair"] == "curly") ?  "selected" : "" ?> value="curly"> Curly </option>
                                                                                <option <?php echo ($data["hair"] == "kinky") ?  "selected" : "" ?> value="kinky"> Kinky</option>
                                                                            </select>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="modelling_block">
                                                                        <td>Eye Colour:</td>
                                                                         <td>
                                                                            <select id="eye">
                                                                                <option <?php echo ($data["eye"] == "black") ?  "selected" : "" ?>  value="black"> Black </option>
                                                                                <option <?php echo ($data["eye"] == "brown") ?  "selected" : "" ?> value="brown"> Brown </option>
                                                                                <option <?php echo ($data["eye"] == "hazel") ?  "selected" : "" ?> value="hazel"> Hazel </option>
                                                                                <option <?php echo ($data["eye"] == "grey") ?  "selected" : "" ?> value="grey"> Grey</option>
                                                                                <option <?php echo ($data["eye"] == "green") ?  "selected" : "" ?> value="green"> Green</option>
                                                                                <option <?php echo ($data["eye"] == "blue") ?  "selected" : "" ?> value="blue"> Blue</option>
                                                                            </select>
                                                                        </td>
                                                                    <tr class="modelling_block">
                                                                        <td>Foot size (in):</td>
                                                                        <td><input id="foot" type="text" class="short"  value="{{@$data["foot"]}}"></td>
                                                                    </tr>
                                                                    <tr class="modelling_block">
                                                                        <td>Waist Size (in):</td>
                                                                        <td><input id="waist" type="text" class="short"  value="{{@$data["waist"]}}"></td>
                                                                    </tr>
                                                                    <tr class="modelling_female_block">
                                                                        <td>Bust Size (in):</td>
                                                                        <td><input id="bust" type="text" class="short"  value="{{@$data["bust"]}}"></td>
                                                                    </tr>
                                                                    <tr class="modelling_female_block">
                                                                        <td>Hip Size (in):</td>
                                                                        <td> <input id="hip" class="short" type="text" value="{{@$data["hip"]}}"></td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>Contact Address:</td>
                                                                        <td>
                                                                            <textarea maxlength="500" id="address"  value="{{$data['address']}}"></textarea>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Enagaged In:</td>
                                                                        <td>
                                                                            <textarea maxlength="200" id="engaged_in"  value="{{$data['engaged_in']}}"></textarea>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <?php
                                                                        $designs = array_pluck($data['design'], 'design');
                                                                        $website = array_pluck($data['website'], 'website');
                                                                        $education = array_pluck($data['education'], 'education');
                                                                        $designs = implode(",",$designs);
                                                                        $website = implode(",",$website);
                                                                        $education = implode(",",$education);
                                                                        ?>
                                                                        <td width="16%">Designs:</td>
                                                                        <td width="84%"> <input id="design" class="" type="text" value="{{$designs}}" data-provide="tokenizer" value=""> </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Website/url:</td>
                                                                        <td> <input id="website" class="" type="text" data-provide="tokenizer" value="{{$website}}"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Years in Business:</td>
                                                                        <td>
                                                                            <input id="yib" type="text" class="short"  value="{{$data['years_in_business']}}">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Education & Qualification:</td>
                                                                        <td> <input id="education" class="" type="text" data-provide="tokenizer" value="{{$education}}"> </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Summary of Qualification:</td>
                                                                        <td>

                                                                            <textarea id="soq" name="message" id="message" placeholder="">{{$data['summary_of_qualification']}}</textarea>

                                                                        </td>
                                                                    </tr>

                                                                </table>
                                                            </div>

                                                        </div>


                                                    </div>
                                                </div>


                                            </div>
                                        </div>

                                        <!--div-- id="endorsments">

                                <h5 data-count="2" class="shortcode_toggles_item_title expanded_yes">endorsements<span class="ico"></span></h5>
                                <div class="shortcode_toggles_item_body">
                                    <div class="ip">

                                        <div class="row">
                                            <div class="span12 module_number_24 module_cont pb20 module_tabs">
                                                <div class="shortcode_tabs type2">
                                                    <div class="all_head_sizer">
                                                        <div class="all_heads_cont"></div>
                                                    </div>
                                                    <div class="all_body_sizer">
                                                        <div class="all_body_cont"></div>
                                                    </div>
                                                    <h5 class="shortcode_tab_item_title expand_yes">Frank J Underwood</h5>
                                                    <div class="shortcode_tab_item_body tab-content clearfix">
                                                        <div class="ip">

                                                            <div class="widget_posts">
                                                                <ul class="recent_posts">
                                                                    <li>
                                                                        <span class="fa fa-times add_b del"></span>
                                                                        <a class="post_title read_more" href="user_profile.php">
                                                                            <div class="recent_posts_img"><img src="img/imgs/testimonial2.jpg"></div>
                                                                            <div class="recent_posts_content"> Jean Sean
                                                                                <br class="clear"> <span>Experienced fashion designer from Dublin</span> </div>
                                                                            <div class="clear"></div>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <p>Sed scelerisque id, sodales et nulla aliquam eget dui ipsum vestibulum vel leo cursus velit. Proin sapien augue, finibus nec quam in, condimentum iaculis sem duis lobortis fermentum pulvinar. Proin sed posuere sapien ac tvestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae nunc tempor ac nisi. Vitae nibh tincidunt, eget posuere dolor lacinia ut ante mauris, lorem ullamcorper.&nbsp;Nulla faucibus eu elit quis efficitur. Mauris tincidunt ligula sed elit tristique euismod. Curabitur commodo eget tellus quis consectetur. Nam eget suscipit arcu, nec maximus orci.</p>
                                                        </div>
                                                    </div>
                                                    <h5 class="shortcode_tab_item_title expand_no">George Orwell</h5>
                                                    <div class="shortcode_tab_item_body tab-content clearfix">
                                                        <div class="ip">


                                                            <div class="widget_posts">
                                                                <ul class="recent_posts">
                                                                    <li>
                                                                        <span class="fa fa-times add_b del"></span>
                                                                        <a class="post_title read_more" href="user_profile.php">
                                                                            <div class="recent_posts_img"><img src="img/imgs/team1.jpg" ></div>
                                                                            <div class="recent_posts_content">Craig Bennert
                                                                                <br class="clear"> <span>Bridal Design Expert</span> </div>
                                                                            <div class="clear"></div>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>

                                                            <p>Vitae nibh tincidunt, eget posuere dolor lacinia ut ante mauris, lorem ullamcorper.&nbsp;Nulla faucibus eu elit quis efficitur. Mauris tincidunt ligula sed elit tristique euismod. Curabitur commodo eget tellus .</p>
                                                        </div>
                                                    </div>
                                                    <h5 class="shortcode_tab_item_title expand_no"> Mary J Blige </h5>
                                                    <div class="shortcode_tab_item_body tab-content clearfix">
                                                        <div class="ip">

                                                            <div class="widget_posts">
                                                                <ul class="recent_posts">
                                                                    <li>
                                                                        <span class="fa fa-times add_b del"></span>
                                                                        <a class="post_title read_more" href="user_profile.php">
                                                                            <div class="recent_posts_img"><img src="img/imgs/avatar3.jpg" alt="Underground Walls"></div>
                                                                            <div class="recent_posts_content">Jennifer Biongo
                                                                                <br class="clear"> <span>Freestyle Hair Stylist</span> </div>
                                                                            <div class="clear"></div>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>

                                                            <p>Nam eget suscipit arcu, nec maximus orci. Sed scelerisque id, sodales et nulla aliquam eget dui ipsum vestibulum vel leo cursus velit. Proin sapien augue, finibus nec quam in, condimentum iaculis sem duis lobortis fermentum pulvinar. Proin sed posuere sapien ac tvestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae nunc tempor ac nisi. Vitae nibh tincidunt, eget posuere dolor lacinia ut ante mauris, lorem ullamcorper.&nbsp;Nulla faucibus eu elit quis efficitur. Mauris tincidunt ligula sed elit tristique euismod. Curabitur commodo eget tellus quis consectetur.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="span12 module_number_16 module_cont pb5 pt10 module_promo_text">
                                        <a href="my_profile.php" class="hasIcon shortcode_button btn_normal btn_type17"><i class=" fa fa-save"></i>Save</a>
                                        <a href="my_profile.php" class="  hasIcon shortcode_button btn_normal btn_type2"><i class=" fa fa-times"></i>Cancel</a>
                                    </div>
                                </div>

                            </div-->
                                    </div>


                                    <!--    <div class="contentarea">
                                            <div class="row">
                                                <div class="span12 first-module module_number_1 module_cont pb0 module_blog no_nav">
                                                    <div class="blog_post_preview preview_type2">
                                                        <div class="blog_content">
                                                            <div class="preview_top">
                                                                <div class="preview_title">
                                                                    <h4 class="blogpost_title"><a href="blog_post_fullwith_slider.html">Meet Soho HTML Template</a></h4>
                                                                    <div class="listing_meta"> by <a href="javascript:void(0)">gt3dev</a><span class="middot">&middot;</span> <span>December 19, 2020</span><span class="middot">&middot;</span> <span>in <a href="javascript:void(0)">Streets</a></span><span class="middot">&middot;</span> <span><a href="javascript:void(0)">5 comments</a></span> <span class="middot">&middot;</span><span class="preview_meta_tags"> <a href="javascript:void(0)">css3</a>, <a href="javascript:void(0)">html5</a>, <a href="javascript:void(0)">photo</a>, <a href="javascript:void(0)">portfolio</a>, <a href="javascript:void(0)">wordpress</a></span> </div>
                                                                </div>
                                                                <div class="gallery_likes_add preview_likes"> <i class="stand_icon icon-heart-o"></i> <span>126</span> </div>
                                                            </div>
                                                            <div class="pf_output_container">
                                                                <div class="slider-wrapper theme-default ">
                                                                    <div class="nivoSlider"> <img src="img/blog/1170_563/1.jpg" alt="" /> <img src="img/blog/1170_563/2.jpg" alt="" /> <img src="img/blog/1170_563/3.jpg" alt="" /> </div>
                                                                </div>
                                                            </div>
                                                            <article class="contentarea"> Nullam a massa tellus. Nulla tempor velit non tincidunt facilisis. Proin sapien augue, finibus nec quam in, condimentum iaculis sem duis lobortis egestas fermentum pulvinar. Proin sed posuere sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae. Nunc tempor nisi vitae nibh tincidunt, eget posuere dolor lacinia. Ut ante mauris, ullamcorper sed scelerisque id, sodales et nulla. Aliquam eget dui ipsum vestibulum vel leo cursus velit fermentum viverra. Nam nunc massa, molestie vel sollicitudin sit amet, consectetur a risus phasellus tristique diam.
                                                                <br class="clear"> <a href="blog_post_fullwith_slider.html" class="preview_read_more">Read More</a> </article>
                                                        </div>
                                                    </div>
                                                    <div class="blog_post_preview preview_type2">
                                                        <div class="blog_content">
                                                            <div class="preview_top">
                                                                <div class="preview_title">
                                                                    <h4 class="blogpost_title"><a href="blog_post_fullwith_slider.html">10 Most Anticipated Events of 2020</a></h4>
                                                                    <div class="listing_meta"> by <a href="javascript:void(0)">gt3dev</a><span class="middot">&middot;</span> <span>December 19, 2020</span><span class="middot">&middot;</span> <span>in <a href="javascript:void(0)">Events</a></span><span class="middot">&middot;</span> <span><a href="javascript:void(0)">3 comments</a></span> <span class="middot">&middot;</span><span class="preview_meta_tags"> <a href="javascript:void(0)">events</a>, <a href="javascript:void(0)">new york</a>, <a href="javascript:void(0)">people</a>, <a href="javascript:void(0)">photography</a>, <a href="javascript:void(0)">soho</a></span> </div>
                                                                </div>
                                                                <div class="gallery_likes_add preview_likes already_liked"> <i class="stand_icon icon-heart"></i> <span>118</span> </div>
                                                            </div>
                                                            <div class="pf_output_container"><img class="featured_image_standalone" src="img/blog/1170_563/my_image.jpg" alt="" /></div>
                                                            <article class="contentarea"> Nunc tempor nisi vitae nibh tincidunt, eget posuere dolor lacinia. Ut ante mauris, ullamcorper sed scelerisque id, sodales et nulla. Aliquam eget dui ipsum vestibulum vel leo cursus velit fermentum viverra. Nam nunc massa, molestie vel sollicitudin sit amet, consectetur a risus. Phasellus tristique diam nisl, eu bibendum urna, venenatis varius. Nullam a massa tellus. Nulla tempor velit non tincidunt facilisis. Proin sapien augue, finibus nec quam in, condimentum iaculis sem duis lobortis egestas fermentum pulvinar. Proin sed posuere sapien. Vestibulum ante ipsum primis in faucibus orci.
                                                                <br class="clear"> <a href="blog_post_fullwith_slider.html" class="preview_read_more">Read More</a> </article>
                                                        </div>
                                                    </div>
                                                    <div class="blog_post_preview preview_type2">
                                                        <div class="blog_content">
                                                            <div class="preview_top">
                                                                <div class="preview_title">
                                                                    <h4 class="blogpost_title"><a href="blog_post_fullwith_slider.html">Underground Walls</a></h4>
                                                                    <div class="listing_meta"> by <a href="javascript:void(0)">gt3dev</a><span class="middot">&middot;</span> <span>December 19, 2020</span><span class="middot">&middot;</span> <span>in <a href="javascript:void(0)">Streets</a></span><span class="middot">&middot;</span> <span><a href="javascript:void(0)">5 comments</a></span> <span class="middot">&middot;</span><span class="preview_meta_tags"> <a href="javascript:void(0)">new york</a>, <a href="javascript:void(0)">photography</a>, <a href="javascript:void(0)">places</a>, <a href="javascript:void(0)">streets</a>, <a href="javascript:void(0)">wordpress</a></span> </div>
                                                                </div>
                                                                <div class="gallery_likes_add preview_likes "> <i class="stand_icon icon-heart-o"></i> <span>44</span> </div>
                                                            </div>
                                                            <div class="pf_output_container">
                                                                <div class="slider-wrapper theme-default ">
                                                                    <div class="nivoSlider"> <img src="img/blog/1170_563/5.jpg" alt="" /> <img src="img/blog/1170_563/6.jpg" alt="" /> <img src="img/blog/1170_563/7.jpg" alt="" /> <img src="img/blog/1170_563/default_image.jpg" alt="" /> </div>
                                                                </div>
                                                            </div>
                                                            <article class="contentarea"> Vestibulum ante ipsum primis in faucibus orci luctus et ultrices. Aliquam eget dui ipsum vestibulum vel leo cursus velit fermentum viverra. Nam nunc massa, molestie vel sollicitudin sit amet, consectetur a risus. Phasellus tristique diam nisl, eu bibendum urna, venenatis varius. Nullam a massa tellus. Nulla tempor velit non tincidunt facilisis. Proin sapien augue, finibus nec quam in, condimentum iaculis sem duis lobortis egestas fermentum pulvinar. Proin sed posuere sapien lorem ipsum vestibulum ante ipsum primis in faucibus orci luctus et ultrices. Aliquam eget dui ipsum vestibulum.
                                                                <br class="clear"> <a href="blog_post_fullwith_slider.html" class="preview_read_more">Read More</a> </article>
                                                        </div>
                                                    </div>
                                                    <div class="blog_post_preview preview_type2">
                                                        <div class="blog_content">
                                                            <div class="preview_top">
                                                                <div class="preview_title">
                                                                    <h4 class="blogpost_title"><a href="blog_post_fullwith_slider.html">Monument Station Opened</a></h4>
                                                                    <div class="listing_meta"> by <a href="javascript:void(0)">gt3dev</a><span class="middot">&middot;</span> <span>December 19, 2020</span><span class="middot">&middot;</span> <span>in <a href="javascript:void(0)">Places</a></span><span class="middot">&middot;</span> <span><a href="javascript:void(0)">3 comments</a></span> <span class="middot">&middot;</span><span class="preview_meta_tags"> <a href="javascript:void(0)">events</a>, <a href="javascript:void(0)">new york</a>, <a href="javascript:void(0)">places</a>, <a href="javascript:void(0)">soho</a>, <a href="javascript:void(0)">streets</a>, <a href="javascript:void(0)">video</a></span> </div>
                                                                </div>
                                                                <div class="gallery_likes_add preview_likes"> <i class="stand_icon icon-heart-o"></i> <span>39</span> </div>
                                                            </div>
                                                            <div class="pf_output_container">
                                                                <iframe src="../../player.vimeo.com/video/19536258.htm" height="665" allowFullScreen></iframe>
                                                            </div>
                                                            <article class="contentarea"> Suspendisse at luctus sapien vel ornare ligula placerat sed Suspendisse sit amet sapien et mi cursus faucibus. Integer metus mauris, dictum vehicula vestibulum eu, vehicula at tellus. Donec id pulvinar mi. Nam eget lacinia eros pellentesque tristique et enim. Eu condimentum. Cras lectus ipsum, commodo a porttitor ac, convallis vel enim. Vivamus quis elementum metus, non vestibulum odio. Sed tincidunt purus in risus faucibus malesuada. Cras ligula nulla, condimentum quis egestas at, sagittis eget lorem. Nam luctus lectus felis, id accumsan diam consequat et maecenas aliquet elit.
                                                                <br class="clear"> <a href="blog_post_fullwith_slider.html" class="preview_read_more">Read More</a> </article>
                                                        </div>
                                                    </div>
                                                    <div class="blog_post_preview preview_type2">
                                                        <div class="blog_content">
                                                            <div class="preview_top">
                                                                <div class="preview_title">
                                                                    <h4 class="blogpost_title"><a href="blog_post_fullwith_slider.html">Awesome Street Art by Beau Stanton</a></h4>
                                                                    <div class="listing_meta"> by <a href="javascript:void(0)">gt3dev</a><span class="middot">&middot;</span> <span>December 19, 2020</span><span class="middot">&middot;</span> <span>in <a href="javascript:void(0)">People</a></span><span class="middot">&middot;</span> <span><a href="javascript:void(0)">3 comments</a></span> <span class="middot">&middot;</span><span class="preview_meta_tags"> <a href="javascript:void(0)">css3</a>, <a href="javascript:void(0)">gallery</a>, <a href="javascript:void(0)">html5</a>, <a href="javascript:void(0)">photo</a>, <a href="javascript:void(0)">soho</a>, <a href="javascript:void(0)">streets</a></span> </div>
                                                                </div>
                                                                <div class="gallery_likes_add preview_likes"> <i class="stand_icon icon-heart-o"></i> <span>25</span> </div>
                                                            </div>
                                                            <div class="pf_output_container">
                                                                <div class="slider-wrapper theme-default ">
                                                                    <div class="nivoSlider"> <img src="img/blog/1170_563/9.jpg" alt="" /> <img src="img/blog/1170_563/10.jpg" alt="" /> <img src="img/blog/1170_563/11.jpg" alt="" /> <img src="img/blog/1170_563/12.jpg" alt="" /> </div>
                                                                </div>
                                                            </div>
                                                            <article class="contentarea"> Ut semper sapien a nulla sollicitudin, at tempus nulla efficitur. Praesent non felis sem. Suspendisse at luctus sapien. Nullam a massa tellus. Nulla tempor velit non tincidunt facilisis. Proin sapien augue, finibus nec quam in, condimentum iaculis sem. Duis lobortis fermentum pulvinar. Proin sed posuere sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae. Nunc tempor nisi vitae nibh tincidunt, eget posuere dolor lacinia. Ut ante mauris, ullamcorper sed scelerisque id, sodales et nulla. Aliquam eget dui ipsum vestibulum vel leo cursus velit.
                                                                <br class="clear"> <a href="blog_post_fullwith_slider.html" class="preview_read_more">Read More</a> </article>
                                                        </div>
                                                    </div>
                                                    <ul class="pagerblock">
                                                        <li><a href="javascript:void(0)" class="current">1</a></li>
                                                        <li><a href="javascript:void(0)">2</a></li>
                                                        <li><a href="javascript:void(0)">3</a></li>
                                                        <li><a href="javascript:void(0)">4</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>-->
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    @include("incs.footer_stuffs")



</div>



<!--    notification panel slides-->


@include("incs.notification_panels")


        <!--    end notification panel slides-->


@include("incs.imports_bottom")
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>

    $(".cc_selector").find("select").change(
            function()
            {
                $("#city_id").val("");
                $("#city").val("");
            }
    );

    $("#city").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: "{{env("ELASTIC_SEARCH_CLUSTER")}}_search",
                dataType: "json",
                data: {
                    q : 'name:'+request.term+' AND '+'country_code:'+$('select[name= country]').val()
                },
                success: function(data) {
                    var options = [];
                    for(var i=0; i<data.hits.hits.length; i++){
                        var city_data = {key:data.hits.hits[i]._source.id, value:data.hits.hits[i]._source.name};
                        options.push(city_data);
                    }
                    console.log(options);
                    response(options);
                },
            });
        },
        focus: function( event, ui ) {
            $( "#city" ).val( ui.item.value );
            return false;
        },
        select: function( event, ui ) {
            $( "#city" ).val( ui.item.value );
            $( "#city_id" ).val( ui.item.key );

            return false;
        },





        min_length: 3,
        delay: 300
    });
    watchInterest();
</script>

</body>

</html>
