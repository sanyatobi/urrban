/*
var api_url = "http://localhost:8000/api/v1/";
var site_url = "http://localhost:8000/";*/
var api_url = "http://urrban.herokuapp.com/api/v1/";
var site_url = "http://urrban.herokuapp.com/";
var user_id = localStorage.getItem('user_id');
const MODELLING_ID = 2;


function showLoader(){
    console.log("Showing Loader");
}
function hideLoader(){
    console.log("Hiding Loader");
}

function showError(msg){
    if(typeof (msg) == "string")
        alert(msg);
    else {
        alert("error logged");
        console.log(msg);
    }

}
var randDim =  function () {
     return Math.floor((Math.random() * 200) + 100);

}

var populateFeed = function() {

    console.log("starting populating");


    doAjax('feed', 'GET', 'json', '', true)
        .then(function(result){

            var markup = "";

                $.each( result.message, function( key, feed ) {

                var pieces = feed.content.split(" "),
                    first_part = pieces.splice(0,10).join(" "),
                    title = str_slug(first_part,'-'),
                    liked_flag = "notliked",
                    likes = array_column(feed.likes,'user_id');

                if(in_array(likes,parseInt(user_id)) >= 0) {
                    liked_flag = "liked";
                }

                markup += "<div class='blogpost_preview_fw'><button value='"+feed.id+"' class='like_botton "+liked_flag+" but_toggler wallpost'><span class='fa fa-heart'></span></button><div class='fw_preview_wrapper featured_items'>";

                    if (feed.image_url) {
                    markup += "<div class='img_block wrapped_img'><div class='pf_output_container'><a href='"+url('post/'+feed.id+'/'+title)+"'><img class='featured_image_standalone'src='http://placehold.it/"+randDim()+"x"+randDim()+"?text=...' alt='' /></a> </div> </div>";
                }

                    if (feed.video_url) {
                    markup += "<div class='img_block wrapped_img'><div class='pf_output_container vodeo_grid'><a href="+url('post/'+feed.id+'/'+title)+"> <video controls preload='none'><source src='"+url('media_feed/'+feed.video_url)+"'  type='video/mp4' id='video-id'> </video> </a> </div> </div>";
                    }

                   markup += "<div class='bottom_box' > <div class='bc_content'><h5 class='bc_title'><a href='"+url('post/'+feed.id+'/'+title)+"'>"+feed.content+"</a></h5><div class='featured_items_meta'> <span class='preview_meta_data'>"+feed.updated_at+"</span><span class='middot'>&middot;</span> <span><a href='/categories/"+feed.owner.interests.slug+"'>"+feed.owner.interests.title+"</a></span> <span class='middot'>&middot;</span> </div></div>";

                markup += "<div class='bc_likes gallery_likes_add '> <div class='cell_info_hrts_cmm'> <i class=' fa fa-heart'></i> <span id='fd_lks"+feed.id+"'>"+feed.likes.length+"</span> </div> <div class='cell_info_hrts_cmm'> <i class=' fa fa-comment'></i> <span id='fd_cmnts'"+feed.id+">"+feed.comments.length+"</span> </div> </div> <div class='row'> <div class='grid_det_bottom'> <div class='grip_u_pic_con'> <a href='"+ url(feed.owner.username)+"'><img src='"+url('media_avatar/'+feed.owner.avatar)+"' /></a></div><div class='grid_det_bottom_text'> <div class='who'> <a href='"+url(feed.owner.username)+"'>"+feed.owner.name+"</a> </div> </div> </div> </div> </div> </div></div>";


            });

            $(".is_masonry").masonry()
                .append( markup )
                .masonry( 'appended', markup )
                .masonry();
        });

    console.log("populating done");

}
function loadFeeds() {


    doAjax('feed', 'GET', 'json', '', true)
        .then(function(result){

            var markup = "";

            $.each( result.message, function( key, feed ) {

                var pieces = feed.content.split(" "),
                    first_part = pieces.splice(0,10).join(" "),
                    title = str_slug(first_part,'-'),
                    liked_flag = "notliked",
                    likes = array_column(feed.likes,'user_id');

                if(in_array(likes,parseInt(user_id)) >= 0) {
                    liked_flag = "liked";
                }

                markup += "<div class='blogpost_preview_fw'><button value='"+feed.id+"' class='like_botton "+liked_flag+" but_toggler wallpost'><span class='fa fa-heart'></span></button><div class='fw_preview_wrapper featured_items'>";

                if (feed.image_url) {
                    markup += "<div class='img_block wrapped_img'><div class='pf_output_container'><a href='"+url('post/'+feed.id+'/'+title)+"'><img class='featured_image_standalone' src='http://placehold.it/"+randDim()+"x"+randDim()+"?text=...' alt='' /></a> </div> </div>";
                }

                if (feed.video_url) {
                    markup += "<div class='img_block wrapped_img'><div class='pf_output_container vodeo_grid'><a href="+url('post/'+feed.id+'/'+title)+"> <video controls preload='none'><source src='"+url('media_feed/'+feed.video_url)+"'  type='video/mp4' id='video-id'> </video> </a> </div> </div>";
                }

                markup += "<div class='bottom_box' > <div class='bc_content'><h5 class='bc_title'><a href='"+url('post/'+feed.id+'/'+title)+"'>"+feed.content+"</a></h5><div class='featured_items_meta'> <span class='preview_meta_data'>"+feed.updated_at+"</span><span class='middot'>&middot;</span> <span><a href='/categories/"+feed.owner.interests.slug+"'>"+feed.owner.interests.title+"</a></span> <span class='middot'>&middot;</span> </div></div>";

                markup += "<div class='bc_likes gallery_likes_add '> <div class='cell_info_hrts_cmm'> <i class=' fa fa-heart'></i> <span id='fd_lks"+feed.id+"'>"+feed.likes.length+"</span> </div> <div class='cell_info_hrts_cmm'> <i class=' fa fa-comment'></i> <span id='fd_cmnts'"+feed.id+">"+feed.comments.length+"</span> </div> </div> <div class='row'> <div class='grid_det_bottom'> <div class='grip_u_pic_con'> <a href='"+ url(feed.owner.username)+"'><img src='"+url('media_avatar/'+feed.owner.avatar)+"' /></a></div><div class='grid_det_bottom_text'> <div class='who'> <a href='"+url(feed.owner.username)+"'>"+feed.owner.name+"</a> </div> </div> </div> </div> </div> </div></div>";


            });


            $("#feed_area").append(markup);
        });


}
$( document ).ready(function() {
    populateFeed();
});
function showSuccess(msg){
    alert(msg);
}
function redirect(link){
    window.location.replace(site_url+link);
}
var reloadPage = function() {
    window.location.reload();
}

/*function checkLoginStatus() {
    var token = localStorage.getItem('token');
   if(token) {
       doAjax('token/verify','GET','json','token='+token,true)
           .then(function(result) {
               if(result.email) {
                   redirect('feed');
               }
           }).catch(function() {
               console.log("Error Occured while verifying user");
           });
   }

}*/

var  str_slug = function(title, separator) {
    if(typeof separator == 'undefined') separator = '-';

    // Convert all dashes/underscores into separator
    var flip = separator == '-' ? '_' : '-';
    title = title.replace(flip, separator);

    // Remove all characters that are not the separator, letters, numbers, or whitespace.
    title = title.toLowerCase()
        .replace(new RegExp('[^a-z0-9' + separator + '\\s]', 'g'), '');

    // Replace all separator characters and whitespace by a single separator
    title = title.replace(new RegExp('[' + separator + '\\s]+', 'g'), separator);

    return title.replace(new RegExp('^[' + separator + '\\s]+|[' + separator + '\\s]+$', 'g'),'');
}

var relatedPosts = function(category,post_id) {
    doAjax('posts/related/'+category+'/'+post_id, 'GET', 'json', '', true)
        .then(function(result){
            var markup = "";
            if(result.message.length) {
                $("#relatedPostsHeader").show();
                $.each(result.message, function (index, value) {
                    var t = value.created_at.split(/[- :]/);
                    var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                    var url;
                    var pieces = value.content.split(" ");
                    var first_part = pieces.splice(0, 10).join(" ");
                    var title = str_slug(first_part, '-');
                    if (value.video_url) {
                        url = "<a href='" + site_url + "post/" + value.id + "/" + title + "'><video controls preload='none'><source src='" + site_url + "media_feed/" + value.video_url + "'  type='video/mp4' id='video-id'></video></a>";
                    } else if (value.image_url) {
                        url = "<a href='" + site_url + "post/" + value.id + "/" + title + "'><img alt='' src='" + site_url + "media_feed/" + value.image_url + "' /></a>";
                    }
                    markup += "<li><div class='item'><div class='item_wrapper'><div class='img_block wrapped_img'>" + url + "</div> <div class='featured_items_body featured_posts_body'><div class='featured_items_title'><h5><a href='javascript:void(0)'>" + value.content + "</a></h5><div class='preview_categ'> <span class='preview_meta_data'>" + d + "</span> </div></div><div class='featured_item_footer'><div class='gallery_likes gallery_likes_add ' data-attachid='173' data-modify='like_post'> <i class='stand_icon icon-heart-o'></i> <span>" + value.likes.length + "</span> </div><a class='morelink' href='" + site_url + "post/" + value.id + "/" + title + "''>Read more</a> </div></div></div></div></li>";
                });
                $('#relatedPosts').html(markup);
            }
        });
};

var delete_cookie = function(name) {
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};

var request_contact = function(user_id,name){
    if(confirm("Request to be added to "+name+"'s contact list")){
        doAjax('users/requestcontact','POST','json','user_id='+user_id)
            .then(function(result){
                showSuccess(result.message);
            });
    }
};

var watchInterest = function () {

    ($("#interest").val() == MODELLING_ID) ? $(".modelling_block").show() : $(".modelling_block").hide();
    ($("#interest").val() == MODELLING_ID) && ($("#gender").val() == 'female') ? $(".modelling_female_block").show() : $(".modelling_female_block").hide();

}


var suggestedContacts = function () {
    doAjax('users/'+user_id+'/contacts/suggested','GET','json','',true)
        .then(function(result){
            var markup = "";
            $.each(result, function (index, value) {
                markup+= "<li><span class='fa fa-user-plus add_b' onclick='request_contact(\""+value.id+"\",\""+value.name+"\")'></span><a class='post_title read_more' href='"+site_url+value.username+"'><div class='recent_posts_img'><img src='"+site_url+'media_avatar/'+value.avatar+"'></div><div class='recent_posts_content'>"+value.name+"<br class='clear'> <span>"+value.headline+"</span> </div> <div class='clear'></div></a></li>";
            });
            $('#suggestedContacts').html(markup);
        });
};

 var add_contact = function (user_id,name){
    if(confirm("Add "+name+" to your contacts list?")){
        showLoader();
        $.ajax({
            url: site_url+"user/addcontact" ,
            type: 'post',
            dataType: 'json',
            data  : {user_id:user_id}
        }).done(function(data) {
            hideLoader();
            if(data.status == "error"){
                showError(data.message);
                return;
            }
            else{
                location.reload();
            }
        }).fail(function() {
            hideLoader();
        });
    }
}
var remove_contact = function (user_id,name){
    if(confirm("Remove "+name+" from your contacts list?")){
        showLoader();
        $.ajax({
            url: site_url+"user/removecontact" ,
            type: 'post',
            dataType: 'json',
            data  : {user_id:user_id}
        }).done(function(data) {
            hideLoader();
            if(data.status == "error"){
                showError(data.message);
                return;
            }
            else{
                location.reload();
            }
        }).fail(function() {
            hideLoader();
        });
    }
}

function doAjax(url,method,type,data,silent) {
    silent = silent || false;
    var token = localStorage.getItem('token');
  return new Promise(function(resolve, reject) {
      showLoader();
      $.ajax({
          url: api_url+url,
          type: method,
          dataType: type,
          data  : data,
          headers: {"Authorization": "Bearer "+token}
      }).done(function(data) {
          hideLoader();
          if( (data.status == "error") && (!silent) ){
              showError(data.message);
              return;
          }
          else{
            resolve(data.data);
          }
      }).fail(function(data) {
          if(data.responseText) {
              var response = JSON.parse(data.responseText);
              if(!silent)
                  showError(response.message);
          }
          hideLoader();
      });
      });
  }


  function fetchCategories(element) {

      var categories = doAjax('utility/categories','GET','json','')
    .then(function(result) {

              $.each(result, function (index, value) {
                  $('#interests').append($('<option/>', {
                      value: value.id,
                      text : value.title
                  }));
              });
              $(function(){
                  $('select').selectric({
                      maxHeight: 200
                  });
                  $('.customOptions').selectric({
                      optionsItemBuilder: function(itemData, element, index) {
                          return element.val().length ? '<span class="ico ico-' + element.val() +  '"></span>'
                          + itemData.text : itemData.text;
                      }
                  });
              });
    }).catch(function() {
        console.log("Error Occured while fetching categories");
      });
  }
$(document).ready(function() {

    $("#album_title").keypress(function(event) {
        if (event.which == 13) {
            $("#create_album").trigger("click");
        }
    });

    $("#create_album").click(function(){

        var album_title = $("#album_title").val();
        var username = $("#username").val();

        if(album_title == ''){
            showError("Enter a name for your album");
        }
        else{
            showLoader();
            $.ajax({
                url: site_url+username+"/gallery" ,
                type: 'post',
                dataType: 'json',
                data  : {album_title:album_title}
            }).done(function(data) {
                hideLoader();
                if(data.status == "error"){
                    showError(data.message);
                    return;
                }
                else{
                    reloadPage();

                }
            }).fail(function() {
                hideLoader();
            });
        }

    });

    $("#deleteAlbumButton").click(function(){

        var album_id = $('#album_id').val();

        if(confirm('Delete an entire album?')){
            showLoader();
            $.ajax({
                url: site_url+"gallery",
                type: 'delete',
                dataType: 'json',
                data  : {album_id:album_id}
            }).done(function(data) {
                hideLoader();
                if(data.status == "error"){
                    showError(data.message);
                    return;
                }
                else{
                    redirect("");
                }
            }).fail(function() {
                hideLoader();
            });
        }

    });

    $(".editAlbumButton").click(function(event){
        var album_title = $('#mc_s_fm_txt_b').val();
        var album_id = $('#album_id').val();
        var privilege = $('input[name=privilege]:checked').val();
        if(album_title == ''){
            showError('Enter an Album Title');
            return;
        }
        showLoader();
        $.ajax({
            url: site_url+"gallery",
            type: 'put',
            dataType: 'json',
            data  : {album_title:album_title,album_id:album_id,privilege:privilege}
        }).done(function(data) {
            hideLoader();
            if(data.status == "error"){
                showError(data.message);
                return;
            }
            else{
                redirect("");
            }
        }).fail(function() {
            hideLoader();
        });
    });

    $(".saveSettingsButton").click(function(){
        var old_username = $("#old_username").val();
        var username = $("#username").val();
        var email = $("#email").val();
        var password = $("#password").val();
        var password_conf = $("#password_conf").val();
        var email_notifications = $("#email_notifications").val();
        var notification_categories = $(".notification_categories:checked").map(function(){
            return $(this).val();
        }).get();
        var privacy_contact = $("#privacy_contact").val();
        var privacy_look_up = $("#privacy_look_up").val();
        var use_email = $("#use_email").is(':checked');
        var use_number = $("#use_number").is(':checked');

        //check that username is not empty
        if(username == ''){
            showError('Please provide a username');
            return;
        }
        //check that passwords are equal
        if( (password != '')&&(password != password_conf) ){
            showError('Passwords do not match');
            return;
        }

        showLoader();

        $.ajax({
            url: site_url+old_username+"/settings",
            type: 'post',
            dataType: 'json',
            data  : {username:username,username:username,email:email,password:password,password_conf:password_conf,email_notifications:email_notifications,email_notifications:email_notifications,notification_categories:notification_categories,privacy_contact:privacy_contact,privacy_look_up:privacy_look_up,use_email:use_email,use_number:use_number}
        }).done(function(data) {
            hideLoader();
            if(data.status == "error"){
                showError(data.message);
                return;
            }
            else{
                $( "#logoutButton" ).trigger( "click" );
            }
        }).fail(function() {
            hideLoader();
        });

    });

    $("#saveButton").click(function(){
        var username = $("#username").val();
        var headline = $("#headline").html();
        var interest = $("#interest").val();
        var name = $("#name").val();
        var gender = $("#gender").val();
        var phone1 = $("#phone1").val();
        var phone2 = $("#phone2").val();
        var dob = $("#dob").val();
        var country = $('select[name= country]').val();
        var city_id = $("#city_id").val();
        var side_gig = $("#side_gig").val();
        var language = $("#languages").val();
        var address = $("#address").val();
        var engaged_in = $("#engaged_in").val();
        var design = $("#design").val();
        var website = $("#website").val();
        var yib = $("#yib").val();
        var education = $("#education").val();
        var soq = $("#soq").val();
        var height = $("#height").val();
        var weight = $("#weight").val();
        var skin = $("#skin").val();
        var hair = $("#hair").val();
        var eye = $("#eye").val();
        var foot = $("#foot").val();
        var waist = $("#waist").val();
        var bust = $("#bust").val();
        var hip = $("#hip").val();

        if(headline.length > 500){
            showError("Headline cannot be more than 500 characters");
            return;
        }
        doAjax(username+"/edit",'PUT','json','username='+username+'&headline='+headline+'&interest='+interest+'&name='+name+'&gender='+gender+'&phone1='+phone1+'&phone2='+phone2+'&dob='+dob+'&country='+country+'&city_id='+city_id+'&side_gig='+side_gig+'&language='+language+'&address='+address+'&design='+design+'&engaged_in='+engaged_in+'&website='+website+'&yib='+yib+'&education='+education+'&soq='+soq+'&height='+height+'&weight='+weight+'&skin='+skin+'&hair='+hair+'&eye='+eye+'&foot='+foot+'&waist='+waist+'&bust='+bust+'&hip='+hip)
            .then(function(result) {
                showSuccess(result.message);
                redirect(username);
            });
    });

    $("#signUpButton").click(function(event){
        event.preventDefault();
        if( !$("#name").val() || (!$("#signup_email").val()|| (!$("#signup_password").val())|| (!$("#interests").val()))){
            showError("A field is missing");
        }
        else{
            var name = $("#name").val();
            var email = $("#signup_email").val();
            var password = $("#signup_password").val();
            var interests = $("#interests").val();

            doAjax('user','POST','json','name='+name+'&email='+email+'&password='+password+'&interests='+interests)
                .then(function(result) {
                    if(result.token) {
                        //store token in local storage
                        localStorage.setItem('token', result.token);
                        localStorage.setItem('user_id', result.user_id);
                        document.cookie="urrbanq-token="+result.token;

                        redirect('feed');
                    }
                }).catch(function() {
                    console.log("Error Occured while creating account");
                });
        }
    });

    $("#loginButton").click(function (event) {
        event.preventDefault();

        if( !$("#email").val() || (!$("#password").val())){
            showError("A field is missing");
        }
        else{
            var email = $("#email").val();
            var password = $("#password").val();
            doAjax('user/login','POST','json','email='+email+'&password='+password)
                .then(function(result) {
                    console.log(result);
                    if(result.token) {
                        //store token in local storage
                        localStorage.setItem('token', result.token);
                        localStorage.setItem('user_id', result.user_id);
                        document.cookie="urrbanq-token="+result.token;

                        redirect('feed');
                    }
                }).catch(function() {
                    console.log("Error Occured while logging in");
                });
        }

    });

    $("#logoutButton").click(function(event){
        event.preventDefault();
        localStorage.clear();
        delete_cookie("urrbanq-token");
        redirect("login")
    });
});

