"use strict";

function content_update() {
	jQuery(".contacts_map").size() > 0 && (map_h = jQuery(".contacts_map").height() + parseInt(jQuery(".contacts_map").css("margin-bottom"), 10)), main_wrapper_min = window_h - header_h - footer.height() - parseInt(jQuery(".site_wrapper").css("padding-top"), 10) - parseInt(jQuery(".site_wrapper").css("padding-bottom"), 10), jQuery(".contacts_map").size() > 0 && jQuery(".content_wrapper").css("min-height", main_wrapper_min - map_h), fs_min = window_h - header_h - footer.height(), main_wrapper.css("min-height", main_wrapper_min + "px"), jQuery(".fullscreen_block").size() > 0 && jQuery(".fullscreen_block").css("min-height", fs_min + "px");
	var e = (81 - parseInt(main_li.children("a").css("line-height"), 10)) / 2 - 1,
		t = e + 2;
	main_li.children("a").css({
		"padding-top": e + "px",
		"padding-bottom": t + "px"
	})/*, jQuery(".contacts_map").size() > 0 ? (right_sidebar.size() > 0 && right_sidebar.height() < main_wrapper.height() - map_h && right_sidebar.height(main_wrapper.height() - map_h), left_sidebar.size() > 0 && left_sidebar.height() < main_wrapper.height() - map_h && left_sidebar.height(main_wrapper.height() - map_h)) : (right_sidebar.size() > 0 && right_sidebar.height() < main_wrapper.height() && right_sidebar.height(main_wrapper.height()), left_sidebar.size() > 0 && left_sidebar.height() < main_wrapper.height() && left_sidebar.height(main_wrapper.height()))*/
}
function iframe16x9(e) {
	e.find("iframe").each(function() {
		jQuery(this).height(jQuery(this).width() / 16 * 9)
	})
}
function pp_center() {
	var e = jQuery(".pp_block");
	setTop = (window_h - e.height()) / 2, e.css("top", setTop + "px"), e.removeClass("fixed")
}
function fullscrn_bg() {
	jQuery(".fw_background.bg_image").size() > 0 ? jQuery(".fw_background").height(jQuery(window).height()) : jQuery(".fw_background.bg_video").size() > 0 && (jQuery(".fw_background").height(jQuery(window).height()), jQuery(window).width() > 1024 ? jQuery(".bg_video").size() > 0 && ((jQuery(window).height() + 150) / 9 * 16 > jQuery(window).width() ? (jQuery("iframe").height(jQuery(window).height() + 150).width((jQuery(window).height() + 150) / 9 * 16), jQuery("iframe").css({
		"margin-left": -1 * jQuery("iframe").width() / 2 + "px",
		top: "-75px",
		"margin-top": "0px"
	})) : (jQuery("iframe").width(jQuery(window).width()).height(jQuery(window).width() / 16 * 9), jQuery("iframe").css({
		"margin-left": -1 * jQuery("iframe").width() / 2 + "px",
		"margin-top": -1 * jQuery("iframe").height() / 2 + "px",
		top: "50%"
	}))) : jQuery(window).width() < 760 ? jQuery(".bg_video, iframe").height(window_h - header.height()).width(window_w).css({
		top: "0px",
		left: "0px",
		"margin-left": "0px",
		"margin-top": "0px"
	}) : jQuery(".bg_video, iframe").height(window_h).width(window_w).css({
		top: "0px",
		"margin-left": "0px",
		left: "0px",
		"margin-top": "0px"
	}))
}
var header = jQuery(".main_header"),
	header_h = header.height(),
	headerWrapper = jQuery(".header_wrapper"),
	menu = header.find("ul.menu"),
	main_li = menu.children("li"),
	html = jQuery("html"),
	body = jQuery("body"),
	footer = jQuery(".main_footer"),
	window_h = jQuery(window).height(),
	window_w = jQuery(window).width(),
	main_wrapper = jQuery(".main_wrapper"),
	main_wrapper_min = window_h - header_h - footer.height() - parseInt(jQuery(".site_wrapper").css("padding-top"), 10) - parseInt(jQuery(".site_wrapper").css("padding-bottom"), 10),
	right_sidebar = jQuery(".right-sidebar-block"),
	left_sidebar = jQuery(".left-sidebar-block"),
	site_wrapper = jQuery(".site_wrapper"),
	preloader_block = jQuery(".preloader"),
	fullscreen_block = jQuery(".fullscreen_block"),
	is_masonry = jQuery(".is_masonry"),
	grid_portfolio_item = jQuery(".grid-portfolio-item"),
	pp_block = jQuery(".pp_block"),
	fs_min = 0,
	map_h = 0,
	setTop = 0;
jQuery(document).ready(function() {
	if (jQuery(".main_wrapper").size() > 0) var e = setTimeout(function() {
		jQuery(".main_wrapper").animate({
			opacity: "1"
		}, 500), clearTimeout(e)
	}, 500);
	else if (jQuery(".fullscreen_block").size() > 0) var t = setTimeout(function() {
		jQuery(".fullscreen_block").animate({
			opacity: "1"
		}, 500), clearTimeout(t)
	}, 500);
	if (jQuery(".ribbon_wrapper").size() > 0) var i = setTimeout(function() {
		jQuery(".ribbon_wrapper").animate({
			opacity: "1"
		}, 1e3), clearTimeout(i)
	}, 500);
	if (jQuery(".pf_output_container").size() > 0) var r = setTimeout(function() {
		jQuery(".pf_output_container").animate({
			opacity: "1"
		}, 1e3), clearTimeout(r)
	}, 500);
	if (jQuery(".strip-template").size() > 0) var a = setTimeout(function() {
		jQuery(".strip-template").animate({
			opacity: "1"
		}, 1e3), clearTimeout(a)
	}, 500);
	if (jQuery(".fs_controls").size() > 0) var s = setTimeout(function() {
		jQuery(".fs_gallery_wrapper").animate({
			opacity: "1"
		}, 1e3), clearTimeout(s)
	}, 500);
	if (jQuery(".strip-landing").size() > 0) var o = setTimeout(function() {
		jQuery(".strip-landing").animate({
			opacity: "1"
		}, 1e3), jQuery(".landing_logo2").animate({
			opacity: "1"
		}, 1e3), clearTimeout(o)
	}, 500);
	if (760 > window_w && jQuery(".module_content").size() > 0 && jQuery(".module_content").each(function() {
		"" == jQuery.trim(jQuery(this).html()) && jQuery(this).parent(".module_cont").addClass("empty_module")
	}), 760 > window_w && jQuery(".module_blog_page").size() > 0 && iframe16x9(jQuery(".pf_output_container")), jQuery(".flickr_widget_wrapper").size() > 0) {
		var n = '';
		jQuery(".flickr_widget_wrapper").each(function() {
			jQuery(this).append(n)
		})
	}
	var d = setTimeout(function() {
		content_update(), clearTimeout(d)
	}, 300);
	header.find(".header_wrapper").append('<a href="javascript:void(0)" class="menu_toggler"></a>'), jQuery(".header_filter").size() > 0 ? jQuery(".header_filter").before('<div class="mobile_menu_wrapper"><ul class="mobile_menu container"/></div>') : header.append('<div class="mobile_menu_wrapper"><ul class="mobile_menu container"/></div>'), jQuery(".mobile_menu").html(header.find(".menu").html()), jQuery(".mobile_menu_wrapper").hide(), jQuery(".menu_toggler").on("click", function() {
		jQuery(".mobile_menu_wrapper").slideToggle(300), jQuery(".main_header").toggleClass("opened")
	}), pp_block.size() > 0 && (html.addClass("pp_page"), jQuery(".custom_bg").remove(), jQuery(".fixed_bg").remove(), pp_center()), (jQuery(".module_accordion").size() > 0 || jQuery(".module_toggle").size() > 0) && (jQuery(".shortcode_accordion_item_title").on("click", function() {
		jQuery(this).hasClass("state-active") || (jQuery(this).parents(".shortcode_accordion_shortcode").find(".shortcode_accordion_item_body").slideUp("fast", function() {
			content_update()
		}), jQuery(this).next().slideToggle("fast", function() {
			content_update()
		}), jQuery(this).parents(".shortcode_accordion_shortcode").find(".state-active").removeClass("state-active"), jQuery(this).addClass("state-active"))
	}), jQuery(".shortcode_toggles_item_title").on("click", function() {
		jQuery(this).next().slideToggle("fast", function() {
			content_update()
		}), jQuery(this).toggleClass("state-active")
	}), jQuery(".shortcode_accordion_item_title.expanded_yes, .shortcode_toggles_item_title.expanded_yes").each(function() {
		jQuery(this).next().slideDown("fast", function() {
			content_update()
		}), jQuery(this).addClass("state-active")
	})), jQuery(".shortcode_counter").size() > 0 && (jQuery(window).width() > 760 ? jQuery(".shortcode_counter").each(function() {
		if (jQuery(this).offset().top < jQuery(window).height()) {
			if (!jQuery(this).hasClass("done")) {
				var e = jQuery(this).find(".stat_count").attr("data-count");
				jQuery(this).find(".stat_temp").stop().animate({
					width: e
				}, {
					duration: 3e3,
					step: function(e) {
						var t = Math.floor(e);
						jQuery(this).parents(".counter_wrapper").find(".stat_count").html(t)
					}
				}), jQuery(this).addClass("done"), jQuery(this).find(".stat_count")
			}
		} else jQuery(this).waypoint(function() {
			if (!jQuery(this).hasClass("done")) {
				var e = jQuery(this).find(".stat_count").attr("data-count");
				jQuery(this).find(".stat_temp").stop().animate({
					width: e
				}, {
					duration: 3e3,
					step: function(e) {
						var t = Math.floor(e);
						jQuery(this).parents(".counter_wrapper").find(".stat_count").html(t)
					}
				}), jQuery(this).addClass("done"), jQuery(this).find(".stat_count")
			}
		}, {
			offset: "bottom-in-view"
		})
	}) : jQuery(".shortcode_counter").each(function() {
		var e = jQuery(this).find(".stat_count").attr("data-count");
		jQuery(this).find(".stat_temp").animate({
			width: e
		}, {
			duration: 3e3,
			step: function(e) {
				var t = Math.floor(e);
				jQuery(this).parents(".counter_wrapper").find(".stat_count").html(t), window_w > 760 && jQuery(this).parents(".counter_wrapper").find(".counter_body").width(jQuery(this).parents(".counter_wrapper").width() - 20 - jQuery(this).parents(".counter_wrapper").find(".stat_count").width())
			}
		}), jQuery(this).find(".stat_count")
	}, {
		offset: "bottom-in-view"
	})), jQuery(".shortcode_tabs").size() > 0 && (jQuery(".shortcode_tabs").each(function() {
		var e = 1;
		jQuery(this).find(".shortcode_tab_item_title").each(function() {
			jQuery(this).addClass("it" + e), jQuery(this).attr("whatopen", "body" + e), jQuery(this).addClass("head" + e), jQuery(this).parents(".shortcode_tabs").find(".all_heads_cont").append(this), e++
		});
		var e = 1;
		jQuery(this).find(".shortcode_tab_item_body").each(function() {
			jQuery(this).addClass("body" + e), jQuery(this).addClass("it" + e), jQuery(this).parents(".shortcode_tabs").find(".all_body_cont").append(this), e++
		}), jQuery(this).find(".expand_yes").addClass("active");
		var t = jQuery(this).find(".expand_yes").attr("whatopen");
		jQuery(this).find("." + t).addClass("active")
	}), jQuery(document).on("click", ".shortcode_tab_item_title", function() {
		jQuery(this).parents(".shortcode_tabs").find(".shortcode_tab_item_body").removeClass("active"), jQuery(this).parents(".shortcode_tabs").find(".shortcode_tab_item_title").removeClass("active");
		var e = jQuery(this).attr("whatopen");
		jQuery(this).parents(".shortcode_tabs").find("." + e).addClass("active"), jQuery(this).addClass("active"), content_update()
	})), jQuery(".shortcode_messagebox").size() > 0 && jQuery(".shortcode_messagebox").find(".box_close").on("click", function() {
		jQuery(this).parents(".module_messageboxes").fadeOut(400)
	}), jQuery(".shortcode_diagramm").size() > 0 && (jQuery(".chart").each(function() {
		jQuery(this).css({
			"font-size": jQuery(this).parents(".skills_list").attr("data-fontsize"),
			"line-height": jQuery(this).parents(".skills_list").attr("data-size")
		}), jQuery(this).find("span").css("font-size", jQuery(this).parents(".skills_list").attr("data-fontsize"))
	}), jQuery(window).width() > 760 ? jQuery(".skill_li").waypoint(function() {
		jQuery(".chart").each(function() {
			jQuery(this).easyPieChart({
				barColor: jQuery(this).parents("ul.skills_list").attr("data-color"),
				trackColor: jQuery(this).parents("ul.skills_list").attr("data-bg"),
				scaleColor: !1,
				lineCap: "square",
				lineWidth: parseInt(jQuery(this).parents("ul.skills_list").attr("data-width"), 10),
				size: parseInt(jQuery(this).parents("ul.skills_list").attr("data-size"), 10),
				animate: 1500
			})
		})
	}, {
		offset: "bottom-in-view"
	}) : jQuery(".chart").each(function() {
		jQuery(this).easyPieChart({
			barColor: jQuery(this).parents("ul.skills_list").attr("data-color"),
			trackColor: jQuery(this).parents("ul.skills_list").attr("data-bg"),
			scaleColor: !1,
			lineCap: "square",
			lineWidth: parseInt(jQuery(this).parents("ul.skills_list").attr("data-width"), 10),
			size: parseInt(jQuery(this).parents("ul.skills_list").attr("data-size"), 10),
			animate: 1500
		})
	})), jQuery(".contact_form").size() > 0 && jQuery("#ajax-contact-form").on("submit", function() {
		var e = $(this).serialize();
		return $.ajax({
			type: "POST",
			url: "contact_form/contact_process.php",
			data: e,
			success: function(e) {
				if ("OK" == e) {
					var t = '<div class="notification_ok">Your message has been sent. Thank you!</div>';
					jQuery("#fields").hide()
				} else var t = e;
				jQuery("#note").html(t)
			}
		}), !1
	}), jQuery(".nivoSlider").size() > 0 && jQuery(".nivoSlider").each(function() {
		jQuery(this).nivoSlider({
			directionNav: !1,
			controlNav: !0,
			effect: "fade",
			pauseTime: 4e3,
			slices: 1
		})
	})/*, jQuery(".photozoom").size() > 0 && (jQuery(".photozoom").parents(".photo_gallery").hasClass("photo_gallery") ? jQuery(".photo_gallery").each(function() {
		jQuery(this).magnificPopup({
			delegate: "a",
			type: "image",
			gallery: {
				enabled: !0
			},
			iframe: {
				markup: '<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe><div class="mfp-counter"></div></div>'
			}
		})
	}) : jQuery(".photozoom").magnificPopup({
		type: "image"
	})), */jQuery(".fw_background").size() > 0 && fullscrn_bg()
}), jQuery(window).resize(function() {
	window_h = jQuery(window).height(), window_w = jQuery(window).width(), header_h = header.height(), content_update(), jQuery(".fw_background").size() > 0 && fullscrn_bg()
}), jQuery(window).load(function() {
	content_update()
});

