/**
 * Created by oluwatobi.okusanya on 15/05/16.
 */

var array_column = function (array,column) {

    var new_array=[];

    array.map(function(value,index) { new_array.push(value[column]); });

    return new_array

}

var in_array = function (array, value) {

    return array.indexOf(value);
}

var url = function (value) {
    return site_url+value;
}