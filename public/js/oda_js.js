function sticky_notifier_config(notif_msg, typ){
		$.toast({
				text: notif_msg, // Text that is to be shown in the toast
				icon: typ,
				showHideTransition: 'fade', // fade, slide or plain
				allowToastClose: true, // Boolean value true or false
				stack: 3,
				position: 'bottom-right',
				textAlign: 'left',  // Text alignment i.e. left, right or center
			   // textColor: 'white',
				//allowToastClose: false,
				hideAfter: false,
			});
	};

  function sticky_onload_notif() {
	  		var notif_msg="This is a test sticky notifier that loads on first login";
			var typ="warning";
			//sticky_notifier_config(notif_msg, typ);
			};

  function post_area_slide() {
			$( ".postarea_post_but" ).click(function() {
				$(".post_bg_inner").slideToggle(300);
				$(this).find('i').toggleClass( 'fa-pencil-square fa-times' );
				$(this).toggleClass( 'bg_o bg_lighter'  );
			});
	  };


function notifier_config(notif_msg, typ){
		$.toast({
				text: notif_msg, // Text that is to be shown in the toast
				icon: typ,
				showHideTransition: 'fade', // fade, slide or plain
				allowToastClose: true, // Boolean value true or false
				stack: 3,
				position: 'bottom-right',
				textAlign: 'left',  // Text alignment i.e. left, right or center
			   // textColor: 'white',
				//allowToastClose: false,
				hideAfter: 5000,
			});
	};

  function toggle_switch() {

	$( ".toggle_switch" ).click(function() {
				// $(".toggle_switch").slideToggle(300);
				$(this).toggleClass( 'fa-toggle-off fa-toggle-on' );
				$(this).toggleClass( 'toggle_off_ toggle_on_' );

				 if ($(this).find('span').text() == "Off"){
				   $(this).find('span').text("On");
				 }
				else{
				   $(this).find('span').text("Off");
				};

			});

	};


			var disp_flag=0;
	function g_t_cntrl_poper() {

		$( ".g_t_cntrl" ).click(function() {
		  var is_jst_icon= 	$(this).hasClass('jst_icon');
		  	if (!is_jst_icon){
	     		   $('.g_t_cntrl_pop').css('display','none');
			if (disp_flag==0){
				$(this).next('.g_t_cntrl_pop').show();
				disp_flag= disp_flag+1;

			}
			else{
				$(this).next('.g_t_cntrl_pop').hide();
				disp_flag= disp_flag-1;
			}
			};


			//alert (disp_flag);
	        //$('.g_t_cntrl_pop').toggle();
			/*if ($(this).hasClass('_open')){
	        	$(this).contents().replaceWith("<span><i class='fa  fa-folder-open'></i>Create Album</span>");
				$(this).toggleClass( '_open _close' );
				document.getElementById("mc_s_fm_txt_b").value = "";
			}
			else if ($(this).hasClass('_close')){
	        	$(this).contents().replaceWith("<span><i class='fa fa-times'></i>Cancel</span>");
				$(this).toggleClass( ' _close _open' );
			}*/

		});

				$( ".g_t_cntrl_pop .fa-times" ).click(function() {
					$('.g_t_cntrl_pop').hide();
					disp_flag= disp_flag-1;
				});
	};



	function request_button_switch() {

	$( ".endorse_butn" ).click(function() {
				$(this).toggleClass( 'btn_type5 btn_type3' );
			//	$(this).find('i').toggleClass( 'fa-send-o fa-times-circle' );

				 if ($(this).text() == "Send Request"){
				 //  $(this).text("Cancel Request");
				   $(this).contents().replaceWith("<span><i class='fa fa-times-circle'></i>Cancel Request </span>");
					var notif_msg="An endorsement request has been sent to John Smith";
					var typ="";
					notifier_config(notif_msg, typ);
					}
				else{
				   $(this).contents().replaceWith("<span><i class='fa fa-send-o'></i>Send Request</span>");
				   var notif_msg="Your endorsement request to John Smith has been withdrawn";
					var typ="";
					notifier_config(notif_msg, typ);
				};

			/*	if ($(this).hasClass( "endrse_sent" )){
					$(this).bind(' mouseover', function (){
						$(this).text("Send Request");
					});
				}
				*/
			});




	};


		function who_hw_changer_color() {
		//	alert ('efef');
			$('.who_hw').each(function(){
			//var load_div2color= $(this).find('.txti');
			var load_div2colorbtextselector= $(this).find('.txti').text();
			//alert (load_div2colorbtextselector);
			if (load_div2colorbtextselector== "Just met"){
					$(this).css('background','#C66161');
				}
				else if (load_div2colorbtextselector== "Quite familiar"){
					$(this).css('background','#D3CC6E');
				}
				else if (load_div2colorbtextselector== "Very familiar"){
					$(this).css('background','#70AF77');
				}
			//$(this).css('background','#000');

			});

			$( ".con_accept_options a" ).click(function() {
			//	var contact_rank_actual_text=$('#who_hw_changer').find('.txti');
				var contact_rank_div= $(this).parent().closest('.who_hw');
				var contact_rank_actual_text=$(this).parent().closest('.who_hw').find('.txti');
				var contact_rank=$(this).text();
				if (contact_rank== "Just met"){
					$(contact_rank_div).css('background','#C66161');
					contact_rank_actual_text.text(contact_rank);
				}
				else if (contact_rank== "Quite familiar"){
					$(contact_rank_div).css('background','#D3CC6E');
					contact_rank_actual_text.text(contact_rank);
				}
				else if (contact_rank== "Very familiar"){
					$(contact_rank_div).css('background','#70AF77');
					contact_rank_actual_text.text(contact_rank);
				}
				///alert (contact_rank);
			});

			/*var color = $('#who_hw_changer').val();
			$('.who_hw').css('background', color);
			$('.who_hw option').css('background', $('.who_hw option').val());
			*/
		};


/*
function set_footer_down(){
	if ($('body').hasClass( "login" )){

		if ($(document).height() <= $(window).height() ) {
			$('.footer_con').css('position', 'absolute');
			$('.footer_con').css('bottom', 0);
			$('.footer_con').css('margin-top', '-1px');
		}
		else{
			$('.footer_con').css('position', 'static');
		}
	};
};
*/
function recom_box_hyt(){
   var name_box_hyt= $('.all_head_sizer').height();
   alert (name_box_hyt);
};

/*function set_tooltip_values(){
			//$('.like_botton').attr('data-tooltip', "testiong first");
			$('.video_grid_play').qtip({
			//    prerender: true,
				content: {
					text: 'Preview video'
				},
				style: {
					//classes: 'qtip-blue qtip-tipsy'
				}
			});

			$('.like_botton').qtip({
			 //   prerender: true,
				content: {
					text: 'like this post'
				},
				style: {
				//	classes: 'qtip-blue qtip-tipsy'
				}
			});

			$('.add_contact_button').qtip({
			    prerender: true,
				content: {
					text: 'Add this User'
				},style: {
				//	classes: 'qtip-blue qtip-tipsy'
				}
			});
};
*/


	$( ".magnific_closer" ).click(function() {
		//$.magnificPopup.close();
		$.fancybox.close();
	});


function my_magnific_video_fu(){
	/*
	$( ".magnific_video_popper" ).click(function() {
		if ($(this).hasClass( "iframe_" )){
			  var src_value = '#magnific_video_pop_inline_iframe';
		  }
		 else if ($(this).hasClass( "html_video_" )){
			  var src_value = '#magnific_video_pop_inline_html_video';
		  }
		 else if ($(this).hasClass( "image_" )){
			  var src_value = '#magnific_video_pop_inline_image';
		  };


		$(this).magnificPopup({
		  items: {

			//  src: $('<div class="white-popup">Dynamically created popup</div>'),
				src: src_value,
			  type: 'inline'
		  },
		  gallery:{
			enabled:true
		  }

		});
		// alert ('segsh');
	});*/
};



/*

function my_magnific_video_fu(){

	$( ".add_contact_button" ).click(function() {
	$('.magnific_video_popper').magnificPopup({
	  items: {

		//  src: $('<div class="white-popup">Dynamically created popup</div>'),
			src: '#magnific_video_pop_inline',
		  type: 'inline'
	  }
	});
	});
};
*/

function add_user(){

	$( ".add_contact_button" ).click(function() {
		var has_added_user=$(this).hasClass( "user_added_yes" );

		var user_id = $(this).val();

		if (has_added_user){
			//if the user is already added, carry out the following
			$(this).removeClass( "user_added_yes");
			$(this).addClass( "notadded");
			$(this).find('span').removeClass( "fa-user-times");
			$(this).find('span').addClass( "fa-user-plus ");

			$.ajax({
				url: site_url+"user/unfollow",
				type: 'post',
				dataType: 'json',
				data  : {user_id:user_id}
			}).done(function(data) {
			}).fail(function() {
			});

			$("#fd_lks"+post_id).html(cnt_likes-1);

			var notif_msg="John has been removed from your contacts";
			var typ='';
			notifier_config(notif_msg, typ);
			$(this).qtip('option', 'content.text', 'Add this user');
		}
		else{
			//if the user is  not already added, carry out the following
			//iToast.showToast('A contact request has been sent to John',{theme: 'urbanq'});
			$(this).removeClass( "notadded");
			$(this).addClass( "user_added_yes");
			$(this).find('span').removeClass( "fa-user-plus");
			$(this).find('span').addClass( " fa-user-times");

			$.ajax({
				url: site_url+"user/follow",
				type: 'post',
				dataType: 'json',
				data  : {user_id:user_id}
			}).done(function(data) {
			}).fail(function() {
			});

			var notif_msg="A contact request has been sent to John";
			var typ="";
			notifier_config(notif_msg, typ);
			$(this).qtip('option', 'content.text', 'Remove this user');

		}

	});
};

function flagNotification(id,all){
	id = typeof id !== 'undefined' ? id : 0;
	all = typeof all !== 'undefined' ? all : 0;
	showLoader();
	$.ajax({
		url: site_url + "notifications/flag",
		type: 'put',
		dataType: 'json',
		data:{id:id,all:all}
	}).done(function (data) {
		hideLoader();
		if(all){
		window.location.reload();
		}
		else {
			window.location.href = site_url + data.link;
		}
	}).fail(function () {
		hideLoader();
	});
}

function clearNotification(id){
	showLoader();
	$.ajax({
		url: site_url + "notifications/flag",
		type: 'put',
		dataType: 'json',
		data:{id:id}
	}).done(function (data) {
		hideLoader();
		$("#notification_"+id).hide();
		countNotifications();
	}).fail(function () {
		hideLoader();
	});
}

function allMessages(name){
	if (typeof(name) === 'undefined') {
		name = ""; // or whatever default value you want
	}else{
		name = $("#user_search").val();
	}
	$.ajax({
		url: site_url +"messages/all",
		type: 'get',
		dataType: 'json',
		data:{q:name}
	}).done(function (data) {
		console.log(data);
		var markup = "";
		for(var i=0; i<data.message.length; i++){
			var avatar = site_url+"media_avatar/"+data.message[i].avatar;
			var last_online = data.message[i].last_online.split(' ');
			last_online = last_online.join('T')+"Z";
			var time_ago = jQuery.timeago(last_online);
			var last_seen = new Date(last_online).getTime() / 1000;
			var now = Math.floor(Date.now() / 1000);
			var online_stat = "u_online";
			if( (now - last_seen) > 300){
				online_stat = "u_offline";
			}

			markup += "<li onclick='loadConversation(\""+data.message[i].id+"\",\""+data.message[i].name+"\",\""+data.message[i].username+")'><span class='fa  fa-circle add_b "+online_stat+"'></span><a class='post_title read_more active' href='javascript:void(0)'><div class='recent_posts_img'><img src='"+avatar+"'></div><div class='recent_posts_content'>"+data.message[i].name+"<br class='clear'> <span>"+time_ago+"</span> </div><div class='clear'></div></a></li>";
		}
		if(data.message.length){
			loadConversation(data.message[0].id,data.message[0].name,data.message[0].username);
		}
		$("#all_messages_area").html(markup);
	}).fail(function () {
		showError("Error Occurred");
	});
}

function sendMessage(id,name,username){
	var message = $("#message_body").html();
	if(!message){
		showError("Message is empty");
		return;
	}
	showLoader();
	$.ajax({
		url: site_url +"messages",
		type: 'post',
		dataType: 'json',
		data: {id:id,msg:message}
	}).done(function (data) {
			loadConversation(id,name,username);
			$("#message_body").html('');
			hideLoader();
	}).fail(function () {
			hideLoader();
			showError("Error Occurred");
		});
}
function clearConversation(id,name,username){
	if(confirm("Clear conversation with "+name+" ?")){

		showLoader();
		$.ajax({
			url: site_url +"messages",
			type: 'delete',
			dataType: 'json',
			data: {id:id}
		}).done(function (data) {
			loadConversation(id,name,username);
			hideLoader();
		}).fail(function () {
			hideLoader();
			showError("Error Occurred");
		});

	}
}

function loadConversation(id,name,username){
	showLoader();
	$.ajax({
		url: site_url +"messages/conversation/"+id,
		type: 'get',
		dataType: 'json',
	}).done(function (data) {
			$("#conversation_with").html(name);
			$("#conversation_with").attr("href",site_url+username);
			var markup = "";
			for(var i=0; i<data.message.length; i++){
				if(data.message[i].message_status == 'cleared'){
					continue;
				}
				var avatar = site_url+"media_avatar/"+data.message[i].avatar;
				var created_at = data.message[i].created_at.split(' ');
				created_at = created_at.join('T')+"Z";
				var time_ago = jQuery.timeago(created_at);
				markup += "<li> <div class='item'><div class='testimonial_item_wrapper me'><div class='testimonials_photo'> <img alt='' src='"+avatar+"' class='testimonials_ava'> </div><div class='testimonials_text'>"+data.message[i].body+"</div></div><div class='testimonials_footer'> "+time_ago+"</div></div></li>";
			}
			$("#submit").off();
			$("#clear").off();
			$("#submit").click(function(){
				sendMessage(id,name,username);
			});
			$("#clear").click(function(){
				clearConversation(id,name,username);
			});
			$("#conversation_list").html(markup);
			hideLoader();
		}

	).fail(function () {
			hideLoader();
		showError("Error Occurred");
	});
	$("#conversation_area").show();
}
function newMessages(){
		doAjax('messages/unread','GET','json','',true)
			.then(function(data){
				$("#messages_count").html(data.count);
				if(data.message.length == 0){
					$("#messages_area").html("No New Messages");
				}
				else{
					var markup = "";
					for(var i=0; i<data.message.length; i++){
						var avatar = site_url+"media_avatar/"+data.message[i].avatar;
						markup += "<div class='accordion_in'><div class='acc_head'> <div class='notif_pic_con'><img src='"+avatar+"' /> </div><span class='notf_text notif_msgs_list'> "+data.message[i].name+"<br /> <font>"+data.message[i].time+"</font> </span> </div> <div class='acc_content'> <a href='"+site_url+"messages'><p>"+data.message[i].preview+"</p></a> </div> </div>";
					}
					$("#messages_area").html(markup);
				}
			});



}


function allNotifications(){
	showLoader();
	$.ajax({
		url: site_url + "notifications/all",
		type: 'get',
		dataType: 'json',
	}).done(function (data) {
		hideLoader();
		var markup = "";
		if(data.message.length == 0){
			$("#all_notifications_area").html("No Notifications");
			return;
		}
		for(var i=0; i< data.message.length; i++){
			var avatar = site_url+"media_avatar/"+data.message[i].avatar;
			markup += "<li style='cursor: pointer; cursor: hand;' id='notification_"+data.message[i].id+"'><span onclick='clearNotification(\""+data.message[i].id+"\")' class='fa  fa-times add_b'></span><a class='post_title read_more' onclick='flagNotification(\""+data.message[i].id+"\")'><div class='recent_posts_img'><img src='"+avatar+"' /></div><div class='recent_posts_content'>"+data.message[i].message+"<br class='clear'> <span class='notif_time'>"+data.message[i].time+"</span> </div><div class='clear'></div></a></li>";
		}
		console.log(markup);
		$("#all_notifications_area").html(markup);

	}).fail(function () {
		hideLoader();
	});
}

function countNotifications(){
	doAjax('notifications/count','GET','json','',true)
		.then(function(data){
			$("#notification_count").html(data.count);
		});
}

function stayOnline(){
	doAjax('stayonline','GET','json','',true)
		.then(function(result){
			console.log(result);
		});
}

var setContents = function() {
	$("#users_avatar").attr("src","http://urrban.herokuapp.com/media_avatar/1.png");
}

$(document).ready(function(){

	//get notification count
	countNotifications();
	newMessages();
	stayOnline();
	setInterval(stayOnline, 20000);
	setContents();
	//end get

	$( ".del_button" ).click(function() {
		var is_wall_post = $(this).hasClass('wallpost');
		if(confirm('Delete this post?')) {

			var post_id = $(this).val();
				$.ajax({
					url: site_url + "gallery/media",
					type: 'delete',
					dataType: 'json',
					data: {post_id: post_id,is_wall_post:is_wall_post}
				}).done(function (data) {
					$(".media_"+post_id).remove();
					$.fancybox.close();
				}).fail(function () {
				});


		}

	});

	$(".like_botton").click(function () {

		var has_liked_post = $(this).hasClass("liked");
		var is_wall_post = $(this).hasClass('wallpost');
		var is_media = $(this).hasClass('media');
		var is_popover = $(this).hasClass('pop');
		var like_url = 'gallery';
		if(is_wall_post){
			like_url = 'feed';
		}

		var post_id = $(this).val();
		var cnt_likes = parseInt($("#fd_lks" + post_id).html());
		if (has_liked_post) {
			//if the user already liked the post clicked, carry out the following
			$(this).qtip('option', 'content.text', 'like this post');
			$(this).removeClass("liked");
			if (is_popover) {
				$("#media_butt_" + post_id).removeClass("liked");
				$("#media_butt_" + post_id).addClass("notliked");
			}
			if (is_media) {
				$("#pop_butt_" + post_id).removeClass("liked");
				$("#pop_butt_" + post_id).addClass("notliked");
			}
			$(this).addClass("notliked");

			$.ajax({
				url: site_url + like_url+"/unlike",
				type: 'post',
				dataType: 'json',
				data: {post_id: post_id}
			}).done(function (data) {
			}).fail(function () {
			});

			var new_cnt = cnt_likes - 1;
			$("#fd_lks" + post_id).html(new_cnt);
			$("#fd_lks_pop" + post_id).html(new_cnt);
		}
		else {
			//if the user has  not yet liked the post, carry out the following
			$(this).qtip('option', 'content.text', 'Unlike this post');
			$(this).removeClass("notliked");
			if (is_popover) {
				$("#media_butt_" + post_id).removeClass("notliked");
				$("#media_butt_" + post_id).addClass("liked");
			}
			if (is_media) {
				$("#pop_butt_" + post_id).removeClass("notliked");
				$("#pop_butt_" + post_id).addClass("liked");
			}
			$(this).addClass("liked");
			$.ajax({
				url: site_url + like_url+"/like",
				type: 'post',
				dataType: 'json',
				data: {post_id: post_id}
			}).done(function (data) {
			}).fail(function () {
			});

			var new_cnt = cnt_likes + 1;
			$("#fd_lks" + post_id).html(new_cnt);
			$("#fd_lks_pop" + post_id).html(new_cnt);
		}

	});
});

function interactions(){



};

        function setUpWindow() {
            "use strict";
            main_wrapper.css("min-height", window_h - parseInt(site_wrapper.css("padding-top"), 10) - parseInt(site_wrapper.css("padding-bottom"), 10) + "px")
        }
        jQuery(document).ready(function() {
            "use strict";
            setUpWindow(), jQuery(".beforeAfter_wrapper").each(function() {
                var e = jQuery(this).find(".after_wrapper"),
                    t = jQuery(this),
                    r = jQuery(this).find(".result_line");
                e.width(t.width() / 2), r.css("left", t.width() / 2 + "px")
            }), jQuery(".beforeAfter_wrapper").mousemove(function(e) {
                var t = jQuery(this).offset().left,
                    r = e.pageX - t,
                    i = jQuery(this).find(".after_wrapper"),
                    s = jQuery(this).find(".result_line");
                i.width(r), s.css("left", r + "px")
            }), jQuery(".beforeAfter_wrapper").FingerScroll()
        }), jQuery(window).load(function() {
            "use strict";
            setUpWindow(), jQuery(".beforeAfter_wrapper").each(function() {
                var e = jQuery(this).find(".after_wrapper"),
                    t = jQuery(this),
                    r = jQuery(this).find(".result_line");
                e.width(t.width() / 2), r.css("left", t.width() / 2 + "px")
            })
        }), jQuery(window).resize(function() {
            "use strict";
            setUpWindow();
            var e = setTimeout(function() {
                setUpWindow(), clearTimeout(e)
            }, 500)
        }), jQuery.fn.FingerScroll = function() {
            "use strict";
            var e = 0,
                t = 15,
                r = 0,
                i = $(this).find(".after_wrapper"),
                s = $(this).find(".result_line");
            return jQuery(this).bind("touchstart", function(t) {
                var r = t.originalEvent;
                e = jQuery(this).scrollTop() + r.touches[0].pageY
            }), jQuery(this).bind("touchmove", function(e) {
                var n = e.originalEvent;
                t = jQuery(this).offset().left, r = n.touches[0].pageX - t, i.width(r), s.css("left", r + "px"), n.preventDefault()
            }), this
        };




/*


*/

<!--	Start of masonry editing-->
function initialise_masonry(){


	function iframe16x9fb(e) {
		//"use strict";
		e.find("iframe").each(function() {
			jQuery(this).height(jQuery(this).width() / 16 * 9)
		})
	}
	jQuery(document).ready(function() {

		//"use strict";
		jQuery(window).resize(function () {
		//	"use strict";
			jQuery(".is_masonry").masonry(), iframe16x9(jQuery(".fs_blog_module"))
		}), jQuery(window).load(function () {
		//	"use strict";
			jQuery(".is_masonry").masonry(), iframe16x9(jQuery(".fs_blog_module"))
		});
	});

	console.log("doing masonry");
};
<!--	end  of masonry editing-->

$(document).ready(function() {
	//	set_footer_down()
	  	toggle_switch();
		request_button_switch();
		who_hw_changer_color();
		g_t_cntrl_poper();
		my_magnific_video_fu();
		post_area_slide();
	//	sticky_onload_notif();
	//	('#who_hw_changer').on('change', who_hw_changer_color);


});